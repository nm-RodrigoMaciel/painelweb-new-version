verifyEach();

/*Category*/

$(document).on('click', '.category-section .category', function () {
    $(document).scrollTop($('#' + $(this).attr('ref')).offset().top - $('.floating-header').height());
});

/*Alert Delete Category*/

$(document).on('click', '.delete-category-header svg:nth-child(3), .delete-category-footer p:nth-child(1)', function () {
    $('.delete-category').remove();
    opacity(false);
});

$(document).on('click', '.more-options .dropdown-item:nth-child(2)', function () {
    opacity(true);
    alertDeleteCategory($(this).closest('.category-section-title').attr('cat'));
});

function alertDeleteCategoryText(cat) {
    if ($('.delete-category-text input[type="text"]').val().replace(/ /g, '').toLowerCase() == "sim,tenhocerteza") {
        $.post("deleteCategory", {
            category: cat
        },
            function (data, status) {
                if (status == "success") {
                    window.location.href = "listar";
                }
            });
    } else
        $('.delete-category-text').addClass('alert-field');
}

function alertDeleteCategory(title) {
    $('body').append('<div class="delete-category center-fix"> <div class="delete-category-header"> <svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"> <path d="M11.46 0.146C11.3663 0.052531 11.2394 2.8119e-05 11.107 0L4.893 0C4.76065 2.8119e-05 4.63371 0.052531 4.54 0.146L0.146 4.54C0.052531 4.63371 2.8119e-05 4.76065 0 4.893L0 11.107C2.8119e-05 11.2394 0.052531 11.3663 0.146 11.46L4.54 15.854C4.63371 15.9475 4.76065 16 4.893 16H11.107C11.2394 16 11.3663 15.9475 11.46 15.854L15.854 11.46C15.9475 11.3663 16 11.2394 16 11.107V4.893C16 4.76065 15.9475 4.63371 15.854 4.54L11.46 0.146ZM8 4C8.535 4 8.954 4.462 8.9 4.995L8.55 8.502C8.53824 8.63977 8.4752 8.76811 8.37336 8.86164C8.27151 8.95516 8.13827 9.00705 8 9.00705C7.86173 9.00705 7.72849 8.95516 7.62664 8.86164C7.5248 8.76811 7.46176 8.63977 7.45 8.502L7.1 4.995C7.08743 4.86923 7.10134 4.74223 7.14084 4.62217C7.18035 4.5021 7.24456 4.39165 7.32934 4.29791C7.41413 4.20418 7.51761 4.12924 7.63312 4.07793C7.74863 4.02662 7.87361 4.00007 8 4ZM8.002 10C8.26722 10 8.52157 10.1054 8.70911 10.2929C8.89664 10.4804 9.002 10.7348 9.002 11C9.002 11.2652 8.89664 11.5196 8.70911 11.7071C8.52157 11.8946 8.26722 12 8.002 12C7.73678 12 7.48243 11.8946 7.29489 11.7071C7.10736 11.5196 7.002 11.2652 7.002 11C7.002 10.7348 7.10736 10.4804 7.29489 10.2929C7.48243 10.1054 7.73678 10 8.002 10Z" fill="#B00020" /> </g> <defs> <clipPath id="clip0"> <rect width="16" height="16" fill="white" /> </clipPath> </defs> </svg> <p>Atenção</p> <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M9.6979 8L14 12.3021L12.3021 14L8 9.6979L3.6979 14L2 12.3021L6.3021 8L2 3.6979L3.6979 2L8 6.3021L12.3021 2L14 3.6979L9.6979 8Z" fill="var(--color-primary)" /> </svg> </div> <div class="delete-category-body"> <p> Você está prestes a <b>EXCLUIR</b> a categoria <b>' + title + '</b> e todos os produtos cadastrados nela.<br><br>Tem certeza desta ação?<br><br>Se sim, escreva ”Sim, tenho certeza” abaixo: </p> </div> <div class="delete-category-text"> <input type="text" placeholder="Frase de confirmação aqui"><p>Escreva para confirmar ou cancele a ação</p> </div> <div class="delete-category-footer"> <p>Cancelar</p> <p>Excluir</p> </div> </div>');

    $(document).on('click', '.delete-category-footer p:nth-child(2)', function () {
        alertDeleteCategoryText(title);
    });
}

/*Copy products*/

$('.copy-products-category .category:nth-child(1)').addClass('selected');
$('.product-' + $('.copy-products-category .category:nth-child(1)').attr('ref')).css('display', 'flex');

$(document).on('click', '.copy-products-item input[type=checkbox]', function () {
    if ($(this).is(':checked'))
        $(this).closest('.copy-products-item').css("background-color", "#F5F5F5");
    else
        $(this).closest('.copy-products-item').css("background-color", "#FCFCFC");

});

$(document).on('click', '.duplicate-product', function () {
    opacity(true);
    $('.copy-products').fadeToggle(100);

    var ref = $(this).attr('ref');

    $(document).on('click', '.copy-products-footer p:nth-child(2)', function () {
        var products = [];

        $(".copy-products .copy-products-body input[type=checkbox]:checked").each(function () {
            products.push({
                "codigoBarras": $(this).attr('cod'),
                "categoria": $(this).attr('cat')
            });
        });

        $.ajax({
            type: 'POST',
            url: 'copyProductsCategory',
            cache: false,
            data: { itens: JSON.stringify(products), category: ref },
            dataType: 'json'
        }).done(function (data) {
            window.location.href = "listar";
        }).fail(function (data) {
            planList(data);
            showMessageAlert("Tivemos um problema ao tentar copiar os produtos.", "error");
            window.location.href = "listar";
        });

    });

});

$(document).on('click', '.copy-products-footer p:nth-child(1)', function () {
    $('.copy-products').fadeToggle(100);

    $('.copy-products input[type="checkbox"]').prop('checked', false);
    $('.copy-products .category-all').remove();
    $('.copy-products .header-category').css('margin-left', '0');

    opacity(false);
});

$(document).on('click', '.copy-products-category .category', function () {
    var ref = $(this).parent().parent().parent();

    $('.category-all', ref).removeClass('selected');
    $('.popup-body', ref).scroll();

    if ($('.copy-products .search input[type="text"]').val() == '') {
        $('.copy-products .product-header').show();
        $('.copy-products .copy-products-item').show();
        var topPos = $('.copy-products .copy-products-body').offset().top;
        $('.copy-products .copy-products-body').scrollTop(topPos);
        var position = $('.copy-products #' + $(this).attr('ref')).offset().top;
        $('.copy-products .copy-products-body').scrollTop(position);
    }
});

$(document).on('input', '.copy-products .search input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.copy-products-item').hide();
        $('.product-header').hide();
        $('.copy-products-item[search*=' + $(this).val().toLowerCase().replace(/\s+/g, '') + ']').css('display', 'flex');
    } else {
        $('.copy-products-item').hide();
        $('.product-header').show();
        $('.product-' + $('.copy-products-category .category.selected').attr('ref')).css('display', 'flex');
    }
});

/*Update copy products category*/

$(document).on('click', '.copy-products-item input[type=checkbox]', function () {
    updateCopyProductPreview();
});

function updateCopyProductPreview() {
    $(".copy-products .header-category .category-all").remove();
    $(".copy-products .header-category").css('margin-left', '0');

    var cont = 0;
    $(".copy-products-item input[type=checkbox]:checked").each(function () {
        cont++;
    });

    if (cont > 0) {
        $("<div class='category-all'><span>Selecionados (" + cont + ")</span></div>").prependTo(".copy-products .header-category");
        $(".copy-products .header-category").css('margin-left', '149px');
    }
}

/*Search Product*/

$(document).on('input', '.see-products .search-bar input[type="text"]', function () {
    var ref_text = $('.see-products-ordering > p');

    if ($(this).val() != '') {
        $('.title-area, .floating-header .header-category').hide();
        $('.category-section-title, .see-products-product').hide();
        $('.see-products-product[search*=' + $(this).val().toLowerCase().replace(/\s+/g, '') + ']').show();

        var qtd = $('.see-products-product[search*=' + $(this).val().toLowerCase().replace(/\s+/g, '') + ']').length;
        if ($(ref_text).attr('old') == null || $(ref_text).attr('old') == '')
            $(ref_text).attr('old', $(ref_text).text());

        $(ref_text).text(qtd + ' resultados');
    } else {
        $('.title-area, .floating-header .header-category').css('display', 'flex');
        $('.category-section-title, .see-products-product').show();

        $(ref_text).text($(ref_text).attr('old'));
        $(ref_text).attr('old', null);
    }
});

/*Fast Update*/

var timer;
$(document).on('keyup', '.see-products-product input[type="text"]', function () {
    if (timer != null)
        clearTimeout(timer);

    var element = $(this).closest('.see-products-product');
    timer = setTimeout(function () { fastUpdate(element) }, 2000);
});

$(document).on('click', '.see-products-product .enabled-obj', function () {
    var cat = $(this).attr('cat');
    var checked = $(this).is(':checked');
    var element = $(this).closest('.see-products-product');

    if (checked) {
        $('.category-section-title[cat="' + cat + '"] .category-switch').prop('checked', true);
        $('.category-section-title[cat="' + cat + '"] .title-disponibility').text('Disponível');
    }
    else {
        if ($('.see-products-product .enabled-obj[cat="' + cat + '"]:checked').length < 1) {
            $('.category-section-title[cat="' + cat + '"] .category-switch').prop('checked', false);
            $('.category-section-title[cat="' + cat + '"] .title-disponibility').text('Indisponível');
        }
    }

    fastUpdate(element);
});

$(document).on('click', '.see-products-product .trash-area', function () {
    var element = $(this).closest('.see-products-product');

    fastUpdate(element);
});

function fastUpdate(element) {
    var valor = nmoney($('.value-obj', element).val());

    var finalPromocao = '0';
    if ($('.value-input-added', element).is(':visible')) {
        var promocao = nmoney($('.promotion-obj', element).val());

        var finalPromocao = (promocao / valor) * 100;
        valor = promocao;
    }

    $('.value-input-promotion *', element).css('color', '#4D4D4D');
    $('.promotion-error', element).hide();

    if (promocao != null && parseFloat(promocao) >= parseFloat(nmoney($('.value-obj', element).val()))) {
        $('.value-input-promotion *', element).css('color', '#B00020');
        $('.promotion-error', element).show();
    } else {
        var itens = [{
            "codigoBarras": $(element).attr('cod'),
            'disponivel': $('.enabled-obj', element).is(':checked') ? true : false,
            "valorInicial": valor.toString(),
            "promocao": finalPromocao.toString()
        }];

        $.ajax({
            type: 'POST',
            url: 'fastUpdate',
            cache: false,
            data: { fastUpdate: JSON.stringify(itens) },
            dataType: 'json'
        }).done(function (data) {
            showMessageAlert("Produto atualizado.");
        }).fail(function (data) {
            planList(data);
            alert("Tivemos problemas para atualizar seu produto.");
        });
    }
}

/*Orderning*/

var current;
$(document).on('click', '.ordering-item', function () {

    $('.ordering').text($(this).text());

    var pos = $(this).index();
    $(".category-section-title").each(function (index) {
        var cat = $(this).attr("cat");
        $(".see-products-product[cat='" + cat + "']").each(function (index) {
            current = $(this);
            switch (pos) {
                case 0:
                    $(".see-products-product[cat='" + cat + "']").each(function (index) {
                        if ($('.title', current).text() < $('.title', this).text()) {
                            $(this).before(current);
                            return false;
                        } else
                            $(this).after(current);
                    });
                    break;
                case 1:
                    $(".see-products-product[cat='" + cat + "']").each(function (index) {
                        if (parseFloat($('.value-obj', current).val()) < parseFloat($('.value-obj', this).val())) {
                            $(this).before(current);
                            return false;
                        } else
                            $(this).after(current);
                    });
                    break;
                case 2:
                    $(".see-products-product[cat='" + cat + "']").each(function (index) {
                        if (parseFloat($('.value-obj', current).val()) > parseFloat($('.value-obj', this).val())) {
                            $(this).before(current);
                            return false;
                        } else
                            $(this).after(current);
                    });
                    break;
                case 3:
                    $(".see-products-product[cat='" + cat + "']").each(function (index) {
                        if ($('.enabled-obj', current).is(':checked'))
                            $(this).after(current);
                    });
                    break;
                case 4:
                    $(".see-products-product[cat='" + cat + "']").each(function (index) {
                        if (!$('.enabled-obj', current).is(':checked'))
                            $(this).after(current);
                    });
                    break;
            }
        });
    });
});

/* Desativating category */

$(document).on('click', '.category-section-title .category-switch', function () {
    var cat = $(this).attr('cat');
    var checked = $(this).is(':checked');
    var ref = $(this).attr('ref');

    $.post("updateCategories", {
        category: cat,
        availability: checked
    },
        function (data, status) {
            if (status == "success") {
                $('.see-products-product[ref="' + ref + '"] .enabled-obj').prop('checked', checked);
                $('.see-products-product[ref="' + ref + '"] .title-disponibility').text(checked ? 'Disponível' : 'Indisponível');
            } else {
                alert('Algo aconteceu, contacte o administrador');
            }
        });

});

/* SLIDER HORIZONTAL FIXED */

var lastCategory;
var category;
var positionSearch = $(window).scrollTop();
$(document).scroll(function () {

    if ($(window).scrollTop() + 100 + $(window).height() >= $(document).height()) {
        category = $(".category-section-title .title").last().attr('id');

        $('.category-section .category').removeClass('selected');
        $('.category-section .category[ref="' + category + '"]').addClass('selected');

        lastCategory = category;
    } else {
        if ($(document).scrollTop() < 270) {
            $('.floating-header').removeClass('floating');
            $('.floating-search').removeClass('floating');
            $('.floating-header').removeClass('show-search');
        } else {
            $('.floating-header').addClass('floating');

            var scrollSearch = $(window).scrollTop();
            if (scrollSearch > positionSearch) {
                $('.floating-search').removeClass('floating');
                $('.floating-header').removeClass('show-search');
            } else {
                $('.floating-search').addClass('floating');
                $('.floating-header').addClass('show-search');
            }
            positionSearch = scrollSearch;
        }

        var position = $(document).scrollTop();

        $(".category-section-title .title").each(function () {
            if (position + 110 < $(this).offset().top) {
                return false;
            }
            category = $(this).attr('id');
        });

        if (category != lastCategory) {
            $('.category-section .category').removeClass('selected');
            $('.category-section .category[ref="' + category + '"]').addClass('selected');

            var scroll = 0;
            $(".category-section .category").each(function () {
                if ($(this).hasClass('selected'))
                    return false;

                scroll += $(this).width();
            });

            $('.category-section').animate({ scrollLeft: scroll }, 50);

            lastCategory = category;
        }
    }
});

/* SLIDER HORIZONTAL FIXED COPY PRODUCTS*/

$('.copy-products-body').scroll(function (e) {
    var bodyTop = $(this).offset().top;

    $(".product-header").each(function () {
        if ($(this).offset().top > bodyTop) {
            return false;
        }
        category = $(this).attr('id');
    });

    if (category != lastCategory) {
        $('.copy-products-category .category').removeClass('selected');
        $('.copy-products-category .category[ref=' + category + ']').addClass('selected');

        var scroll = 0;
        $(".copy-products-category .category").each(function () {
            if ($(this).hasClass('selected'))
                return false;

            scroll += $(this).width();
        });

        $('.copy-products-category').animate({ scrollLeft: scroll }, 50);

        lastCategory = category;
    }
});

/*Header switch*/

$(".category-section-title").each(function () {
    var ref = $(this).attr('ref');
    var has = false;
    $(".see-products-product[ref=" + ref + "] .cl-switch input").each(function () {
        if ($(this).is(":checked"))
            has = true;
    });
    if (has)
        $('.title-disponibility', this).text('Disponível');
    else
        $('.title-disponibility', this).text('Indisponível');
});

/*Not special caracter*/

$(document).on('input', '.register-category-fields input[type="text"]', function () {
    if (!hasSpecialCharacter($(this).val()))
        $(this).val($(this).val().slice(0, -1));
});

function hasSpecialCharacter(val) {
    return /^[a-õ-zA-Z0-9- ?!,.*]*$/.test(val);
}
