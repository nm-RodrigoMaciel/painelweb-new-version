/* Preview */

$(document).on('click', '.remove-ingredients', function () {
    if ($('.product-section .ingredientes').hasClass('closed')) {
        $('.product-section .ingredientes').removeClass('closed');
    } else {
        $('.product-section .ingredientes').addClass('closed');
    }
    $('.product-section .remove-ingredients svg').toggle();
});

$(document).on('input', '.product-title input[type=text]', function () {
    $('.product-preview .info .title-text').text($(this).val() != '' ? $(this).val() : 'Nome do produto');
});

$(document).on('input', '.serves-quantity .quantity input[type=number]', function () {
    if ($(this).val() > 0) {
        $('.product-preview .info .quantidade').show();
        $('.product-preview .info .quantidade span').text($(this).val() + ' ' + $('#unity-product-obj').attr('value'));
    } else
        $('.product-preview .info .quantidade').hide();
});

$(document).on('input', '.serves-quantity .serves input[type=number]', function () {
    if ($(this).val() > 0) {
        $('.product-preview .info .serve').show();
        $('.product-preview .info .serve span').text('Serve ' + $(this).val() + ($(this).val() > 1 ? ' pessoas' : ' pessoa'));
    } else
        $('.product-preview .info .serve').hide();
});

$(document).on('input', '.product-description textarea', function () {
    $('.product-preview .info .description span').text($(this).val());
});

/*Category Dropdown*/

$(document).on('click', '.selector-category, .category-dropdown-footer p:nth-child(1)', function () {
    $('.category-dropdown').fadeToggle(100);
});

$(document).on('click', '.category-dropdown-item input[type="radio"]', function () {
    $('.category-selected').text($(this).attr('ref'));
    $('.category-dropdown').fadeToggle(100);
});

$(document).on('input', '.category-dropdown .search-area input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.category-dropdown-item').hide();
        $('.category-dropdown-item[search*=' + $(this).val().replace(/ /g, '').toLowerCase() + ']').show();
    } else {
        $('.category-dropdown-item').show();
    }
});

/*Change Type*/

$(document).on('click', '.button-option div', function () {
    $('.ordinary').hasClass('selected') ? comboProduct() : defaultProduct();
    $('.button-option div').removeClass('selected');
    $(this).addClass('selected');
});

function defaultProduct() {
    /*Ingredientes*/
    $('.product-ingredients div:nth-child(1) p:nth-child(1)').text('Deseja detalhar ingredientes?');
    $('.product-ingredients div:nth-child(1) p:nth-child(2)').text('Ao inserir ingredientes, os clientes poderão removê-los.');
    $('.product-ingredients .selector p').text('Selecionar ingredientes');

    $('.selected-ingredients .selected-combo').remove();
    $('.selected-ingredients .selected').show();
    updateIngredientsPreview();
}

function comboProduct() {
    /*Ingredientes*/
    $('.product-ingredients div:nth-child(1) p:nth-child(1)').text('Detalhe os produtos do combo');
    $('.product-ingredients div:nth-child(1) p:nth-child(2)').text('Com produtos detalhados, os clientes poderão removê-loso');
    $('.product-ingredients .selector p').text('Selecionar produtos');

    $('.selected-ingredients .selected').hide();
    updateProductPreview();
}

/*Upload Image*/

var readURL = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.product-section .image-uploaded').css('width', '360px');
            $('.product-section .image-uploaded').css('height', '360px');
            $('.product-section .image-uploaded').css('background-image', 'url(' + e.target.result + ')');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('change', '.file-upload', function () {
    readURL(this);
});

$(document).on('click', '.upload-area, .image-uploaded', function () {
    $('.file-upload').click();
});

/*Aditional/Ingredient Dropdown*/

$(document).on('change', '.product-dropdown-item-check', function () {
    var ref = $(this).parents('.product-dropdown-item-more');
    if ($(this).is(':checked'))
        $('.input-quantity', ref).css('display', 'flex');
    else
        $('.input-quantity', ref).hide();
});

/*Dropdown Ingredient*/

$(document).on('click', '.product-ingredients .selector', function () {
    if (isCombo())
        $('.product-details').fadeToggle(100);
    else
        $('.ingredient-details').fadeToggle(100);
    opacity(true);
});

$(document).on('click', '.selected-ingredients .selected-ordinary', function () {
    $('.ingredient-details').fadeToggle(100);
    opacity(true);

    $('.ingredient-details .category-all').click();
});

$(document).on('click', '.ingredient-details .product-dropdown-item p', function () {
    if ($('.ingredient-details .category-all').hasClass('selected')) {
        var cod = $($(this).parent()).attr('cod');
        var position = 0;

        $('.ingredient-details .category-all').removeClass('selected');
        $('.ingredient-details .all-item .popup-item').show();

        var topPos = $('.ingredient-details .all-item').offset().top;
        $('.ingredient-details .all-item').scrollTop(topPos);
        position = $('.ingredient-details .product-dropdown-item[cod="' + cod + '"]').offset().top;
        $('.ingredient-details .all-item').scrollTop(position);

        $('.ingredient-details .product-dropdown-item').removeClass('pointer');
    }
});

$(document).on('click', '.ingredient-details .product-dropdown-buttons p', function () {
    $('.ingredient-details').fadeToggle(100);
    opacity(false);
});

$(document).on('click', '.ingredient-details .category', function () {
    var ref = $(this).parent().parent().parent();

    $('.category-all', ref).removeClass('selected');
    $('.popup-body', ref).scroll();

    $('.ingredient-details .ingredient-header').show();
    $('.ingredient-details .product-dropdown-item').show();
    var topPos = $('.ingredient-details .all-item').offset().top;
    $('.ingredient-details .all-item').scrollTop(topPos);
    var position = $('.ingredient-details #' + $(this).attr('ref')).offset().top;
    $('.ingredient-details .all-item').scrollTop(position);

    $('.ingredient-details .product-dropdown-item').removeClass('pointer');
});

$(document).on('input', '.ingredient-details .search-area input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.ingredient-details .ingredient-header').hide();
        $('.ingredient-details .product-dropdown-item').hide();
        $('.ingredient-details .product-dropdown-item[search*=' + $(this).val().replace(/ /g, '').toLowerCase() + ']').css('display', 'flex');
    } else {
        $('.ingredient-details .ingredient-header').show();
        $('.ingredient-details .product-dropdown-item').show();
    }
});

/* Adittional Dropdown */

$(document).on('click', '.header-adittional-button', function () {
    var header = $(this).parent().parent().parent();
    var ref = $(this).attr('ref');

    if ($(header).hasClass('selected')) {
        $(header).removeClass('selected');

        $('.header-adittional-options input[type="checkbox"]', header).prop('checked', false);
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] input[type="checkbox"]').prop('checked', false);
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] .input-quantity').hide();
    } else {
        $(header).addClass('selected');

        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] input[type="checkbox"]').prop('checked', true);
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] .input-quantity').css('display', 'flex');
    }
});

$(document).on('click', '.product-additional .selector', function () {
    $('.adittional-details').fadeToggle(100);
    opacity(true);
});

$(document).on('click', '.selected-adittional .selected-ordinary', function () {
    $('.adittional-details').fadeToggle(100);
    opacity(true);

    $('.adittional-details .category-all').click();
});

$(document).on('click', '.adittional-details .product-dropdown-item p', function () {
    if ($('.adittional-details .category-all').hasClass('selected')) {
        var cod = $($(this).parent()).attr('cod');
        var position = 0;

        $('.adittional-details .category-all').removeClass('selected');
        $('.adittional-details .all-item .popup-item').show();

        var topPos = $('.adittional-details .all-item').offset().top;
        $('.adittional-details .all-item').scrollTop(topPos);
        position = $('.adittional-details .product-dropdown-item[cod="' + cod + '"]').offset().top;
        $('.adittional-details .all-item').scrollTop(position);

        $('.adittional-details .product-dropdown-item').removeClass('pointer');
    }
});

$(document).on('click', '.adittional-details .product-dropdown-buttons p', function () {
    $('.adittional-details').fadeToggle(100);
    opacity(false);
});

$(document).on('click', '.adittional-details .category', function () {
    var ref = $(this).parent().parent().parent();

    $('.category-all', ref).removeClass('selected');
    $('.popup-body', ref).scroll();

    $('.adittional-details .product-dropdown-item, .header-adittional').show();
    var topPos = $('.adittional-details .all-item').offset().top;
    $('.adittional-details .all-item').scrollTop(topPos);
    var position = $('.adittional-details #' + $(this).attr('ref')).offset().top;
    $('.adittional-details .all-item').scrollTop(position);
});

$(document).on('input', '.adittional-details .search-area input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.adittional-details .product-dropdown-item').hide();
        $('.adittional-details .header-adittional').hide();
        $('.adittional-details .product-dropdown-item[search*=' + $(this).val().replace(/ /g, '').toLowerCase() + ']').css('display', 'flex');
    } else {
        $('.adittional-details .header-adittional').show();
        $('.adittional-details .product-dropdown-item').show();
    }
});

$(document).on('change', '.adittional-details .product-dropdown-item input[type=checkbox]', function () {
    var ref = $(this).attr('ref');
    if ($('.adittional-details .product-dropdown-item input[type=checkbox][ref="' + ref + '"]:checked').length > 0) {
        $('.header-adittional[ref="' + ref + '"]').addClass('selected');
    } else {
        $('.header-adittional[ref="' + ref + '"]').removeClass('selected');
    }
});

/*Dropdown Product*/

$(document).on('click', '.product-details .combo-products-item input[type=checkbox]', function () {
    var combo_item = $(this).parent().parent().parent();

    if ($(this).is(':checked')) {
        $(combo_item).addClass('selected');
    } else {
        $(combo_item).removeClass('selected');
    }
});

$(document).on('click', '.product-details .product-dropdown-buttons p', function () {
    $('.product-details').fadeToggle(100);
    opacity(false);
});

$('.product-' + $('.product-details .category:nth-child(1)').attr('ref')).show();

$(document).on('click', '.selected-combo', function () {
    $('.product-details').fadeToggle(100);
    opacity(true);

    $('.product-details .category-all').click();
});

$(document).on('click', '.product-details .combo-products-item .info', function () {
    if ($('.product-details .category-all').hasClass('selected')) {
        var cod = $($(this).parent().parent()).attr('cod');
        var position = 0;

        $('.product-details .category-all').removeClass('selected');
        $('.product-details .all-item .popup-item').show();

        var topPos = $('.product-details .all-item').offset().top;
        $('.product-details .all-item').scrollTop(topPos);
        position = $('.product-details .combo-products-item[cod="' + cod + '"]').offset().top;
        $('.product-details .all-item').scrollTop(position);

        $('.product-details .combo-products-item').removeClass('pointer');
    }
});

$(document).on('click', '.product-details .category', function () {
    var ref = $(this).parent().parent().parent();

    $('.category-all', ref).removeClass('selected');
    $('.popup-body', ref).scroll();

    $('.product-details .product-header').show();
    $('.product-details .combo-products-item').show();
    var topPos = $('.product-details .all-item').offset().top;
    $('.product-details .all-item').scrollTop(topPos);
    var position = $('.product-details #' + $(this).attr('ref')).offset().top;
    $('.product-details .all-item').scrollTop(position);
});

$(document).on('input', '.product-details .search-area input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.product-details .product-header').hide();
        $('.product-details .combo-products-item').hide();
        $('.product-details .combo-products-item[search*=' + $(this).val().replace(/ /g, '').toLowerCase() + ']').show();
    } else {
        $('.product-details .product-header').show();
        $('.product-details .product-dropdown-item').show();
    }
});

/*Update Ingredients Preview and under of dropdown*/

$(document).on('change', '.ingredient-details input[type=checkbox]', function () {
    updateIngredientsPreview();
});

$(document).on('change', '.ingredient-details input[type=number]', function () {
    updateIngredientsPreview();
});

function updateIngredientsPreview() {
    $(".ingredient-details .header-category .category-all").remove();
    $(".ingredient-details .header-category").css('margin-left', '0');

    $('.ingredient-preview .itens div').remove();
    $('.ingredient-preview').hide();

    $('.selected-ingredients').hide();
    $('.selected-ingredients .selected .selected-ordinary').remove();

    var cont = 0;
    $(".ingredient-details .product-dropdown-item input[type=checkbox]:checked").each(function () {
        var ref = $(this).parent().parent().parent();

        var ingredientName = $('.product-dropdown-item-name', ref).text();
        var ingredientQuant = $('.product-dropdown-item-quantity', ref).val();

        $('.selected-ingredients').show();
        $('.ingredient-preview').show();

        $('.remove-ingredients').next().append('<div class="item-container"> <div class="title-choose"> <div class="info"> <span> ' + ingredientName + ' </span> <span style="display: none;"> Descrição ou preço +R$ 0,00 </span> </div> <div class="buttons-elegant-ingredientes elegant" ref="2" max="2" min="0"> <div> <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect y="6.66675" width="16" height="2.66667" fill="#E4002B"></rect> </svg> </div> <div> <span>' + ingredientQuant + '</span> </div> <div> <svg class="item-disabled" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M6.66667 0H9.33333V16H6.66667V0Z" fill="#E4002B"></path> <path d="M16 6.66667V9.33333L0 9.33333L1.16564e-07 6.66667L16 6.66667Z" fill="#E4002B"></path> </svg> </div> </div> </div> </div>');
        $('.selected-ingredients .selected').append('<span class="selected-ordinary">' + ingredientName + '</span>');

        cont++;
    });

    if (cont > 0) {
        $("<div class='category-all'><span>Selecionados (" + cont + ")</span></div>").prependTo(".ingredient-details .header-category");
        $(".ingredient-details .header-category").css('margin-left', '149px');
    }
}

/*Update Adittional Preview and under of dropdown*/

$(document).on('change', '.adittional-details .product-dropdown-item input[type=checkbox]', function () {
    updateAdittionalPreview();
});

$(document).on('change', '.adittional-details .product-dropdown-item input[type=number]', function () {
    updateAdittionalPreview();
});

$(document).on('change', '.adittional-details .header-adittional input[type=checkbox]', function () {
    updateAdittionalPreview();
});

$(document).on('click', '.header-adittional-button', function () {
    updateAdittionalPreview();
});

function updateAdittionalPreview() {
    $(".adittional-details .header-category .category-all").remove();
    $(".adittional-details .header-category").css('margin-left', '0');

    $('.adittional-preview div').remove();
    $('.selected-adittional .selected span').remove();
    $('.selected-adittional').hide();

    var cont = 0;
    $(".header-adittional.selected").each(function () {
        var ref = $(this).attr('ref');

        if (!$(".header-adittional-options input[type=checkbox]", this).is(':checked')) {
            var title = 'd-none'
            var buttons = '<div class="buttons-elegant-adicionais elegant"> <div> <svg class="item-disabled" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <rect y="6.66675" width="16" height="2.66667" fill="#E4002B"></rect> </svg> </div> <div> <span>0</span> </div> <div> <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M6.66667 0H9.33333V16H6.66667V0Z" fill="#E4002B"></path> <path d="M16 6.66667V9.33333L0 9.33333L1.16564e-07 6.66667L16 6.66667Z" fill="#E4002B"></path> </svg> </div> </div>';
            var price = ''
        } else {
            var title = '';
            var buttons = '<div class="area-custom-radio"><input type="radio" class="custom-radio"><label></label> </div>';
            var price = 'd-none'
        }

        $('.adittional-preview').append('<div class="title-adicionais"> <div class="title-description"> <span> ' + $(this).attr('ref') + ' </span><span class="' + title + '" > Escolha uma opção. (obrigatório) </span> </div> </div><div class="itens">');

        $(".adittional-details .product-dropdown-item input[ref='" + ref + "']:checked").each(function () {
            var nameAdittional = $(this).attr('val');
            var priceAdittional = $(this).attr('price');

            $('.adittional-preview').append('<div class="item-container"> <div class="title-choose"> <div class="info"> <span> ' + nameAdittional + ' </span> <div class="' + price + '"><span>+R$&nbsp;</span> <span>' + priceAdittional + '</span> </div> </div> ' + buttons + ' </div> </div>');
            $('.selected-adittional').show();
            $('.selected-adittional .selected').append('<span class="selected-ordinary">' + nameAdittional + '</span>');

            cont++;
        });
    });

    if (cont > 0) {
        $("<div class='category-all'><span>Selecionados (" + cont + ")</span></div>").prependTo(".adittional-details .header-category");
        $(".adittional-details .header-category").css('margin-left', '149px');
    }
}

/*Update product under of dropdown*/

$(document).on('click', '.combo-products-item input[type=checkbox]', function () {
    updateProductPreview();
});

function updateProductPreview() {
    $(".product-details .header-category .category-all").remove();
    $(".product-details .header-category").css('margin-left', '0');

    $('.selected-ingredients .selected-combo').remove();
    $('.selected-ingredients').hide();

    var cont = 0;
    $(".combo-products-item input[type=checkbox]:checked").each(function () {
        var ref = $(this).parent().parent();

        $('.selected-ingredients').show();

        var product = '<div class="selected-combo"> <img class="image" src="' + $('.image', ref).attr('src') + '" onerror="this.style.opacity=0"><svg class="not-wl image-placeholder" width="112" height="112" viewBox="0 0 112 112" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M112 0H0V112H112V0Z" fill="#F5F5F5"/> <path opacity="0.5" d="M48.0603 46.0568C48.0603 47.6391 47.4319 49.1565 46.313 50.2754C45.1942 51.3941 43.6767 52.0227 42.0944 52.0227C40.5122 52.0227 38.9947 51.3941 37.876 50.2754C36.7571 49.1565 36.1285 47.6391 36.1285 46.0568C36.1285 44.4746 36.7571 42.9571 37.876 41.8382C38.9947 40.7195 40.5122 40.0909 42.0944 40.0909C43.6767 40.0909 45.1942 40.7195 46.313 41.8382C47.4319 42.9571 48.0603 44.4746 48.0603 46.0568Z" fill="#BFBFBF"/> <path opacity="0.5" fill-rule="evenodd" clip-rule="evenodd" d="M25.9292 33.8837C27.048 32.765 28.5655 32.1364 30.1477 32.1364H81.8523C83.4345 32.1364 84.952 32.765 86.0709 33.8837C87.1896 35.0025 87.8182 36.52 87.8182 38.1023V59.9015C87.1412 59.8041 86.449 59.7535 85.7449 59.7535C85.0994 59.7535 84.4639 59.796 83.8409 59.8783V38.1023C83.8409 37.5749 83.6314 37.0691 83.2585 36.696C82.8855 36.3231 82.3797 36.1136 81.8523 36.1136H30.1477C29.6203 36.1136 29.1145 36.3231 28.7415 36.696C28.3686 37.0691 28.1591 37.5749 28.1591 38.1023V73.8977C28.1596 73.9508 28.1623 74.0039 28.167 74.0568V71.9091L38.6909 62.5465C39.016 62.2227 39.4431 62.0211 39.8998 61.9762C40.3564 61.9313 40.8146 62.0457 41.1965 62.3L51.7762 69.3477L66.5318 54.592C66.8272 54.2975 67.2078 54.1033 67.6196 54.0369C68.0315 53.9705 68.4538 54.0353 68.8267 54.2221L81.0605 60.5319C75.4003 62.4761 71.3324 67.846 71.3324 74.166C71.3324 76.1898 71.7495 78.1161 72.5025 79.8636H30.1477C28.5655 79.8636 27.048 79.235 25.9292 78.1163C24.8104 76.9975 24.1818 75.48 24.1818 73.8977V38.1023C24.1818 36.52 24.8104 35.0025 25.9292 33.8837Z" fill="#BFBFBF"/> <path opacity="0.5" d="M77.7607 69.0116L80.5855 66.1868L85.7435 71.3449L90.8977 66.1868L93.7225 69.0116L88.5683 74.1697L93.7225 79.3239L90.8977 82.1487L85.7435 76.9945L80.5855 82.1487L77.7607 79.3239L82.9187 74.1697L77.7607 69.0116Z" fill="#BFBFBF"/> </svg> <div> <p class="selected-combo-title">' + $('.info .title', ref).text() + '</p> <p class="selected-combo-description">' + $('.info .description', ref).text() + '</p> <div class="d-flex value">';
        $('.info .measure', ref).text() != '' ? product += '<p class="measure">' + $('.info .measure', ref).text() + '</p>' : '';
        $('.info .serves', ref).text() != '' ? product += '<p class="serves"> <svg class="not-wl" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.8" clip-path="url(#clip0)"> <path d="M11.3584 10.8346C10.8688 10.1908 10.2656 9.27269 9.30092 9.27269C8.33503 9.27269 6.77618 9.27269 6.77618 9.27269C6.36577 9.27269 6.03393 9.60453 6.03393 10.0155C6.03393 10.4254 6.36577 10.7578 6.77618 10.7578H8.07512C8.23892 10.7578 8.3719 10.8908 8.3719 11.0546V11.1041C8.3719 11.2679 8.23892 11.4009 8.07512 11.4009H5.8931L3.2106 9.02548C2.96519 8.80848 2.58984 8.83145 2.37224 9.07685C2.15464 9.32226 2.17761 9.69761 2.42301 9.91521C2.42301 9.91521 5.11821 13.2191 5.47724 13.2191L8.99145 13.2185L10.5382 14L12.6066 12.4853C12.606 12.4853 12.4192 12.2296 11.3584 10.8346Z" fill="#4D4D4D"></path> <path d="M12.4597 7.34875H1.39056V8.16052H12.4597V7.34875Z" fill="#4D4D4D"> </path> <path d="M11.822 6.79027C11.8371 6.62767 11.8468 6.46387 11.8468 6.29765C11.8468 3.92765 10.1719 1.95052 7.94153 1.48148C7.98807 1.36059 8.01648 1.23003 8.01648 1.09222C8.01648 0.4902 7.52628 0 6.92426 0C6.32224 0 5.83204 0.4902 5.83204 1.09222C5.83204 1.23003 5.86045 1.35999 5.90699 1.48148C3.67721 1.94992 2.00171 3.92765 2.00171 6.29765C2.00171 6.46387 2.01017 6.62828 2.02649 6.79027H11.822ZM6.55253 1.09283C6.55253 0.887317 6.71935 0.720491 6.92486 0.720491C7.13037 0.720491 7.2972 0.887317 7.2972 1.09283C7.2972 1.21492 7.23434 1.31828 7.14246 1.38658C7.06993 1.38296 6.99861 1.3751 6.92486 1.3751C6.85112 1.3751 6.77919 1.38296 6.70666 1.38658C6.61479 1.31828 6.55253 1.21492 6.55253 1.09283Z" fill="#4D4D4D"></path> </g> <defs> <clipPath id="clip0"> <rect width="14" height="14" fill="white"></rect> </clipPath> </defs> </svg> ' + $('.info .serves', ref).text() + ' </p>' : '';
        product += '<p class="price">' + $('.info .price', ref).text() + '</p> </div> </div> </div>';

        $('.selected-ingredients').append(product);
        cont++;
    });

    if (cont > 0) {
        $("<div class='category-all'><span>Selecionados (" + cont + ")</span></div>").prependTo(".product-details .header-category");
        $(".product-details .header-category").css('margin-left', '149px');
    }
}

/*Register Product*/

var canRefresh = false;
$(document).on('click', '#save-product', function () {
    if (validateForm() && verifyPromotion() && verifyCategory()) {
        canRefresh = true;
        $('.loading-registering').fadeIn(200);
        opacity(true);

        var categorias = [];
        var itensAdicionais = [];
        var ingredientesIniciais = '';
        var listasIngredientes = '';
        var itensAdicionais = [];
        var produtosCombo = [];

        $(".header-adittional.selected").each(function () {
            categorias.push({
                "id": $(this).attr('ref'),
                "obrigatorio": $(".header-adittional-options input[type=checkbox]", this).is(':checked'),
                "quantidade": ($(".header-adittional-options input[type=number]", this).val() != ""
                    ? parseInt($(".header-adittional-options input[type=number]", this).val())
                    : 0)
            });
        });

        $(".adittional-details .product-dropdown-item input[type=checkbox]:checked").each(function () {
            itensAdicionais.push({
                "key": $(this).attr('ref'),
                "quantidadeEscolha": $('input[type="number"]', $(this).parent().parent()).val() != "" ? $('input[type="number"]', $(this).parent().parent()).val().toString() : "0",
                "codigoBarras": $(this).attr('cod')
            });
        });

        if (!isCombo()) {
            $(".ingredient-details .product-dropdown-item input[type=checkbox]:checked").each(function () {
                ingredientesIniciais += $(this).attr('cod') + ';' + $('input[type="number"]', $(this).parent().parent()).val() + ';';
                listasIngredientes += $(this).attr('ref') + ';';
            });
        } else {
            $(".combo-products-item input[type=checkbox]:checked").each(function () {
                produtosCombo.push({
                    "codigoBarras": $(this).attr('cod'),
                    "nomeProduto": $(this).attr('val'),
                    "quantidade": parseInt($('input[type="number"]', $(this).parent().parent()).val()),
                    "valorDesconto": $('.value-remove input[type="text"]', $(this).parent().parent().parent()).val() != "" ? nmoney($('.value-remove input[type="text"]', $(this).parent().parent().parent()).val()) : "0"
                });
            });
        }

        var valor = nmoney($('#price-product-obj').val());

        var finalPromocao = '0';
        if ($('.value-input-added').is(':visible')) {
            var promocao = nmoney($('#promotion-product-obj').val());

            finalPromocao = (promocao / valor) * 100;
            valor = promocao;
        }

        var itens = [
            {
                "nomeProduto": $('#title-product-obj').val(),
                "valorInicial": valor.toString(),
                "categoria": $('#category-product-obj').text(),
                "promocao": finalPromocao.toString(),
                "fraseVenda": "",
                "estoque": 99999999,
                "ingredientesIniciais": ingredientesIniciais != '' ? ingredientesIniciais.slice(0, -1) : null,
                "listasIngredientes": listasIngredientes != '' ? listasIngredientes.slice(0, -1) : null,
                "medida": ($('#measure-product-obj').val() != '' ? $('#measure-product-obj').val() + ' ' + $('#unity-product-obj').attr('value') : null),
                "servePessoas": parseInt($('#serves-product-obj').val()),
                "produtosCombo": produtosCombo.length > 0 ? produtosCombo : null,
                "descricao": $('#description-product-obj').val(),
                categorias,
                "custom": true,
                "disponivel": $('#avaliable-product-obj').is(':checked') ? true : false,
                itensAdicionais
            }
        ];

        var base = '';
        if ($('.image-uploaded').attr('style').indexOf('url("') >= 0)
            base = $('.image-uploaded').attr('style').split('url("')[1].slice(0, -3);

        $.ajax({
            type: 'POST',
            url: 'registerProduct',
            cache: false,
            data: { product: JSON.stringify(itens), base: base },
            dataType: 'text'
        }).done(function (data) {
            if (data != 201)
                setTimeout(function () { window.location.href = "listar"; }, 3000);
            else
                setTimeout(function () { window.location.href = "account?personalization"; }, 3000);
        }).fail(function (data) {
            planList(data);
            $('.loading-registering').hide();
            opacity(false);
        });
    }
});

/* Verify if is combo */

function isCombo() {
    return $('.button-option .combo').hasClass('selected');
}

/* SLIDER HORIZONTAL FIXED INGREDIENTES*/

var lastCategory;
var category;
$('.ingredient-details .all-item').scroll(function () {
    if (!$('.ingredient-details .category-all').hasClass('selected')) {
        var bodyTop = $(this).offset().top;

        $(".ingredient-header").each(function () {
            if ($(this).offset().top > bodyTop) {
                return false;
            }
            category = $(this).attr('id');
        });

        if (category != lastCategory) {
            $('.ingredient-details .category').removeClass('selected');
            $('.ingredient-details .category[ref=' + category + ']').addClass('selected');

            var scroll = 0;
            $(".ingredient-details .category").each(function () {
                if ($(this).hasClass('selected'))
                    return false;

                scroll += $(this).width();
            });

            $('.ingredient-details .header-category').animate({ scrollLeft: scroll }, 200);
            lastCategory = category;
        }
    } else
        $('.ingredient-details .header-category').animate({ scrollLeft: 0 }, 200);
});

/* SLIDER HORIZONTAL FIXED ADICIONAIS*/

$('.adittional-details .all-item').scroll(function () {
    if (!$('.adittional-details .category-all').hasClass('selected')) {
        var bodyTop = $(this).offset().top;

        $(".header-adittional").each(function () {
            if ($(this).offset().top > bodyTop) {
                return false;
            }
            category = $(this).attr('id');
        });

        if (category != lastCategory) {
            $('.adittional-details .category').removeClass('selected');
            $('.adittional-details .category[ref=' + category + ']').addClass('selected');

            var scroll = 0;
            $(".adittional-details .category").each(function () {
                if ($(this).hasClass('selected'))
                    return false;

                scroll += $(this).width();
            });

            $('.adittional-details .header-category').animate({ scrollLeft: scroll }, 200);
            lastCategory = category;
        }
    } else
        $('.adittional-details .header-category').animate({ scrollLeft: 0 }, 200);
});

/* SLIDER HORIZONTAL FIXED PRODUCTS*/

$('.product-details .all-item').scroll(function () {
    if (!$('.product-details .category-all').hasClass('selected')) {
        var bodyTop = $(this).offset().top;

        $(".product-header").each(function () {
            if ($(this).offset().top > bodyTop) {
                return false;
            }
            category = $(this).attr('id');
        });

        if (category != lastCategory) {
            $('.product-details .category').removeClass('selected');
            $('.product-details .category[ref=' + category + ']').addClass('selected');

            var scroll = 0;
            $(".product-details .category").each(function () {
                if ($(this).hasClass('selected'))
                    return false;

                scroll += $(this).width();
            });

            $('.product-details .header-category').animate({ scrollLeft: scroll }, 200);
            lastCategory = category;
        }
    } else
        $('.product-details .header-category').animate({ scrollLeft: 0 }, 200);
});

/* Header adittional checkbox */

$(document).on('click', '.header-adittional-options input[type=checkbox]', function () {
    var parent = $(this).parent().parent().parent();
    var ref = $(this).attr('ref');

    if ($(this).is(':checked')) {
        $('.header-adittional-right', parent).css('opacity', 0);
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] .input-quantity').hide();
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] .product-dropdown-item-more p').hide();
    } else {
        $('.header-adittional-right', parent).css('opacity', 1);
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] .input-quantity').css('display', 'flex');
        $('.adittional-details .product-dropdown-item[ref="' + ref + '"] .product-dropdown-item-more p').show();
    }
});

/*Verify Promotion Input*/

function verifyPromotion() {
    var valor = nmoney($('#price-product-obj').val());
    var promocao = nmoney($('#promotion-product-obj').val());

    $('.value-input-added *').css('color', '#4D4D4D');
    $('#promotion-product-obj').css('border', '1px solid #DDDDDD');
    $('.promotion-error').hide();

    if (promocao != null && parseFloat(promocao) >= parseFloat(valor)) {
        $('.value-input-added *').css('color', '#B00020');
        $('#promotion-product-obj').css('border', '1px solid #B00020');
        $('.promotion-error').show();

        return false;
    } else
        return true;
}

/*Verify Category*/

function verifyCategory() {
    if ($('#category-product-obj').text() == '') {
        showMessageAlert('Crie uma categoria para o produto.', 'error');
        $('.category-dropdown').show();
        return false;
    } else
        return true;
}

/*Trash extra*/

$(document).on('click', '.trash-area', function () {
    $('.value-input-added *').css('color', '#4D4D4D');
    $('#promotion-product-obj').css('border', '1px solid #DDDDDD');
    $('#promotion-product-obj').val(null);
    $('.promotion-error').hide();
});

/*Selector Size*/

$(document).on('click', '.quantity .dropdown-item', function () {
    $('.selector-size p').text($(this).text());
    $('#unity-product-obj').attr('value', $(this).attr('value'));

    $('.product-preview .info .quantidade span').text($('.serves-quantity .quantity input[type=number]').val() + ' ' + $(this).attr('value'));
});

/*New Item Category*/

$(document).on('click', '.category-dropdown-new-item', function () {
    $('.new-item-default').hide();
    $('.new-item-insert').css('display', 'flex');
    $('.new-item-insert input[type="text"]').focus();
});

$(document).on('keypress', '.new-item-insert input[type=text]', function (e) {
    if (e.which == 13) {
        createNewCategory();
    }
});

$(document).on('click', '.new-item-insert .new-item-insert-save', function (e) {
    createNewCategory();
});

function createNewCategory() {
    if ($('.new-item-insert input[type=text]').val() != "") {
        var name = $('.new-item-insert input[type=text]').val();
        var nameFix = $('.new-item-insert input[type=text]').val().replace(/ /g, '').toLowerCase();
        $('.category-dropdown-new-item').after('<div class="category-dropdown-item" search="' + nameFix + '"> <p>' + name + '</p> <div class="area-custom-radio"> <input id="' + nameFix + '" type="radio" class="custom-radio" name="category" ref="' + name + '" checked> <label for="' + nameFix + '"></label> </div> </div>');
        $('.new-item-insert input[type=text]').val('');
        $('.category-selected').text(name);

        $('.new-item-insert').hide();
        $('.new-item-default').css('display', 'flex');

        $('.category-dropdown').fadeToggle(100);
    } else {
        showMessageAlert('Digite o nome da categoria.', 'error');
    }
}

/*Verify Reload*/

var canVerRefresh = false;
$(document).on("change input", "input", function () {
    canVerRefresh = true;
});
window.onbeforeunload = function (event) {
    if (!canRefresh && canVerRefresh)
        return confirm("Confirm refresh");
};