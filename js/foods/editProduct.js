if (isCombo())
    comboProduct();
else
    defaultProduct();

updateAdittionalPreview();

/*Register Product*/

var canRefresh = false;
$(document).on('click', '#edit-product', function () {
    if (validateForm() && verifyPromotion()) {
        canRefresh = true;

        $('.loading-registering').fadeIn(200);
        opacity(true);

        var codigoBarras = $(this).attr('cod');

        var categorias = [];
        var itensAdicionais = [];
        var ingredientesIniciais = '';
        var listasIngredientes = '';
        var itensAdicionais = [];
        var produtosCombo = [];

        $(".header-adittional.selected").each(function () {
            categorias.push({
                "id": $(this).attr('ref'),
                "obrigatorio": $(".header-adittional-options input[type=checkbox]", this).is(':checked'),
                "quantidade": ($(".header-adittional-options input[type=number]", this).val() != "" ? parseInt($(".header-adittional-options input[type=number]", this).val().toString()) : 0)
            });
        });

        $(".adittional-details .product-dropdown-item input[type=checkbox]:checked").each(function () {
            itensAdicionais.push({
                "key": $(this).attr('ref'),
                "quantidadeEscolha": $('input[type="number"]', $(this).parent().parent()).val() != "" ? $('input[type="number"]', $(this).parent().parent()).val().toString() : "0",
                "codigoBarras": $(this).attr('cod')
            });
        });

        if (!isCombo()) {
            $(".ingredient-details .product-dropdown-item input[type=checkbox]:checked").each(function () {
                ingredientesIniciais += $(this).attr('cod') + ';' + $('input[type="number"]', $(this).parent().parent()).val() + ';';
                listasIngredientes += $(this).attr('ref') + ';';
            });
        } else {
            $(".combo-products-item input[type=checkbox]:checked").each(function () {
                produtosCombo.push({
                    "codigoBarras": $(this).attr('cod'),
                    "nomeProduto": $(this).attr('val'),
                    "quantidade": parseInt($('input[type="number"]', $(this).parent().parent()).val()),
                    "valorDesconto": $('.value-remove input[type="text"]', $(this).parent().parent().parent()).val() != "" ? nmoney($('.value-remove input[type="text"]', $(this).parent().parent().parent()).val()) : "0"
                });
            });
        }

        var valor = nmoney($('#price-product-obj').val());

        var finalPromocao = '0';
        if ($('.value-input-added').is(':visible')) {
            var promocao = nmoney($('#promotion-product-obj').val());

            finalPromocao = (promocao / valor) * 100;
            valor = promocao;
        }

        var itens = [
            {
                "adicionais": "Lista" + codigoBarras + "PRDFL",
                "codigoBarras": codigoBarras,
                "nomeProduto": $('#title-product-obj').val(),
                "valorInicial": valor.toString(),
                "categoria": $('#category-product-obj').text(),
                "promocao": finalPromocao.toString(),
                "fraseVenda": "",
                "estoque": 99999999,
                "ingredientesIniciais": ingredientesIniciais != '' ? ingredientesIniciais.slice(0, -1) : null,
                "listasIngredientes": listasIngredientes != '' ? listasIngredientes.slice(0, -1) : null,
                "medida": ($('#measure-product-obj').val() != '' ? $('#measure-product-obj').val() + ' ' + $('#unity-product-obj').attr('value') : null),
                "servePessoas": parseInt($('#serves-product-obj').val()),
                "produtosCombo": produtosCombo.length > 0 ? produtosCombo : null,
                "descricao": $('#description-product-obj').val(),
                categorias,
                "custom": true,
                "disponivel": $('#avaliable-product-obj').is(':checked') ? true : false,
                itensAdicionais
            }
        ];

        var base = '';
        if ($('#image-product-obj').prop('files')[0] != null && $('.image-uploaded').attr('style').indexOf('url("') >= 0)
            base = $('.image-uploaded').attr('style').split('url("')[1].slice(0, -3);

        $.ajax({
            type: 'POST',
            url: 'updateProduct',
            cache: false,
            data: { product: JSON.stringify(itens), base: base },
            dataType: 'json'
        }).done(function (data) {
            setTimeout(function () { window.location.href = "listar"; }, 3000);
        }).fail(function (data) {
            $('.loading-registering').hide();
            opacity(false);
        });
    }
});

$(document).on('click', '#exclude-product', function () {
    $('.loading-registering-title p').text('Removendo o produto');
    $('.loading-registering-message').text('Aguarde enquanto o produto está sendo removido, não atualize a página durante o processo.');
    $('.loading-registering').fadeIn(200);
    opacity(true);

    $.post("excludeProduct", {
        cod: $(this).attr('ref')
    },
        function (data, status) {
            if (status == "success") {
                window.location.href = "listar";
            } else {
                $('.loading-registering').hide();
                opacity(false);
            }
        });
});

/*Verify Reload*/

var canVerRefresh = false;
$(document).on("change input", "input", function () {
    canVerRefresh = true;
});
window.onbeforeunload = function (event) {
    if (!canRefresh && canVerRefresh)
        return confirm("Confirm refresh");
};