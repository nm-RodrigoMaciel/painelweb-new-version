verifyEach();

/*Change Type Order*/

$(document).on('click', '.segmented-control div', function () {
    $('.kanban-order').show();
    $('.kanban-column').show();
    $('.search-bar input[type="text"]').val('');

    $('.segmented-control div').removeClass('selected');
    $(this).addClass('selected');

    if ($('.segmented-control div[type="delivery"]').hasClass('selected'))
        deliveryOrder();
    else if ($('.segmented-control div[type="menu"]').hasClass('selected'))
        menuOrder();

    var lenghtNew = $('.kanban-column:eq(0) .kanban-order:visible').length;
    $('.kanban-header-title .title-cont').text(lenghtNew);
    var lenghtNew = $('.kanban-column .kanban-order:visible').length;
    $('.ordening p:nth-child(1)').text(lenghtNew);
});

function menuOrder() {
    $('.kanban-order:not([type="mesa"])').hide();
    $('.kanban-column:eq(2)').hide();
}

function deliveryOrder() {
    $('.kanban-order:not(.order-delivery)').hide();
}

/*Print*/

$(document).on('change', '.status-filter .print input[type=checkbox]', function () {
    if ($(this).is(':checked'))
        $('.order-blink').show();
    else
        $('.order-blink').hide();
});

/*Ordening*/

$(document).on('click', '.ordering-item', function () {
    $('.ordening-button p').text($(this).text());
});

/*Back to top*/

$(document).on('click', '.kanban-back-top', function () {
    var parent = $(this).closest('.kanban-body');
    $(parent).scrollTop(0);
    $(this).hide();
});

var positionBackTop = 0;
$('.kanban-body').scroll(function () {
    var scrollBackTop = $(this).scrollTop();
    if (scrollBackTop > positionBackTop) {
        $('.kanban-back-top', this).css('display', 'flex');
    } else {
        if (scrollBackTop == 0)
            $('.kanban-back-top', this).hide();
    }
    positionBackTop = scrollBackTop;
});

/*Header options*/

$(document).on('click', '.edit-title-kanban', function () {
    var parent = $(this).closest('.kanban-header');
    $('.kanban-header-default', parent).hide();
    $('.kanban-header-search', parent).show();
    $('.kanban-header-search input[type=text]', parent).focus();
});

$(document).on('keypress', '.kanban-header-search input[type=text]', function (e) {
    var parent = $(this).closest('.kanban-header');
    var title = $('.kanban-header-default .title-text', parent).text();
    var newTitle = $(this).val();
    var position = $(this).attr('pos');

    if (e.which == 13) {
        if (newTitle != '') {
            $.ajax({
                type: 'POST',
                url: 'changeTitle',
                cache: false,
                data: { pos: position, title: newTitle },
                dataType: 'json'
            }).done(function (data) {
                if ($('.title-text-sub', parent).text() === '') {
                    $('.title-text-sub', parent).text('(' + title + ')');
                    $('.title-text-sub', parent).show();
                }

                $('.kanban-header-default .title-text', parent).text(newTitle)

                $('.kanban-header-search', parent).hide();
                $('.kanban-header-default', parent).show();

                $(this).val('');
            }).fail(function (data) {
                if (!planList(data)) {
                    $('.kanban-header-search', parent).hide();
                    $('.kanban-header-default', parent).show();
                    showMessageAlert("Não foi possível alterar o título desta coluna.", 'error')
                }
            });
        } else {
            $('.kanban-header-search', parent).hide();
            $('.kanban-header-default', parent).show();
        }
    }

});

$(document).on('click', '.restore-title-kanban', function () {
    var parent = $(this).closest('.kanban-header');
    var title = $('.kanban-header-default .title-text-sub', parent).text();
    var position = $(this).attr('pos');

    if (title != '') {
        $.ajax({
            type: 'POST',
            url: 'restoreTitle',
            cache: false,
            data: { pos: position },
            dataType: 'json'
        }).done(function (data) {
            $('.title-text-sub', parent).text('');
            $('.title-text-sub', parent).hide();
            $('.kanban-header-default .title-text', parent).text(title.substring(1, title.length - 1));
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert("Não foi possível restaurar o título desta coluna.", 'error')
        });
    }
});

/*Search*/

$(document).on('input', '.search-bar input[type="text"]', function () {
    searchSale()
});

function searchSale() {
    var text = $('.search-bar input[type="text"]').val().toLowerCase();
    var type = $('.segmented-control div.selected').attr('type');
    $('.kanban-order').hide();

    var search = '.kanban-order';
    search += (type != null ? '[type="' + type + '"]' : '');

    if (text != '') {
        $(search).each(function () {
            var name = $('.order-client-name', this).text().trim().toLowerCase();
            var cod = $('.order-cod', this).text().trim().toLowerCase();

            if (name.includes(text) || cod.includes(text))
                $(this).show();
        });
    } else {
        $(search).show();
    }
}

/*Click sales item*/

$(document).on('click', '.kanban-order', function () {
    $('.purchase-data .purchase-data-skeleton').show();
    $('.purchase-data .purchase-data-header, .purchase-data .purchase-data-body').remove();

    var key = $(this).attr('key');
    var type = $(this).attr('type');
    var telefone = $(this).attr('telefone') ?? '';
    $('.purchase-data').show(250);
    opacity(true);
    $('html').css('overflow', 'hidden');

    $.ajax({
        type: 'POST',
        url: 'getSaleByKey',
        cache: false,
        data: { key: key, type: type, telefone: telefone },
        dataType: 'text'
    }).done(function (data) {
        $('.purchase-data .purchase-data-skeleton').hide();
        $('.purchase-data').append(data);
    }).fail(function (data) {
        if (!planList(data))
            alert('Tivemos problemas ao tentar abrir esta venda.');
    });
});

$(document).on('click', '#opacity-body', function () {
    if (window.location.href.includes('vendas')) {
        $('.purchase-data .purchase-data-area').remove();
        $('.purchase-data').hide(250);
        opacity(false);
        $('html').css('overflow', 'auto');
    }
});

/* Print Sale */

$(document).on('click', '.purchase-data-header .right button:nth-child(1)', function () {
    var cod = $(this).parents('.purchase-data-area').attr('key');
    var tipo = $(this).parents('.purchase-data-area').attr('type');
    var telefone = $(this).parents('.purchase-data-area').attr('phone');

    $('.new-sales-area').addClass('sales');
    $('.new-sales').addClass('printing');
    $.ajax({
        type: 'POST',
        url: 'printSale',
        cache: false,
        data: {
            cod: cod,
            tipo: tipo,
            telefone: telefone
        },
        dataType: 'text'
    }).done(function (data) {
        $('.new-sales').append(data);
        html_pdf();
        $('.new-sales-area').removeClass('sales');
        $('.new-sales').removeClass('printing');
    }).fail(function (data) {
        if (!planList(data))
            location.reload();
    });
});


/* Cancel Order */

$(document).on('click', '#order-cancel', function () {
    alertCancelSale(
        $(this).parents('.purchase-data-area').attr('key'),
        $(this).parents('.purchase-data-area').attr('type'),
        $(this).parents('.purchase-data-area').attr('phone')
    );
});

function alertCancelSale(key, tipo, telefone = null) {
    opacityOnModal(true);
    $('body').after('<div class="order-cancel center-fix"> <div class="order-cancel-header"> <svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M11.46 0.146C11.3663 0.052531 11.2394 2.8119e-05 11.107 0L4.893 0C4.76065 2.8119e-05 4.63371 0.052531 4.54 0.146L0.146 4.54C0.052531 4.63371 2.8119e-05 4.76065 0 4.893L0 11.107C2.8119e-05 11.2394 0.052531 11.3663 0.146 11.46L4.54 15.854C4.63371 15.9475 4.76065 16 4.893 16H11.107C11.2394 16 11.3663 15.9475 11.46 15.854L15.854 11.46C15.9475 11.3663 16 11.2394 16 11.107V4.893C16 4.76065 15.9475 4.63371 15.854 4.54L11.46 0.146ZM8 4C8.535 4 8.954 4.462 8.9 4.995L8.55 8.502C8.53824 8.63977 8.4752 8.76811 8.37336 8.86164C8.27151 8.95516 8.13827 9.00705 8 9.00705C7.86173 9.00705 7.72849 8.95516 7.62664 8.86164C7.5248 8.76811 7.46176 8.63977 7.45 8.502L7.1 4.995C7.08743 4.86923 7.10134 4.74223 7.14084 4.62217C7.18035 4.5021 7.24456 4.39165 7.32934 4.29791C7.41413 4.20418 7.51761 4.12924 7.63312 4.07793C7.74863 4.02662 7.87361 4.00007 8 4ZM8.002 10C8.26722 10 8.52157 10.1054 8.70911 10.2929C8.89664 10.4804 9.002 10.7348 9.002 11C9.002 11.2652 8.89664 11.5196 8.70911 11.7071C8.52157 11.8946 8.26722 12 8.002 12C7.73678 12 7.48243 11.8946 7.29489 11.7071C7.10736 11.5196 7.002 11.2652 7.002 11C7.002 10.7348 7.10736 10.4804 7.29489 10.2929C7.48243 10.1054 7.73678 10 8.002 10Z" fill="#B00020" /> </svg> <p>Atenção</p> <svg class="order-cancel-close" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M7.6979 6L12 10.3021L10.3021 12L6 7.6979L1.6979 12L0 10.3021L4.3021 6L0 1.6979L1.6979 0L6 4.3021L10.3021 0L12 1.6979L7.6979 6Z" fill="#F16B24" /> </svg> </div> <div class="order-cancel-body"> <p> Tem certeza de que deseja cancelar o pedido? </p> <p> Detalhe o motivo do cancelamento </p> <textarea></textarea> <p> Máximo 400 caracteres </p> </div> <div class="order-cancel-footer"> <p> Desfazer </p> <p> Confirmar cancelamento </p> </div> </div>');

    $(document).on('click', '.order-cancel-footer p:nth-child(2)', function () {
        $('.order-cancel').hide();

        $('.loading-registering').fadeIn(200);
        opacity(true);

        if ($('.order-cancel textarea').val().length > 0) {
            $.ajax({
                type: 'POST',
                url: 'cancelSale',
                cache: false,
                data: { key: key, reason: $('.order-cancel textarea').val(), tipo: tipo, telefone: telefone },
                dataType: 'json'
            }).done(function (data) {
                location.reload();
            }).fail(function (data) {
                if (!planList(data)) {
                    alert('Tivemos problemas ao tentar cancelar esta venda.');
                    location.reload();
                }
            });
        } else {
            if (!planList(data))
                alert('Digite o motivo.');
        }
    });
}

$(document).on('click', '.order-cancel-close, .order-cancel-footer p:nth-child(1)', function () {
    $('.order-cancel').remove();
    opacityOnModal(false);
});

/* Change status order */

$(document).on('click', '#order-next', function () {
    var index = $(this).closest('.kanban-column').index();
    var ref = $(this).parents('.kanban-order');
    var id = $(ref).attr('key');
    var type = $(ref).attr('type');
    var telefone = $(ref).attr('telefone') ?? '';

    changeStatus(index, id, type, telefone);

    return false;
});

$(document).on('click', '.sales-progress-button', function () {
    var index = $(this).attr('index');
    var id = $(this).attr('key');
    var type = $(this).attr('type');
    var telefone = $(this).attr('telefone') ?? '';

    opacityOnModal(true);
    changeStatus(index, id, type, telefone);

    return false;
});

function changeStatus(index, id, type, telefone) {
    $('.loading-registering').fadeIn(200);
    opacity(true);
    $.ajax({
        type: 'POST',
        url: 'changeStatus',
        cache: false,
        data: { index: index, id: id, type: type, telefone: telefone },
        dataType: 'text'
    }).done(function (data) {
        location.reload();
    }).fail(function (data) {
        if (!planList(data)) {
            $('.loading-registering').hide();
            opacity(false);
            opacityOnModal(false);
            alert('Tivemos problemas ao tentar alterar o status desta venda.');
        }
    });
}

/*Order Cancelled Message*/

$(document).on("click", ".purchase-data-header-info.canceled", function () {
    opacityOnModal(true);
    $('.order-cancelled').appendTo('body');
    $('.order-cancelled').show();
});

$(document).on("click", ".order-cancelled-title svg:nth-child(3)", function () {
    opacityOnModal(false);
    $('.order-cancelled').appendTo('.purchase-data');
    $('.order-cancelled').hide();
});