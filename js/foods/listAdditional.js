verifyEach();

$(document).on('click', '.delete-category-header svg:nth-child(3), .delete-category-footer p:nth-child(1)', function () {
    $('.delete-category').remove();
    opacity(false);
});

$(document).on('click', '.more-options .dropdown-item:nth-child(2)', function () {
    opacity(true);
    alertDeleteCategory($(this).closest('.category-section-title').attr('cat'));
});

function alertDeleteCategoryText(cat) {
    if ($('.delete-category-text input[type="text"]').val().replace(/ /g, '').toLowerCase() == "sim,tenhocerteza") {
        $.post("deleteAdittional", {
            category: cat
        },
            function (data, status) {
                if (status == "success") {
                    opacity(false);
                    $('.delete-category').remove();
                    $('.category-section-title[cat="' + cat + '"], .see-itens-adittional[cat="' + cat + '"], .category[ref="' + cat.toLowerCase().replace(/\s+/g, '') + '"]').remove();
                    showMessageAlert('Categoria removida com sucesso!');
                } else {
                    showMessageAlert('Tivemos problemas ao tentar excluir a categoria de adicionais.', 'error');
                    setTimeout(function () { window.location.href = "adicionais" }, 2000);
                }
            });
    } else
        $('.delete-category-text').addClass('alert-field');
}

function alertDeleteCategory(title) {
    $('body').append('<div class="delete-category center-fix"> <div class="delete-category-header"> <svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"> <path d="M11.46 0.146C11.3663 0.052531 11.2394 2.8119e-05 11.107 0L4.893 0C4.76065 2.8119e-05 4.63371 0.052531 4.54 0.146L0.146 4.54C0.052531 4.63371 2.8119e-05 4.76065 0 4.893L0 11.107C2.8119e-05 11.2394 0.052531 11.3663 0.146 11.46L4.54 15.854C4.63371 15.9475 4.76065 16 4.893 16H11.107C11.2394 16 11.3663 15.9475 11.46 15.854L15.854 11.46C15.9475 11.3663 16 11.2394 16 11.107V4.893C16 4.76065 15.9475 4.63371 15.854 4.54L11.46 0.146ZM8 4C8.535 4 8.954 4.462 8.9 4.995L8.55 8.502C8.53824 8.63977 8.4752 8.76811 8.37336 8.86164C8.27151 8.95516 8.13827 9.00705 8 9.00705C7.86173 9.00705 7.72849 8.95516 7.62664 8.86164C7.5248 8.76811 7.46176 8.63977 7.45 8.502L7.1 4.995C7.08743 4.86923 7.10134 4.74223 7.14084 4.62217C7.18035 4.5021 7.24456 4.39165 7.32934 4.29791C7.41413 4.20418 7.51761 4.12924 7.63312 4.07793C7.74863 4.02662 7.87361 4.00007 8 4ZM8.002 10C8.26722 10 8.52157 10.1054 8.70911 10.2929C8.89664 10.4804 9.002 10.7348 9.002 11C9.002 11.2652 8.89664 11.5196 8.70911 11.7071C8.52157 11.8946 8.26722 12 8.002 12C7.73678 12 7.48243 11.8946 7.29489 11.7071C7.10736 11.5196 7.002 11.2652 7.002 11C7.002 10.7348 7.10736 10.4804 7.29489 10.2929C7.48243 10.1054 7.73678 10 8.002 10Z" fill="#B00020" /> </g> <defs> <clipPath id="clip0"> <rect width="16" height="16" fill="white" /> </clipPath> </defs> </svg> <p>Atenção</p> <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M9.6979 8L14 12.3021L12.3021 14L8 9.6979L3.6979 14L2 12.3021L6.3021 8L2 3.6979L3.6979 2L8 6.3021L12.3021 2L14 3.6979L9.6979 8Z" fill="var(--color-primary)" /> </svg> </div> <div class="delete-category-body"> <p> Você está prestes a <b>EXCLUIR</b> a categoria <b>' + title + '</b> e todos os produtos cadastrados nela.<br><br>Tem certeza desta ação?<br><br>Se sim, escreva ”Sim, tenho certeza” abaixo: </p> </div> <div class="delete-category-text"> <input type="text" placeholder="Frase de confirmação aqui"><p>Escreva para confirmar ou cancele a ação</p> </div> <div class="delete-category-footer"> <p>Cancelar</p> <p>Excluir</p> </div> </div>');

    $(document).on('click', '.delete-category-footer p:nth-child(2)', function () {
        alertDeleteCategoryText(title);
    });
}

/*Search Adittional*/

$(document).on('input', '.see-additional .search-bar input[type="text"]', function () {
    var ref_text = $('.see-additional-ordering > p');

    if ($(this).val() != '') {
        $('.title-area, .floating-header .header-category').hide();
        $('.category-section-title, .see-itens-adittional').hide();
        $('.see-itens-adittional[search*=' + $(this).val().toLowerCase().replace(/\s+/g, '') + ']').show();

        var qtd = $('.see-itens-adittional[search*=' + $(this).val().toLowerCase().replace(/\s+/g, '') + ']').length;
        if ($(ref_text).attr('old') == null || $(ref_text).attr('old') == '')
            $(ref_text).attr('old', $(ref_text).text());

        $(ref_text).text(qtd + ' resultados');
    } else {
        $('.title-area, .floating-header .header-category').css('display', 'flex');
        $('.category-section-title, .see-itens-adittional').show();

        $(ref_text).text($(ref_text).attr('old'));
        $(ref_text).attr('old', null);
    }
});

/*Change Type*/

$(document).on('click', '.button-option div', function () {
    $('.button-option div').removeClass('selected');
    $(this).addClass('selected');
});

/*Category*/

$(document).on('click', '.see-additional .category-section .category', function () {
    $(document).scrollTop($('#' + $(this).attr('ref')).offset().top - $('.floating-header').height());
});

/*New adittional*/

$(document).on('click', '.see-additional .new-adittional', function () {
    showRegisterAdittional('Cadastrar novo item adicional', 'register-adittional-item', '', $(this).attr('cat'));
});

$(document).on('click', '.register-adittional .register-adittional-footer p:nth-child(1)', function () {
    opacity(false);
    $('.register-adittional').hide();
});

/*Category dropdown inside new adittional*/

$(document).on('click', '.register-adittional .selector-category', function () {
    $('.category-dropdown').fadeToggle(100);
});

$(document).on('click', '.category-dropdown input[type="checkbox"]', function () {
    $('.category-selected').text($(this).attr('ref'));
    $('.category-dropdown').fadeToggle(100);
});

$(document).on('input', '.category-dropdown input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.category-dropdown-item').hide();
        $('.category-dropdown-item[search*=' + $(this).val().toLowerCase().replace(/\s+/g, '') + ']').show();
    } else {
        $('.category-dropdown-item').show();
    }
});

$(document).on('click', '.category-dropdown-footer p:nth-child(1)', function () {
    $('.category-dropdown').fadeToggle(100);
});

/* Register adittional */

$(document).on('click', '.register-adittional-footer .register-adittional-item', function () {
    if (validateForm()) {
        var adittional = [{
            "disponivel": $('#adittional-disponivel').is(':checked'),
            "categoria": $('#adittional-category').text(),
            "key": $('#adittional-category').text(),
            "nomeProduto": $('#adittional-title').val(),
            "quantidadeEscolha": "0",
            "valorInicial": ($('#adittional-price').val() == "" ? "0" : nmoney($('#adittional-price').val())),
            "estoque": 999999
        }];

        $.ajax({
            type: 'POST',
            url: 'registerAdittional',
            cache: false,
            data: { itens: JSON.stringify(adittional) },
            dataType: 'json'
        }).done(function (data) {
            opacity(false);
            $('.register-adittional').fadeToggle(100);
            showMessageAlert("Adicional cadastrado.");
            window.location.href = "adicionais";
        }).fail(function (data) {
            opacity(false);
            $('.register-adittional').fadeToggle(100);
            showMessageAlert("Tivemos um problema ao tentar cadastrar o adicional.", "error");
        });
    }
});

function showRegisterAdittional(title, type, nome = '', categoria = '', valor = '', keypush = '', codbarras = '', disponivel = true) {
    $('.register-adittional #adittional-category').text(categoria);
    $('.register-adittional #adittional-title').val(nome);
    $('.register-adittional #adittional-price').val(valor);
    $('.register-adittional #adittional-keypush').val(keypush);
    $('.register-adittional #adittional-codbarras').val(codbarras);
    $('.register-adittional #adittional-disponivel').prop('checked', disponivel);
    $('.register-adittional #adittional-oldcategory').val(categoria);

    $('.exclude-adittional').hide();
    if (type == 'edit-adittional-item')
        $('.exclude-adittional').show();

    opacity(true);
    $('.register-adittional .register-adittional-title p:nth-child(1)').text(title);
    $('.register-adittional-footer p:nth-child(2)').removeClass();
    $('.register-adittional-footer p:nth-child(2)').addClass(type);
    $('.register-adittional').show();
}

/* New Category */

function openCategoryAdittional(title) {
    opacity(true);
    $('html').append('<div class="register-category center-fix"> <p class="register-category-title">' + title + '</p> <div class="register-category-fields"> <p>Título da categoria</p> <input class="fieldNotEmpty" type="text" maxlength="28"> <p class="register-category-description textDefaultNotEmpty">Após cadastrar você poderá inserir ou copiar itens na nova categoria</p><p class="register-category-description alertTextNotEmpty" style="display:none">Insira um título</p> </div> <div class="register-category-footer"> <p>Cancelar</p> <a class="new-category">Salvar categoria</a> </div> </div>');
}

$(document).on('click', '.see-additional .add-category', function () {
    openCategoryAdittional('Nova categoria de adicionais');
});

$(document).on('click', '.register-category .new-category', function () {
    var categoria = $('.register-category input[type=text]').val();

    opacity(false);
    $('.register-category').remove();
    showRegisterAdittional('Cadastrar novo item adicional', 'register-adittional-item', '', categoria);
});

$(document).on('keypress', '.register-category input[type=text]', function (e) {
    if (e.which == 13) {
        $('.register-category .new-category').click();
    }
});

/* Edit adittional */

$(document).on('click', '.register-adittional-footer .edit-adittional-item', function () {

    var adittional = [{
        "codigoBarras": $('#adittional-codbarras').val(),
        "disponivel": $('#adittional-disponivel').is(':checked'),
        "categoria": $('#adittional-category').text(),
        "keyPush": $('#adittional-keypush').val(),
        "nomeProduto": $('#adittional-title').val(),
        "valorInicial": ($('#adittional-price').val() != "" ? nmoney($('#adittional-price').val()) : "0"),
        "estoque": 999999
    }];

    $('.register-adittional').fadeToggle(100);

    $.ajax({
        type: 'POST',
        url: 'editAdittional',
        cache: false,
        data: { itens: JSON.stringify(adittional), cat: $('#adittional-oldcategory').val() },
        dataType: 'json'
    }).done(function (data) {
        opacity(false);
        showMessageAlert("Adicional cadastrado.");
        window.location.href = "adicionais";
    }).fail(function (data) {
        opacity(false);
        $('.register-adittional').fadeToggle(100);
        showMessageAlert("Tivemos um problema ao tentar atualizar o adicional.", "error");
    });
});

/* Edit Adittional */

$(document).on('click', '.edit-adittional', function () {
    var ref = $(this).parent().parent();

    var nome = $('.title', ref).text();
    var valor = $('.value-obj', ref).val();
    var categoria = $(ref).attr('cat');
    var keypush = $(ref).attr('key');
    var codbarras = $(ref).attr('cod');
    var disponivel = $('.enabled-obj', ref).is(':checked');

    showRegisterAdittional('Editar item adicional', 'edit-adittional-item', nome, categoria, valor, keypush, codbarras, disponivel);
});

/*Orderning*/

var current;
$(document).on('click', '.ordering-item', function () {

    $('.ordering').text($(this).text());

    var pos = $(this).index();
    $(".category-section-title").each(function (index) {
        var cat = $(this).attr("cat");
        $(".see-itens-adittional[cat='" + cat + "']").each(function (index) {
            current = $(this);
            switch (pos) {
                case 0:
                    $(".see-itens-adittional[cat='" + cat + "']").each(function (index) {
                        if ($('.title', current).text() < $('.title', this).text()) {
                            $(this).before(current);
                            return false;
                        } else
                            $(this).after(current);
                    });
                    break;
                case 1:
                    $(".see-itens-adittional[cat='" + cat + "']").each(function (index) {
                        if (parseFloat($('.value-obj', current).val()) < parseFloat($('.value-obj', this).val())) {
                            $(this).before(current);
                            return false;
                        } else
                            $(this).after(current);
                    });
                    break;
                case 2:
                    $(".see-itens-adittional[cat='" + cat + "']").each(function (index) {
                        if (parseFloat($('.value-obj', current).val()) > parseFloat($('.value-obj', this).val())) {
                            $(this).before(current);
                            return false;
                        } else
                            $(this).after(current);
                    });
                    break;
                case 3:
                    $(".see-itens-adittional[cat='" + cat + "']").each(function (index) {
                        if ($('.enabled-obj', current).is(':checked'))
                            $(this).after(current);
                    });
                    break;
                case 4:
                    $(".see-itens-adittional[cat='" + cat + "']").each(function (index) {
                        if (!$('.enabled-obj', current).is(':checked'))
                            $(this).after(current);
                    });
                    break;
            }
        });
    });
});

/*Fast Update*/

var timer;
$(document).on('keyup', '.see-itens-adittional input[type="text"]', function () {
    if (timer != null)
        clearTimeout(timer);

    var element = $(this).closest('.see-itens-adittional');
    timer = setTimeout(function () { fastUpdate(element) }, 2000);
});

$(document).on('click', '.see-itens-adittional .enabled-obj', function () {
    var cat = $(this).attr('cat');
    var checked = $(this).is(':checked');
    var element = $(this).closest('.see-itens-adittional');

    if (checked) {
        $('.category-section-title[cat="' + cat + '"] .category-switch').prop('checked', true);
        $('.category-section-title[cat="' + cat + '"] .title-disponibility').text('Disponível');
    }
    else {
        if ($('.see-itens-adittional .enabled-obj[cat="' + cat + '"]:checked').length < 1) {
            $('.category-section-title[cat="' + cat + '"] .category-switch').prop('checked', false);
            $('.category-section-title[cat="' + cat + '"] .title-disponibility').text('Indisponível');
        }
    }

    fastUpdate(element);
});

function fastUpdate(element) {
    var itens = [{
        "valorInicial": nmoney($('.value-obj', element).val()),
        "disponivel": $('.enabled-obj', element).is(':checked'),
        "keyPush": $(element).attr('key')
    }];

    $.ajax({
        type: 'POST',
        url: 'fastUpdateAdittional',
        cache: false,
        data: { fastUpdate: JSON.stringify(itens) },
        dataType: 'json'
    }).done(function (data) {
        showMessageAlert("Adicional atualizado.");
    }).fail(function (data) {
        alert("Tivemos problemas para atualizar seu adicional.");
    });
}

/* Exclude Adittional */

$(document).on('click', '.register-adittional .exclude-adittional', function () {
    var category = $('#adittional-category').text();
    var codbarras = $('#adittional-codbarras').val();

    $.ajax({
        type: 'POST',
        url: 'excludeAdittional',
        cache: false,
        data: { key: category, cod: codbarras },
        dataType: 'text'
    }).done(function (data) {
        opacity(false);
        $('.register-adittional').fadeToggle(100);
        showMessageAlert("Adicional excluido.");
        $('.see-itens-adittional[cod="' + codbarras + '"]').remove();
    }).fail(function (data) {
        alert('Tivemos problemas ao tentar excluir o adicional.');
    });
});

/* Desativating category */

$(document).on('click', '.category-section-title .category-switch', function () {
    var cat = $(this).attr('cat');
    var checked = $(this).is(':checked');

    $.post("updateCategoriesAdittional", {
        category: cat,
        availability: checked
    },
        function (data, status) {
            if (status == "success") {
                $('.see-itens-adittional[cat="' + cat + '"] .enabled-obj').prop('checked', checked);
                $('.see-itens-adittional[cat="' + cat + '"] .title-disponibility').text(checked ? 'Disponível' : 'Indisponível');
            } else {
                alert('Algo aconteceu, contacte o administrador');
            }
        });

});

/* SLIDER HORIZONTAL FIXED */

var lastCategory;
var category;
var positionSearch = $(window).scrollTop();
$(document).scroll(function () {

    if ($(window).scrollTop() + 100 + $(window).height() >= $(document).height()) {
        category = $(".category-section-title .title").last().attr('id');

        $('.category-section .category').removeClass('selected');
        $('.category-section .category[ref=' + category + ']').addClass('selected');

        lastCategory = category;
    } else {
        if ($(document).scrollTop() < 270) {
            $('.floating-header').removeClass('floating');
            $('.floating-search').removeClass('floating');
            $('.floating-header').removeClass('show-search');
        } else {
            $('.floating-header').addClass('floating');

            var scrollSearch = $(window).scrollTop();
            if (scrollSearch > positionSearch) {
                $('.floating-search').removeClass('floating');
                $('.floating-header').removeClass('show-search');
            } else {
                $('.floating-search').addClass('floating');
                $('.floating-header').addClass('show-search');
            }
            positionSearch = scrollSearch;
        }

        var position = $(document).scrollTop();

        $(".category-section-title .title").each(function () {
            if (position + 110 < $(this).offset().top) {
                return false;
            }
            category = $(this).attr('id');
        });

        if (category != lastCategory) {
            $('.category-section .category').removeClass('selected');
            $('.category-section .category[ref=' + category + ']').addClass('selected');

            var scroll = 0;
            $(".category-section .category").each(function () {
                if ($(this).hasClass('selected'))
                    return false;

                scroll += $(this).width();
            });

            $('.category-section').animate({ scrollLeft: scroll }, 50);

            lastCategory = category;
        }
    }
});

/* SLIDER HORIZONTAL FIXED COPY ADITTIONAL*/

$('.copy-adittional .all-item').scroll(function () {
    var bodyTop = $(this).offset().top;

    $(".adittional-header").each(function () {
        if ($(this).offset().top > bodyTop) {
            return false;
        }
        category = $(this).attr('id');
    });

    if (category != lastCategory) {
        $('.copy-adittional-categories .category').removeClass('selected');
        $('.copy-adittional-categories .category[ref=' + category + ']').addClass('selected');

        var scroll = 0;
        $(".copy-adittional-categories .category").each(function () {
            if ($(this).hasClass('selected'))
                return false;

            scroll += $(this).width();
        });

        $('.copy-adittional-categories').animate({ scrollLeft: scroll }, 200);

        lastCategory = category;
    }
});

/*COPY ADITTIONAL*/

$(document).on('click', '.duplicate-adittional', function () {
    var adittionalRef = $(this).attr('ref');

    opacity(true);
    $('.copy-adittional').fadeToggle(100);

    $(document).on('click', '.copy-adittional .product-dropdown-buttons p:nth-child(2)', function () {
        var adittional = [];

        $(".copy-adittional .product-dropdown-item input[type=checkbox]:checked").each(function () {
            adittional.push({
                "codigoBarras": $(this).attr('cod'),
                "categoria": $(this).attr('ref')
            });
        });

        $.ajax({
            type: 'POST',
            url: 'copyAdittionalCategory',
            cache: false,
            data: { itens: JSON.stringify(adittional), category: adittionalRef },
            dataType: 'text'
        }).done(function (data) {
            window.location.href = "adicionais";
        }).fail(function (data) {
            alert('Tivemos problemas ao tentar copiar os adicionais.');
            $('.copy-adittional').fadeToggle(100);
            opacity(false);
        });
    });
});

$(document).on('click', '.copy-adittional .product-dropdown-buttons p:nth-child(1)', function () {
    $('.copy-adittional').fadeToggle(100);

    $('.copy-adittional input[type="checkbox"]').prop('checked', false);
    $('.copy-adittional .category-all').remove();
    $('.copy-adittional .header-category').css('margin-left','0');

    opacity(false);
    adittionalRef = null;
});

$(document).on('input', '.copy-adittional .search-area input[type="text"]', function () {
    if ($(this).val() != '') {
        $('.copy-adittional .adittional-header').hide();
        $('.copy-adittional .product-dropdown-item').hide();
        $('.copy-adittional .product-dropdown-item[search*=' + $(this).val().replace(/ /g, '').toLowerCase() + ']').css('display', 'flex');
    } else {
        $('.copy-adittional .adittional-header').show();
        $('.copy-adittional .product-dropdown-item').show();
    }
});

/* Adittional selecteds */

$(document).on('click', '.copy-adittional input[type=checkbox]', function () {
    updateCopyAdittionalDropdown();
});

function updateCopyAdittionalDropdown() {
    $(".copy-adittional .header-category .category-all").remove();
    $(".copy-adittional .header-category").css('margin-left', '0');

    var cont = 0;
    $(".copy-adittional input[type=checkbox]:checked").each(function () {
        cont++;
    });

    if (cont > 0) {
        $("<div class='category-all'><span>Selecionados (" + cont + ")</span></div>").prependTo(".copy-adittional .header-category");
        $(".copy-adittional .header-category").css('margin-left', '149px');
    }
}

/* Category Dropdown Copy Adittional */

$(document).on('click', '.copy-adittional .category', function () {
    var ref = $(this).parent().parent();

    $('.category-all', ref).removeClass('selected');
    $('.popup-body', ref).scroll();

    $('.copy-adittional .adittional-header').show();
    $('.copy-adittional .product-dropdown-item').show();
    var topPos = $('.copy-adittional .all-item').offset().top;
    $('.copy-adittional .all-item').scrollTop(topPos);
    var position = $('.copy-adittional #' + $(this).attr('ref')).offset().top;
    $('.copy-adittional .all-item').scrollTop(position);
});

/*Not special caracter*/

$(document).on('input', '.register-category-fields input[type="text"]', function () {
    if (!hasSpecialCharacter($(this).val()))
        $(this).val($(this).val().slice(0, -1));
});

function hasSpecialCharacter(val) {
    return /^[a-õ-zA-Z0-9- ]*$/.test(val);
}

/*Header switch*/

$(".category-section-title").each(function () {
    var ref = $(this).attr('ref');
    var has = false;
    $(".see-itens-adittional[ref=" + ref + "] .cl-switch input").each(function () {
        if ($(this).is(":checked"))
            has = true;
    });
    if (has)
        $('.title-disponibility', this).text('Disponível');
    else
        $('.title-disponibility', this).text('Indisponível');
});
