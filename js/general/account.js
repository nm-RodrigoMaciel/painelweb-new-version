/*--------------------------Account General--------------------------*/

$(document).ready(function () {

    /*Date/Time Picker*/
    $(document).on('click', '.timepicker, .datepicker', function () {
        ref_tab = $('.tabs-main:visible');
    });
    $(document).on('click', '.timepicker-done', function () {
        if ($(ref_tab).hasClass('tabs-main-coupon')) {

            var time = $('.timepicker').val();
            $(ref_time).text('');
            $(ref_time).append('<h5>' + time + '</h5><h5>Editar</h5>');
            opacityOnModal(false);

            var timestamp = parseInt(time.split(':')[0]) * 3600;
            timestamp += parseInt(time.split(':')[1]) * 60;

            $(ref_time).attr('timestamp', timestamp * 1000);

        } else if ($(ref_tab).hasClass('tabs-main-delivery')) {

            var time = $('.timepicker').val();
            $(ref_time).val('');
            $(ref_time).val(time);
            opacityOnModal(false);

            var timestamp = parseInt(time.split(':')[0]) * 3600;
            timestamp += parseInt(time.split(':')[1]) * 60;

            $(ref_time).attr('timestamp', timestamp * 1000);
            verifyDeliveryPicker(ref_time);

        }
    });
    /*Timepicker Clear*/
    $(document).on('click', '.timepicker-clear', function () {
        if ($(ref_tab).hasClass('tabs-main-coupon')) {

            $(ref_time).text('');
            $(ref_time).append('<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M8 3.5C8 3.36739 7.94732 3.24021 7.85355 3.14645C7.75979 3.05268 7.63261 3 7.5 3C7.36739 3 7.24021 3.05268 7.14645 3.14645C7.05268 3.24021 7 3.36739 7 3.5V9C7.00003 9.08813 7.02335 9.17469 7.06761 9.25091C7.11186 9.32712 7.17547 9.39029 7.252 9.434L10.752 11.434C10.8669 11.4961 11.0014 11.5108 11.127 11.4749C11.2525 11.4391 11.3591 11.3556 11.4238 11.2422C11.4886 11.1288 11.5065 10.9946 11.4736 10.8683C11.4408 10.7419 11.3598 10.6334 11.248 10.566L8 8.71V3.5Z" fill="#E4002B"></path> <path d="M8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16ZM15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8Z" fill="#E4002B"></path> </svg> Definir horário');
            $(ref_time).attr('timestamp', '0');

        } else if ($(ref_tab).hasClass('tabs-main-delivery')) {

            $(ref_time).val('');
            $(ref_time).attr('timestamp', '0');
            verifyDeliveryPicker(ref_time);

        }
    });
    $(document).on('click', '.datepicker-done', function () {
        var date = $('.datepicker').val();
        var dia = date.split('/')[1];
        dia = addZero(dia);
        var mes = parseInt(date.split('/')[0]) + 1;
        mes = addZero(mes);
        var ano = date.split('/')[2];
        var diadasemana = date.split('/')[3];

        if ($(ref_tab).hasClass('tabs-main-coupon')) {

            //Ação a ser realizada na tela de cupom
            ano = ano.substr(2);
            var timestamp = Date.parse(mes + '/' + dia + '/' + ano + " 00:00");

            if ($(ref_date).is("#coupon_data_inicio")) {
                var timestamp_fim = parseInt($('#coupon_data_fim').attr('timestamp'));
                if (timestamp_fim > 0 && timestamp > timestamp_fim) {
                    alert('Insira um período válido.');
                    $(ref_date).click();
                    return;
                }
            } else if ($(ref_date).is("#coupon_data_fim")) {
                var timestamp_inicio = parseInt($('#coupon_data_inicio').attr('timestamp'));
                if (timestamp < timestamp_inicio) {
                    alert('Insira um período válido.');
                    $(ref_date).click();
                    return;
                }
            }

            $(ref_date).val(dia + '/' + mes + '/' + ano);
            $(ref_date).attr('timestamp', timestamp);

        } else if ($(ref_tab).hasClass('tabs-main-delivery')) {

            //Ação a ser realizada na tela de horarios especiais
            var ref = $('.delivery-special-add-item');
            $.ajax({
                type: 'POST',
                url: 'new-special-hours',
                cache: false,
                data: {
                    semana: diadasemana,
                    dia: dia,
                    mes: mes,
                    ano: ano,
                    abre: $('#abre_especial').val(),
                    fecha: $('#fecha_especial').val()
                },
                dataType: 'text'
            }).done(function (data) {
                $(ref).before(data);
            }).fail(function (data) {
                if (!planList(data))
                    showMessageAlert(data.responseText, 'error');
            });

        }

        opacityOnModal(false);
    });
    /*Datepicker Clear*/
    $(document).on('click', '.datepicker-clear', function () {
        if ($(ref_tab).hasClass('tabs-main-coupon')) {
            $(ref_date).val('');
            $(ref_date).attr('timestamp', '0');
        } else if ($(ref_tab).hasClass('tabs-main-delivery')) {

            alert();

        }

        $('.datepicker-cancel').click();
    });

    /*Verify Reload*/
    $(document).on("change input", ".tabs-main-information input, .tabs-main-information textarea", function () {
        canRefresh = true;
        $('.save-changes').css('visibility', 'visible');
    });
    $(document).on("click", ".tabs-main-information .color-circle", function () {
        canRefresh = true;
        $('.save-changes').css('visibility', 'visible');
    });
    window.onbeforeunload = function (event) {
        if (canRefresh)
            return confirm("Confirm refresh");
    };

    /*Enable Store*/
    $(document).on('click', '.side-menu-info .enabled-obj', function () {
        var enable = $(this).is(':checked');
        $.ajax({
            type: 'POST',
            url: 'enable-store',
            cache: false,
            data: {
                enable: enable
            },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Estabelecimento ' + (enable ? 'disponível' : 'indisponível') + '.');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar atualizar o status.', 'error');
        });
    });

    /*Tabs*/
    $(document).on('click', '.tabs-categories-item', function () {
        if ($(this).index() == 0) {
            $(".form-header").prependTo(".form");
        } else if ($(this).index() == 1) {
            $(".form-header").prependTo(".preview-left");
        }
    });

    /*Side Menu*/
    $(document).on('click', '.side-menu-item', function () {
        $('.side-menu-item').removeClass('selected');
        $(this).addClass('selected');
        $('.tabs-main').hide();
        $('.tabs-main:eq(' + ($(this).index() - 1) + ')').show();
    });

    /*Inicializando o DataPicker e TimePicker*/
    $(".datepicker").datepicker();
    $(".is-today").addClass("is-selected");
    $(".timepicker").timepicker();
    /*Definindo hora*/
    $(document).on('click', '.new-coupon-right-validity div:nth-child(2) p', function () {
        ref_time = this;
        $('.timepicker').click();
        opacityOnModal(true);
    });
    /*Definindo data*/
    $(document).on('click', '.new-coupon-right-validity div:nth-child(2) input', function () {
        ref_date = this;
        $('.datepicker').click();
        opacityOnModal(true);
    });
    /*Remover opacidade ao cancelar dos pickers*/
    $(document).on('click', '.datepicker-cancel, .timepicker-close, .datepicker-clear, .timepicker-clear ', function () {
        opacityOnModal(false);
    });

    /*-----------------------------------------------------------*/

    /*--------------------------Info Area--------------------------*/

    function rgb2hex(rgb) {
        var hexadecimal = "#";
        $.each(rgb, function (i, x) {
            x = parseInt(x);
            var hex = x.toString(16)
            hexadecimal += hex.length === 1 ? '0' + hex : hex;
        });
        return hexadecimal;
    }

    function fillColors(colorThief, img) {
        var rgb = colorThief.getPalette(img);

        $('.palette-colors div').remove();

        $(rgb).each(function (index, value) {
            var cor = rgb2hex(value);
            var selected = (cor == $('#color-primary').val().toLowerCase() ? ' selected' : '');
            $('.palette-colors').append('<div class="color-area"><div class="color-circle' + selected + '" style="background-color:' + cor + '"></div></div>');
        });

        $('.form-header .header-img img').css({
            height: 200,
            width: 200
        });

        if ($('#img-profile').hasClass('added'))
            $('.color-area:nth-child(2) .color-circle').click();
    }

    function changeColor() {
        const colorThief = new ColorThief();
        const img = document.getElementById('img-profile');

        $('.form-header .header-img img').css({
            height: 96,
            width: 96
        });

        if (img.complete) {
            fillColors(colorThief, img);
        } else {
            img.addEventListener('load', function () {
                fillColors(colorThief, img);
            });
        }
    }

    $(document).on('click', '.color-circle', function () {
        $('.color-circle').removeClass('selected');
        $(this).addClass('selected');

        var color = $(this).attr("style").split('background-color:')[1];

        document.documentElement.style.setProperty('--color-primary', color);
        document.documentElement.style.setProperty('--color-primary-fifty', color + '50');
        document.documentElement.style.setProperty('--color-primary-ten', color + '10');
        document.documentElement.style.setProperty('--color-primary-effects', color + '27');
    });

    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-profile').addClass('added');
                $('#img-profile').attr('src', e.target.result);
                $('.phone-body-header img').attr('src', e.target.result);
                $('header img').attr('src', e.target.result);
                $('.side-menu-info img').attr('src', e.target.result);

                var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
                link.href = e.target.result;
                document.getElementsByTagName('head')[0].appendChild(link);
                changeColor();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '.file-upload', function () {
        readURL(this);
    });

    $(document).on('click', '#img-profile, .img-default', function () {
        $('.file-upload').click();
    });

    var MaskFone = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000 0000' : '(00) 0000 00009';
    },
        Options = {
            onKeyPress: function (val, e, field, options) {
                field.mask(MaskFone.apply({}, arguments), options);
            }
        };
    $('#fone').mask(MaskFone, Options);

    function clipboard(text) {
        navigator.clipboard.writeText(text);
        showMessageAlert('Copiado para área de transferência');
    }

    $(document).on('click', '.form-disabled svg', function () {
        clipboard($('p', $(this).parent()).text());
    });

    $(document).on('click', '.form-disabled:not(.editing) h5', function () {
        var ref = $(this).parents();
        $(ref).addClass('editing');
        $('input[type=text]', ref).prop('disabled', false);
        $(ref).prev().addClass('editing');
        $('input[type=text]', ref).focus();
    });

    $(document).on('click', '.form-disabled.editing:not(.new) h5', function () {
        var ref = $(this).parents();
        $(ref).removeClass('editing');
        $('input[type=text]', ref).prop('disabled', true);
        $(ref).prev().removeClass('editing');
    });

    $(document).on('click', '.save-changes:not([disabled])', function () {
        canRefresh = false;

        $('.save-changes').attr('disabled', true);

        var telefone = $('#fone').val();
        var endereco = $('#address').val();
        var retirada = $('.form-title-available .enabled-obj').is(':checked');
        var cor = getComputedStyle(document.body).getPropertyValue('--color-primary');
        var image = $('#img-profile').hasClass('added') ? $('#img-profile').attr('src') : null;
        var paletteColors = '';
        $('.color-area').each(function () {
            paletteColors += $('.color-circle', this).attr('style').split('background-color:')[1] + ';';
        });

        $.ajax({
            type: 'POST',
            url: 'customize',
            cache: false,
            data: {
                telefoneWhatsApp: telefone,
                colorPrimary: cor,
                image: image,
                endereco: endereco,
                retirada: retirada,
                paletteColors: paletteColors
            },
            dataType: 'text'
        }).done(function (data) {
            if (data === "200") {
                showMessageAlert('Conta atualizada com sucesso!');
                $('.save-changes').attr('disabled', false);
                $('.save-changes').css('visibility', 'hidden');
            } else if (data === "201") {
                showMessageAlert('Conta atualizada com sucesso!');
                setTimeout(function () {
                    $('.save-changes').attr('disabled', false);
                    $('.save-changes').css('visibility', 'hidden');
                    location.reload(true);
                }, 3000);
            } else {
                showMessageAlert('Tivemos um problema ao tentar alterar a conta', 'error');
                $('.save-changes').attr('disabled', false);
            }
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar alterar a conta', 'error');
            $('.save-changes').attr('disabled', false);
        });
    });

    /*-----------------------------------------------------------*/

    /*--------------------------Plan Management Area-----------------*/

    $(document).on('click', '.next-bill-action button, .planInfo-title button', function () {
        $('.planInfo-area').addClass('opened');
    });

    $(document).on('click', '.planInfo-area.opened:not(.changing) .planInfo-title button', function () {
        $('.planInfo-area').removeClass('opened');
    });

    $(document).on('click', '.planInfo-area.changing .planInfo-title button, .payment-add-cards-cancel button:last-child', function () {
        $('.planInfo-area').removeClass('changing');
        $('.plan.new').removeClass('new');
        $('.planInfo-area').removeClass('newPayment');
    });

    $(document).on('click', '.plan-payment-info button', function () {
        $('.planInfo-area').addClass('newPayment');
    });

    $(document).on('click', '.planInfo-area:not(.opened):not(.changing) .payment-add-cancel', function () {
        $('.planInfo-area').removeClass('newPayment');
    });

    $(document).on('click', '.plan-button', function () {
        var key = $('.plan-column .plan.selected').attr('key');
        if (key != "completoKey") {
            window.scrollTo(0, 0);
            $('.planInfo-area').addClass('changing');
            $(this).parents('.plan').addClass('new');
        } else {
            opacity(true);
            $('.change-plan-modal').show();
        }
    });

    //Botão de alterar metodo pagamento
    $(document).on('click', '.planInfo-area:not(.changing) .payment-add-save', function () {
        $('.payment-add-save').prop('disabled', true);
        var key = $('.plan-column .plan.new').attr('key');
        var card = {};
        $('.payment-add input[type="text"]', ref).each(function () {
            card[$(this).attr('id')] = $(this).val();
        });
        $.ajax({
            type: 'POST',
            url: 'planPayment',
            cache: false,
            data: {
                key: key,
                card: card,
                img: $('.payment-add-form img').attr('flag')
            },
            dataType: 'text'
        }).done(function (data) {
            window.scrollTo(0, 0);
            $('.payment-add-save').prop('disabled', false);
            showMessageAlert('Cartão alterado com sucesso!');
            $('.payment-add input[type="text"]').val('');
            $('.plan-payment-info').remove()
            $('.payment-has').append(data);
            $('.planInfo-area').removeClass('newPayment');
            $('.planInfo-area').addClass('hasPayment');
        }).fail(function (data) {
            window.scrollTo(0, 0);
            $('.payment-add-save').prop('disabled', false);
            if (!planList(data))
                showMessageAlert(data.responseText, 'error');
        });
    });

    //Botão de contratar plano
    $(document).on('click', '.planInfo-area.changing .payment-add-save', function () {
        var key = $('.plan-column .plan.new').attr('key');
        var card = {};
        $('.payment-add input[type="text"]', ref).each(function () {
            card[$(this).attr('id')] = $(this).val();
        });
        planChange(key, card)
    });

    //Modal novo plano
    $(document).on('click', '.change-plan-modal div button:nth-child(1)', function () {
        opacity(false);
        $('.change-plan-modal').hide();
    });

    $(document).on('click', '.change-plan-modal div button:nth-child(2)', function () {
        var key = $('.plan-column .plan.new').attr('key');
    });

    function planChange(key, card) {
        $('.payment-add-save').prop('disabled', true);
        $('.plan-error').hide();
        $.ajax({
            type: 'POST',
            url: 'planChange',
            cache: false,
            data: {
                key: key,
                card: card
            },
            dataType: 'json'
        }).done(function (data) {
            $('.payment-add-save').prop('disabled', false);
            $('.plan-payment-method').after(data.append);
            $('.plan-column .plan.selected').removeClass('selected');
            $('.plan-column .plan.new').addClass('selected').removeClass('new');
            $('.side-menu-item div:last-child').text(data.days + ' dias restantes');
            $('.planInfo-area').append(data.button);

            $('.next-bill-action, .payment-has, .payment-add-save, .plan-cancel').hide();
            $('.planInfo-area').attr('class', 'planInfo-area hasPayment');
        }).fail(function (data) {
            $('.payment-add-save').prop('disabled', false);
            if (!planList(data)){
                $('.plan-error').show();
                $('.plan-error-title').text('Não conseguimos processar seu pedido');
                $('.plan-error-title-body').text(data.responseText);
            }
        });
    }

    $(document).on('click', '.payment-success button', function () {
        $('.payment-success').remove();
        $('.next-bill-action, .payment-has, .payment-add-save, .plan-cancel').show();
    });

    $(document).on('click', '.plan-cancel', function () {
        opacity(true);
        $('.cancel-plan-modal').show();
    });

    $(document).on('click', '.cancel-plan-modal div:nth-child(3) button:nth-child(2)', function () {
        $('.cancel-plan-modal div:nth-child(3) button:nth-child(2)').prop('disabled', true);
        $('.cancel-plan-modal').removeClass('error');
        var text = $('.cancel-plan-modal div:nth-child(2) input[type="text"]').val();
        if (text == "Sim, tenho certeza") {
            $.ajax({
                type: 'POST',
                url: 'planCancel',
                cache: false,
                data: {},
                dataType: 'text'
            }).done(function (data) {
                showMessageAlert('Assinatura cancelada com sucesso!');
                setTimeout(function () { location.reload(true); }, 2000);
            }).fail(function (data) {
                $('.cancel-plan-modal div:nth-child(3) button:nth-child(2)').prop('disabled', false);
                if (!planList(data))
                    showMessageAlert(data.responseText, 'error');
            });
        } else
            $('.cancel-plan-modal').addClass('error');
    });

    $(document).on('click', '.cancel-plan-modal div:nth-child(1) button, .cancel-plan-modal div:nth-child(3) button:first-child', function () {
        opacity(false);
        $('.cancel-plan-modal').removeClass('error');
        $('.cancel-plan-modal').hide();
    });

    /*CARD VERIFICATIONS*/
    var PREFIXES_ELO = [
        "401178", "401179", "438935", "457631", "457632", "431274", "451416", "457393",
        "504175", "506699", "506778", "509000", "509999",
        "627780", "636297", "636368", "650031", "650033", "650035", "650051", "650405", "650439", "650485", "650538", "650541", "650598", "650700", "650718", "650720", "650727", "650901", "650978", "651652", "651679", "655000", "655019", "655021", "655058", "6504"
    ];
    var PREFIXES_AMERICAN_EXPRESS = ["34", "37"];
    var PREFIXES_DISCOVER = ["6011", "622", "64", "65"];
    var PREFIXES_JCB = ["35"];
    var PREFIXES_DINERS_CLUB = ["300", "301", "302", "303", "304", "305", "309", "36", "38", "39"];
    var PREFIXES_VISA = ["4"];
    var PREFIXES_MASTERCARD = [
        "2221", "2222", "2223", "2224", "2225", "2226", "2227", "2228", "2229",
        "223", "224", "225", "226", "227", "228", "229",
        "23", "24", "25", "26",
        "270", "271", "2720",
        "50", "51", "52", "53", "54", "55"
    ];
    var PREFIXES_HIPERCARD = ["38", "60"];
    function getFlagCard(cardNumber) {
        var flag = {};
        if (cardNumber != '') {
            if (hasAnyPrefix(cardNumber, PREFIXES_ELO)) {
                flag = { "name": "elo", "flag": "elo.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_AMERICAN_EXPRESS)) {
                flag = { "name": "amex", "flag": "american_express.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_DISCOVER)) {
                flag = { "name": "discover", "flag": "discover.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_JCB)) {
                flag = { "name": "jcb", "flag": "jcb.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_DINERS_CLUB)) {
                flag = { "name": "diners", "flag": "diners_club.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_VISA)) {
                flag = { "name": "visa", "flag": "visa.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_MASTERCARD)) {
                flag = { "name": "mastercard", "flag": "mastercard.svg" };
            } else if (hasAnyPrefix(cardNumber, PREFIXES_HIPERCARD)) {
                flag = { "name": "hipercard", "flag": "hipercard.svg" };
            } else {
                flag = { "name": "semBandeira", "flag": "sem_bandeira.svg" };
            }
        }
        return flag;
    }
    function hasAnyPrefix(number, prefixes) {
        var exists = false;
        $.each(prefixes, function (index, value) {
            if (number.startsWith(value)) {
                exists = true;
            }
        });

        return exists;
    }

    $(document).on('input', '#c_input', function () {
        var flag = getFlagCard($(this).val())['flag'];
        $('.payment-add-form img').attr('src', 'img/payment_type/' + flag);
        $('.payment-add-form img').attr('flag', flag.split('.svg')[0]);
    });

    /*-----------------------------------------------------------*/

    /*--------------------------Links Area--------------------------*/

    /*Add new buttons*/
    $(document).on('click', '.new-link-custom', function () {
        var ref = $('.new-link-custom-area');

        $.ajax({
            type: 'POST',
            url: 'render',
            cache: false,
            data: { view: 'account/link-enabled' },
            dataType: 'text'
        }).done(function (data) {
            $(ref).before(data);
            $(ref).removeClass('new');
            $('.text-link-custom:disabled~h3').hide();
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar adicionar um campo.', 'error');
        });
    });
    /*Create button linkTree*/
    $(document).on('click', '.new-link-custom-save p:nth-child(2)', function () {
        var title = $('.title-link-custom:not(:disabled)');
        var link = $('.text-link-custom:not(:disabled)');
        var ref = $(link).parent();
        var key = $(link).attr('key') ?? '';

        $.ajax({
            type: 'POST',
            url: 'new-link',
            cache: false,
            data: {
                title: $(title).val(),
                link: !$(link).val().includes('http') ? 'https://' + $(link).val() : $(link).val(),
                key: key
            },
            dataType: 'text'
        }).done(function (data) {
            $(link).prop('disabled', true);
            $(title).prop('disabled', true);
            refresh_preview();
            $('.text-link-custom:disabled~h3').show();
            $('.new-link-custom-area').addClass('new');
            $(ref).removeClass('new');

            if (data == "201")
                $('[onboardinghelpstep]').attr('onboardinghelpstep', "4");
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar adicionar um campo.', 'error');
        });
    });
    $(document).on('keyup', '.text-link-custom:not(:disabled)', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            $('.new-link-custom-save p:nth-child(2)').click();
        }
    });
    function refresh_preview() {
        $.ajax({
            type: 'POST',
            url: 'refresh-preview',
            cache: false,
            data: {},
            dataType: 'json'
        }).done(function (data) {
            $('.linktree-button.custom').remove();
            $('.title-link-custom').remove();
            $('.link-custom').remove();
            $('.new-link-custom-area').before(data.links);
            $('.linktree-profile').append(data.preview);
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar atualizar os botões do preview', 'error');
        });
    }
    /*Cancel save state*/
    $(document).on('click', '.new-link-custom-save p:nth-child(1)', function () {
        $('.new-link-custom-area').addClass('new');
        $('.text-link-custom:disabled~h3').show();

        var ref = $('.text-link-custom:not(:disabled)');
        if ($(ref).attr('key') === undefined) {
            $(ref).parent().prev().remove();
            $(ref).parent().remove();
        } else {
            $(ref).prop('disabled', true);
            $(ref).parent().prev().prop('disabled', true);
        }
    });
    /*Edit custom link*/
    $(document).on('click', '.link-custom h3:nth-child(3)', function () {
        var ref = $(this).parents('.link-custom');
        var title = $(ref).prev();
        var link = $('.text-link-custom', ref);

        $(title).prop('disabled', false);
        $(link).prop('disabled', false);
        $('.text-link-custom:disabled~h3').hide();
        $('.new-link-custom-area').removeClass('new');
    });
    /*Exclude custom link*/
    $(document).on('click', '.text-link-custom:not(:disabled)~h3', function () {
        var ref = $(this).parents('.link-custom');
        var title = $(ref).prev();
        var link = $('.text-link-custom', ref);

        if ($(link).attr('key') != undefined) {
            $.ajax({
                type: 'POST',
                url: 'remove-link',
                cache: false,
                data: { key: $(link).attr('key') },
                dataType: 'text'
            }).done(function (data) {
                $(title).next().remove();
                $(title).remove();
                refresh_preview();
                $('.new-link-custom-area').addClass('new');
            }).fail(function (data) {
                if (!planList(data))
                    showMessageAlert('Tivemos um problema ao tentar adicionar um campo.', 'error');
            });
        } else {
            $(title).prop('disabled', true);
            $(link).prop('disabled', true);
        }
    });
    /*Copy name of link*/
    $(document).on('click', '.link-default svg, .link-custom svg', function () {
        var ref = $(this).parent();
        clipboard($(ref).attr('link'));
        $(ref).addClass('copied');
        setTimeout(function () { $(ref).removeClass('copied'); }, 2000);
    });
    /*Enable/desable button*/
    $(document).on('change', '.link-default .enabled-obj', function () {
        var enable = $(this).is(':checked');
        var button = $(this).attr('button');
        $.ajax({
            type: 'POST',
            url: 'disable-button',
            cache: false,
            data: {
                button: button,
                enable: enable
            },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Botão atualizado.');
            if (enable)
                $('.' + button).addClass('checked');
            else
                $('.' + button).removeClass('checked');

            if (data == "201")
                $('[onboardinghelpstep]').attr('onboardinghelpstep', "4");

        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar atualizar o botão.', 'error');
        });
    });


    /*-----------------------------------------------------------*/

    /*--------------------------Delivery Area--------------------------*/

    /*Delivery Item Days*/
    $(document).on('click', '.delivery-item-days div', function () {
        if ($(this).attr('state') == 'selected')
            $(this).removeAttr('state');
        else
            $(this).attr('state', 'selected');

        verifyDeliveryPicker(this);
    });
    /*Delivery Create*/
    $(document).on('click', '.delivery-create', function () {
        $.ajax({
            type: 'POST',
            url: 'render',
            cache: false,
            data: { view: 'delivery/delivery-item-edit' },
            dataType: 'text'
        }).done(function (data) {
            $('.tabs-main-delivery').append(data);
            opacity(true);
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar adicionar um campo.', 'error');
        });
    });
    /*Cancel Delivery Create*/
    $(document).on('click', '.delivery-item.creating .delivery-item-title h1', function () {
        $('.delivery-item.creating').remove();
        opacity(false);
    });
    /*Button Item */
    $(document).on('click', '.delivery-item-hours button:not(.disabled):not(.saving)', function () {
        var ref = $(this).parents('.delivery-item');
        var button = $(this);

        var dias = [];
        $('.delivery-item-days div', ref).each(function () {
            if ($(this).attr('state') == 'selected')
                dias.push($(this).index());
        });

        $.ajax({
            type: 'POST',
            url: 'new-hours',
            cache: false,
            data: {
                name: $('#name', ref).val(),
                dias: dias,
                abre: $('#abre', ref).val(),
                fecha: $('#fecha', ref).val(),
                index: $('#name', ref).attr('index') ?? ''
            },
            dataType: 'text'
        }).done(function (data) {
            if ($(".delivery-item.creating").length > 0) {
                $(".delivery-item.creating").prependTo(".delivery-itens");
                $(".delivery-item.creating").removeClass("creating");
                $('#name', ref).prop('disabled', true);
                opacity(false);
            }
            $('#name', ref).attr('index', $('#name', ref).attr('index'));
            $(button).addClass('saving');
            setTimeout(function () { $(button).removeClass('saving').addClass('disabled'); }, 3000);

            if (data == "201")
                $('[onboardinghelpstep]').attr('onboardinghelpstep', "finish");
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert(data.responseText, 'error');
        });
    });
    /*Definindo hora*/
    $(document).on('click', '.delivery-item-hours input[type="text"]', function () {
        ref_time = this;
        $('.timepicker').click();
        opacityOnModal(true);
    });
    /*Verificar estado botão*/
    function verifyDeliveryPicker(r) {
        r = $(r).parents('.delivery-item-verify');

        var can = true;
        $('.delivery-item-hours input[type="text"]', r).each(function () {
            if ($(this).val() == '')
                can = false;
        });

        $('.delivery-item-hours button', r).addClass('disabled');
        if (can)
            $('.delivery-item-hours button', r).removeClass('disabled');
    }
    /*Exclude Delivery*/
    $(document).on('click', '.exclude-delivery', function () {
        var ref = $(this).parents('.delivery-item');
        $.ajax({
            type: 'POST',
            url: 'remove-hours',
            cache: false,
            data: { key: $('#name', ref).attr('index') },
            dataType: 'text'
        }).done(function (data) {
            $(ref).remove();
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar remover o horário.', 'error');
        });
    });
    /*Change Delivery*/
    $(document).on('click', '.change-delivery', function () {
        var ref = $(this).parents('.delivery-item-verify');
        $('#name', ref).prop('disabled', false);
        $('.delivery-item-button', ref).removeClass('disabled');
        $('#name', ref).focus();
    });
    /*Delivery Special Create*/
    $(document).on('click', '.delivery-special-add-item', function () {
        ref_date = this;
        $('.datepicker').click();
        opacityOnModal(true);
    });
    /*Enable special hour*/
    // $(document).on('click', '.delivery-special-item .enabled-obj', function () {
    //     var ref = $(this);

    //     $.ajax({
    //         type: 'POST',
    //         url: 'status-special-hour',
    //         cache: false,
    //         data: {
    //             key: $(ref).attr('key'),
    //             disponibilidade: $(ref).prop('checked')
    //         },
    //         dataType: 'text'
    //     }).done(function (data) {
    //         showMessageAlert('Status da data alterada.');
    //     }).fail(function (data) {
    //         showMessageAlert('Tivemos um problema ao tentar alterar o status do cupom.', 'error');
    //     });
    // });
    /*Exclude special dates*/
    $(document).on('click', '.exclude-special-dates', function () {
        $('.delivery-special-itens').addClass('excluding');
    });
    /*Cancel exclude special dates*/
    $(document).on('click', '.exclude-special-dates-title p:nth-child(2)', function () {
        $('.delivery-special-itens').removeClass('excluding');
    });
    /*Excluding special hour*/
    $(document).on('click', '.delivery-special-item-exclude', function () {
        var ref = $(this);
        var item = $(this).parents('.delivery-special-item');

        $.ajax({
            type: 'POST',
            url: 'remove-special-hour',
            cache: false,
            data: {
                key: $(ref).attr('key')
            },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Data removida com sucesso.');
            $(item).remove();
            if ($('.delivery-special-item').length == 0)
                $('.delivery-special-itens').removeClass('excluding');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar remover a data.', 'error');
        });
    });

    /*-----------------------------------------------------------*/

    /*--------------------------Payment Area--------------------------*/

    $(document).on('click', '.payment-card-area-buttons button', function () {
        $('.payment-card-area-buttons button').removeClass('selected');
        $(this).addClass('selected');

        if ($(this).text() != "Todos") {
            $(".payment-card-area-item").hide();
            $(".payment-card-area-item h2:contains('" + $(this).text() + "')").parent().show();
        } else
            $(".payment-card-area-item").show();
    });

    timerValorMinimo = null;
    $(document).on('keyup', '.payment-field input[type="text"]', function () {
        var valorMinimo = $(this).val();

        if (timerValorMinimo != null)
            clearTimeout(timerValorMinimo);

        timerValorMinimo = setTimeout(function () {
            setValorMinimo(valorMinimo);
        }, 2000);
    });

    function setValorMinimo(valorMinimo) {
        $.ajax({
            type: 'POST',
            url: 'valorMinimo',
            cache: false,
            data: { valorMinimo: valorMinimo },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert("Valor mínimo atualizado.");
        }).fail(function (data) {
            if (!planList(data))
                alert("Tivemos problemas para atualizar o valor mínimo.");
        });
    }

    $(document).on('click', '.payment-type-payment .enabled-obj', function () {
        var enable = $(this).is(':checked');
        $.ajax({
            type: 'POST',
            url: 'habilitarDinheiro',
            cache: false,
            data: {
                status: enable
            },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Pagamento no dinheiro ' + (enable ? 'habilitado' : 'desabilitado') + '.');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar atualizar o pagamento em dinheiro.', 'error');
        });
    });

    $(document).on('click', '.payment-card-area-add-flag', function () {
        opacity(true);
        $('.add-payment-type').show();
    });

    timerCartoes = null;
    $(document).on('change', '.payment-card-area-item input[type="checkbox"]', function () {
        if (timerCartoes != null)
            clearTimeout(timerCartoes);

        timerCartoes = setTimeout(function () {
            atualizarCartoes();
        }, 2000);
    });

    function atualizarCartoes() {
        var cards = {};
        $(".payment-card-area-item:not(.select-all)").each(function () {
            if ($("input[type='checkbox']", this).is(':checked')) {
                var id = $(this).attr('id') ?? $(this).attr('name');
                var type = $(this).attr('type');
                var itens = cards[id] ?? []
                itens.push(type);
                cards[id] = itens;
            }
        });

        $.ajax({
            type: 'POST',
            url: 'atualizarCartoes',
            cache: false,
            data: { cards: cards },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert("Os carões aceitos foram atualizados com sucesso.");
        }).fail(function (data) {
            if (!planList(data))
                alert("Tivemos problemas ao tentar atualizar os carões aceitos.");
        });
    }

    $(document).on('click', '.add-payment-type-buttons button', function () {
        $(this).toggleClass('selected');
    });

    $(document).on('click', '.add-payment-type-footer h5', function () {
        $('.add-payment-type').hide();
        $('.add-payment-type input[type="text"]').val('');
        $('.add-payment-type-buttons button').removeClass('selected');
        opacity(false);
    });

    $(document).on('click', '.add-payment-type-footer button', function () {
        var tipo = [];
        $(".add-payment-type-buttons button.selected").each(function () {
            tipo.push($(this).attr('type'));
        });
        var nome = $('.add-payment-type-field input').val();

        $.ajax({
            type: 'POST',
            url: 'novosCartoes',
            cache: false,
            data: {
                nome: nome,
                tipo: tipo,
            },
            dataType: 'text'
        }).done(function (data) {
            $('.payment-card-area-item.select-all').after(data);
            $('.add-payment-type-footer h5').click();
            showMessageAlert('Cartão adicionado com sucesso!');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert(data.responseText, 'error');
        });
    });

    $(document).on('click', '.payment-card-area-item.select-all input[type="checkbox"]', function () {
        $('.payment-card-area-item input[type="checkbox"]').prop('checked', $(this).is(':checked'));
    });

    $(document).on('input', '.payment-card-area-title .search-bar input[type="text"]', function () {
        $(".payment-card-area-item").hide();
        $(".payment-card-area-item h1:contains('" + $(this).val() + "')").parents('.payment-card-area-item').show();

        if ($(this).val() == '')
            $(".payment-card-area-item").show();
    });

    /*-----------------------------------------------------------*/

    /*--------------------------Coupon Area--------------------------*/

    /*New Coupon*/
    $(document).on('click', '.tabs-main-coupon-empty div p, .tabs-main-coupon-content-search div:nth-child(2)', function () {
        opacity(true);
        $('.tabs-main-coupon-new-coupon').css('display', 'flex');
    });
    /*Details Coupon*/
    $(document).on('click', '.tabs-main-coupon-content:not(.excluding) .tabs-main-coupon-content-item-content', function (e) {
        if ($(e.target).prop('nodeName') == 'DIV' ||
            $(e.target).prop('nodeName') == 'H5' ||
            $(e.target).prop('nodeName') == 'H4') {

            var ref = $(this).parents('.tabs-main-coupon-content-item');
            var code = $('.tabs-main-coupon-content-item-title h5', ref).text();

            $.ajax({
                type: 'POST',
                url: 'getByKeyCoupon',
                cache: false,
                data: { key: code },
                dataType: 'json'
            }).done(function (data) {
                $('.stats-iterations *').remove();
                $('.stats-iterations').append(data.uso);

                $('#coupon_name').val(data.nomeCupom);
                $('#coupon_name').prop('disabled', true);
                $('#coupon_disponivel').prop('checked', data.disponibilidade);
                $('#usado').append(data.usado + '<span>x</span>');
                $('#movimentado').append('<span>R$</span>' + data.movimentado);
                $('#coupon_porcentagem').val(parseInt(data.valorPorcentagem) > 0 ? data.valorPorcentagem : '');
                $('#coupon_real').val(parseInt(data.valorReal) > 0 ? money(data.valorReal) : '');

                if (data.valorMinimoCompra > 0) {
                    $('.new-coupon-right-hint').click();
                    $('#coupon_minimo').val(money(data.valorMinimoCompra));
                }

                $('#coupon_frete').prop('checked', data.freteGratis);
                $('#coupon_limite_cliente').val(data.limiteUsoUsuario);
                $('#coupon_limite_geral').val(data.quantidadeLimite);

                if (data.dataInicio > 0) {
                    data_inicio = timestampString(data.dataInicio);

                    $('#coupon_data_inicio').val(data_inicio.data);

                    if (parseInt(data_inicio.time_t) > 0) {
                        $('#coupon_hora_inicio').text('');
                        $('#coupon_hora_inicio').append('<h5>' + data_inicio.time + '</h5><h5>Editar</h5>');
                    }

                    $('#coupon_data_inicio').attr('timestamp', data_inicio.data_t);
                    $('#coupon_hora_inicio').attr('timestamp', data_inicio.time_t);
                }

                if (data.dataLimite > 0) {
                    data_limite = timestampString(data.dataLimite);

                    $('#coupon_data_fim').val(data_limite.data);

                    if (parseInt(data_limite.time_t) > 0) {
                        $('#coupon_hora_fim').text('');
                        $('#coupon_hora_fim').append('<h5>' + data_limite.time + '</h5><h5>Editar</h5>');
                    }

                    $('#coupon_data_fim').attr('timestamp', data_limite.data_t);
                    $('#coupon_hora_fim').attr('timestamp', data_limite.time_t);
                }

                opacity(true);
                $('.tabs-main-coupon-new-coupon').addClass('editing');
                $('.tabs-main-coupon-new-coupon').css('display', 'flex');
            }).fail(function (data) {
                if (!planList(data))
                    showMessageAlert('Tivemos um problema ao tentar ver os detalhes do cupom.', 'error');
            });
        }
    });
    function timestampString(timestamp) {
        var date = new Date(timestamp);
        var dia = addZero(date.getDate());
        var mes = addZero(date.getMonth() + 1);
        var ano = addZero(date.getFullYear().toString().substr(2));

        var hora = addZero(date.getHours());
        var minuto = addZero(date.getMinutes());

        var response = {};
        response.data = dia + '/' + mes + '/' + ano;
        response.time = hora + ':' + minuto;
        response.data_t = Date.parse(mes + '/' + dia + '/' + ano + " 00:00");
        response.time_t = hora * 3600;
        response.time_t += minuto * 60;
        response.time_t *= 1000;

        return response;
    }

    /*Close modal new coupon*/
    $(document).on('click', '.new-coupon-right-action p:nth-child(1)', function () {
        opacity(false);
        closeModal();
    });
    function closeModal() {
        var modal = '.tabs-main-coupon-new-coupon';

        $(modal).removeClass('editing');
        $(modal + ' input[type="text"]').val('');
        $('#coupon_name').prop('disabled', false);
        $(modal).hide();
        $('.new-coupon-right-minimum div:nth-child(1) p').click();
        $(modal + ' input[type="checkbox"]').prop('checked', true);
        $(modal + ' [timestamp]').attr('timestamp', '0');
        $('#usado').text('');
        $('#movimentado').text('');
        $('#coupon_hora_inicio').text('');
        $('#coupon_hora_fim').text('');
        $('#coupon_hora_inicio').attr('timestamp', '0');
        $('#coupon_hora_fim').attr('timestamp', '0');
        $('#coupon_hora_inicio').append('<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M8 3.5C8 3.36739 7.94732 3.24021 7.85355 3.14645C7.75979 3.05268 7.63261 3 7.5 3C7.36739 3 7.24021 3.05268 7.14645 3.14645C7.05268 3.24021 7 3.36739 7 3.5V9C7.00003 9.08813 7.02335 9.17469 7.06761 9.25091C7.11186 9.32712 7.17547 9.39029 7.252 9.434L10.752 11.434C10.8669 11.4961 11.0014 11.5108 11.127 11.4749C11.2525 11.4391 11.3591 11.3556 11.4238 11.2422C11.4886 11.1288 11.5065 10.9946 11.4736 10.8683C11.4408 10.7419 11.3598 10.6334 11.248 10.566L8 8.71V3.5Z" fill="#E4002B"></path> <path d="M8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16ZM15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8Z" fill="#E4002B"></path> </svg> Definir horário');
        $('#coupon_hora_fim').append('<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M8 3.5C8 3.36739 7.94732 3.24021 7.85355 3.14645C7.75979 3.05268 7.63261 3 7.5 3C7.36739 3 7.24021 3.05268 7.14645 3.14645C7.05268 3.24021 7 3.36739 7 3.5V9C7.00003 9.08813 7.02335 9.17469 7.06761 9.25091C7.11186 9.32712 7.17547 9.39029 7.252 9.434L10.752 11.434C10.8669 11.4961 11.0014 11.5108 11.127 11.4749C11.2525 11.4391 11.3591 11.3556 11.4238 11.2422C11.4886 11.1288 11.5065 10.9946 11.4736 10.8683C11.4408 10.7419 11.3598 10.6334 11.248 10.566L8 8.71V3.5Z" fill="#E4002B"></path> <path d="M8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16ZM15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8Z" fill="#E4002B"></path> </svg> Definir horário');
    }

    /*Empty coupon*/
    function emptyModal() {
        $('.tabs-main-coupon').removeClass('empty');

        if ($('.tabs-main-coupon-content-item').length <= 0) {
            $('.tabs-main-coupon').addClass('empty');
            $('.tabs-main-coupon-content').removeClass('excluding');
        }
    }

    /*Confirm modal new coupon*/
    $(document).on('click', '.new-coupon-right-action button', function () {
        var coupon = {};

        coupon.key = $('#coupon_name').val();
        coupon.valorReal = nmoneyf($('#coupon_real').val());
        coupon.valorPorcentagem = $('#coupon_porcentagem').val();
        coupon.valorMinimoCompra = $('.new-coupon-right-hint').hasClass('minimun') ? nmoneyf($('#coupon_minimo').val()) : 0;
        coupon.freteGratis = $('#coupon_frete').is(':checked');
        coupon.limiteUsoUsuario = parseInt($('#coupon_limite_cliente').val());
        coupon.quantidadeLimite = parseInt($('#coupon_limite_geral').val());
        coupon.dataInicio = parseInt($('#coupon_data_inicio').attr('timestamp')) + parseInt($('#coupon_hora_inicio').attr('timestamp'));
        coupon.dataLimite = parseInt($('#coupon_data_fim').attr('timestamp')) + parseInt($('#coupon_hora_fim').attr('timestamp'));
        coupon.disponibilidade = $('#coupon_disponivel').is(':checked');

        $.ajax({
            type: 'POST',
            url: 'createCoupon',
            cache: false,
            data: { data: JSON.stringify(coupon) },
            dataType: 'json'
        }).done(function (data) {
            if (coupon.nomeAnterior != '')
                $('.tabs-main-coupon-content-item[key="' + coupon.nomeAnterior + '"]').remove();

            if ($('.tabs-main-coupon-content-item[key="' + data.key + '"]').length > 0) {
                $('.tabs-main-coupon-content-item[key="' + data.key + '"]').remove();
                showMessageAlert('Cupom alterado com sucesso!');
            } else
                showMessageAlert('Cupom cadastrado com sucesso!');

            $('.new-coupon-right-action p:nth-child(1)').click();
            $('.tabs-main-coupon-content-itens').append(data.view);
            emptyModal();
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar cadastrar o cupom.', 'error');
        });
    });

    /*Search coupon*/
    $(document).on('input', '.tabs-main-coupon-content-search div:nth-child(1) input', function () {
        $('.tabs-main-coupon-content-item').appendTo('.tabs-main-coupon-hidden');
        if ($(this).val() != '') {
            $('.tabs-main-coupon-content-item-title h5:contains("' + $(this).val().toUpperCase() + '")').parents('.tabs-main-coupon-content-item').appendTo('.tabs-main-coupon-content-itens');
        } else
            $('.tabs-main-coupon-content-item').appendTo('.tabs-main-coupon-content-itens');
    });

    /*Copy name of coupon*/
    $(document).on('click', '.tabs-main-coupon-content-item-content div:nth-child(1) svg', function () {
        var ref = this;
        clipboard($('h5', $(ref).parent()).text());
        $(ref).addClass('copied');
        setTimeout(function () { $(ref).removeClass('copied'); }, 2000);
    });

    /*Excluding coupon*/
    $(document).on('click', '.exclude-coupon', function () {
        $('.tabs-main-coupon-content').addClass('excluding');
    });
    /*Exclude coupon*/
    $(document).on('click', '.tabs-main-coupon-content.excluding .tabs-main-coupon-content-item-content div:nth-child(3) h4:nth-child(1)', function () {
        ref_modal = $(this).parents('.tabs-main-coupon-content-item');
        code_coupon = $('.tabs-main-coupon-content-item-title h5', ref_modal).text();
        opacity(true);
        $('.delete-modal-coupon div:nth-child(2) h3 span').text(code_coupon);
        $('.delete-modal-coupon').show();

        $(document).on('click', '.delete-modal-coupon div:nth-child(3) h5:nth-child(2)', function () {
            $.ajax({
                type: 'POST',
                url: 'removeCoupon',
                cache: false,
                data: { key: code_coupon },
                dataType: 'text'
            }).done(function (data) {
                $(ref_modal).remove();
                $('.delete-modal-coupon div:nth-child(3) h5:nth-child(1)').click();
                showMessageAlert('Cupom removido com sucesso!');
                emptyModal();
            }).fail(function (data) {
                if (!planList(data))
                    showMessageAlert('Tivemos um problema ao tentar cancelar o cupom.', 'error');
            });
        });
    });
    /*Cancel exclude coupon*/
    $(document).on('click', '.delete-modal-coupon div:nth-child(3) h5:nth-child(1)', function () {
        $('.delete-modal-coupon').hide();
        opacity(false);
    });

    /*Generate random code*/
    $(document).on('click', '.new-coupon-right-random-coupon p', function () {
        $('.new-coupon-right-name-coupon div:nth-child(1) input').val('');
        $('.new-coupon-right-name-coupon div:nth-child(1) input').val(makeid(12));
    });
    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        return result;
    }
    /*Prevent space bar*/
    $(document).on('keydown', '.new-coupon-right-name-coupon div:nth-child(1) input', function (e) {
        if (e.keyCode == 32) {
            e.preventDefault();
        }
    });
    /*Order minimum*/
    $(document).on('click', '.new-coupon-right-hint', function () {
        $(this).addClass('minimun');
    });

    /*Order minimum*/
    $(document).on('click', '.new-coupon-right-minimum div:nth-child(1) p', function () {
        $(this).parents('.new-coupon-right-minimum').prev().removeClass('minimun');
    });

    /*Cancel excluding*/
    $(document).on('click', '.tabs-main-coupon-content-search div:nth-child(3) p:nth-child(2)', function () {
        $('.tabs-main-coupon-content').removeClass('excluding');
    });
    /*Clean real/percent*/
    $(document).on('keydown', '.new-coupon-right-discount input', function (e) {
        if (e.which == 9) {
            e.preventDefault();
        }
    });
    $(document).on('keydown', '.new-coupon-right-discount div:nth-child(2) input', function () {
        $('.new-coupon-right-discount div:nth-child(4) input').val('');
    });
    $(document).on('keydown', '.new-coupon-right-discount div:nth-child(4) input', function () {
        $('.new-coupon-right-discount div:nth-child(2) input').val('');
    });
    /*Go to sale*/
    $(document).on('click', '.stats-iterations div', function () {
        window.location.href = "vendas?o=3&key=" + $(this).attr('key');
    });
    /*Copy name of coupon*/
    $(document).on('click', '.link-default svg, .link-custom svg', function () {
        var ref = $(this).parent();
        clipboard($(ref).attr('link'));
        $(ref).addClass('copied');
        setTimeout(function () { $(ref).removeClass('copied'); }, 2000);
    });
    $(document).on('click', '.tabs-main-coupon-content-item .title-disponibility', function () {
        $('.tabs-main-coupon-content-item .enabled-obj').click();
    });
    /*Enable coupon*/
    $(document).on('click', '.tabs-main-coupon-content-item .enabled-obj', function () {
        var ref = $(this);

        $.ajax({
            type: 'POST',
            url: 'statusCoupon',
            cache: false,
            data: {
                key: $(ref).attr('key'),
                disponibilidade: $(ref).prop('checked')
            },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Status do cupom alterado.');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar alterar o status do cupom.', 'error');
        });
    });
    /*Copy name of coupon on modal*/
    $(document).on('click', '.new-coupon-right-name-coupon div:nth-child(1) svg', function () {
        var ref = this;
        clipboard($('input', $(ref).parent()).val());
        $(ref).addClass('copied');
        setTimeout(function () { $(ref).removeClass('copied'); }, 2000);
    });

    /*-----------------------------------------------------------*/

    /*--------------------------Info Area--------------------------*/

    verifyEach();

    function str2num(val) {
        var result = 0;
        for (var i = 0; i < val.length; i++) {
            if (i % 2 == 0)
                result *= val.charCodeAt(i);
            else
                result += val.charCodeAt(i);
        }

        return result.toString().substring(0, 7);
    }

    var path = document.getElementById("path").value;

    $(document).on('click', '.coupon-top-desks input[type="button"]', function () {

        $('.orders-qr-info').show();
        $('.orders-scroll-item').remove();
        $('.coupon-area-result-list').hide();

        var qtd = parseInt($('.coupon-top-desks input[type="text"]').val());

        if (qtd > 0) {
            $.ajax({
                type: 'POST',
                url: 'etiquetasQr',
                cache: false,
                data: { qtd: qtd },
                dataType: 'text'
            })

            $(this).addClass('generated');
            $(this).val('Etiquetas geradas');

            for (var i = 1; i <= qtd; i++) {
                if (i < 10)
                    i = '0' + i;

                $('.orders-scroll-list').append('<div class="orders-scroll-item" pos="' + i + '" url="https://cardapio.ai/' + path + '?mesa=' + i + '&v=' + str2num("cardapio.ai/" + path) + '"> <p>Mesa ' + i + '</p> <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M12.75 9.75C12.75 9.55109 12.671 9.36032 12.5303 9.21967C12.3896 9.07902 12.1989 9 12 9C11.8011 9 11.6103 9.07902 11.4696 9.21967C11.329 9.36032 11.25 9.55109 11.25 9.75V15.4395L9.53097 13.719C9.46124 13.6493 9.37846 13.594 9.28735 13.5562C9.19624 13.5185 9.09859 13.4991 8.99997 13.4991C8.90136 13.4991 8.8037 13.5185 8.7126 13.5562C8.62149 13.594 8.5387 13.6493 8.46897 13.719C8.39924 13.7887 8.34392 13.8715 8.30619 13.9626C8.26845 14.0537 8.24902 14.1514 8.24902 14.25C8.24902 14.3486 8.26845 14.4463 8.30619 14.5374C8.34392 14.6285 8.39924 14.7113 8.46897 14.781L11.469 17.781C11.5386 17.8508 11.6214 17.9063 11.7125 17.9441C11.8036 17.9819 11.9013 18.0013 12 18.0013C12.0986 18.0013 12.1963 17.9819 12.2874 17.9441C12.3785 17.9063 12.4613 17.8508 12.531 17.781L15.531 14.781C15.6007 14.7113 15.656 14.6285 15.6938 14.5374C15.7315 14.4463 15.7509 14.3486 15.7509 14.25C15.7509 14.1514 15.7315 14.0537 15.6938 13.9626C15.656 13.8715 15.6007 13.7887 15.531 13.719C15.4612 13.6493 15.3785 13.594 15.2873 13.5562C15.1962 13.5185 15.0986 13.4991 15 13.4991C14.9014 13.4991 14.8037 13.5185 14.7126 13.5562C14.6215 13.594 14.5387 13.6493 14.469 13.719L12.75 15.4395V9.75Z" fill="#E4002B" /> <path d="M21 21V6.75L14.25 0H6C5.20435 0 4.44129 0.316071 3.87868 0.87868C3.31607 1.44129 3 2.20435 3 3V21C3 21.7956 3.31607 22.5587 3.87868 23.1213C4.44129 23.6839 5.20435 24 6 24H18C18.7956 24 19.5587 23.6839 20.1213 23.1213C20.6839 22.5587 21 21.7956 21 21ZM14.25 4.5C14.25 5.09674 14.4871 5.66903 14.909 6.09099C15.331 6.51295 15.9033 6.75 16.5 6.75H19.5V21C19.5 21.3978 19.342 21.7794 19.0607 22.0607C18.7794 22.342 18.3978 22.5 18 22.5H6C5.60218 22.5 5.22064 22.342 4.93934 22.0607C4.65804 21.7794 4.5 21.3978 4.5 21V3C4.5 2.60218 4.65804 2.22064 4.93934 1.93934C5.22064 1.65804 5.60218 1.5 6 1.5H14.25V4.5Z" fill="#E4002B" /> <path d="M21 21V6.75L14.25 0H6C5.20435 0 4.44129 0.316071 3.87868 0.87868C3.31607 1.44129 3 2.20435 3 3V21C3 21.7956 3.31607 22.5587 3.87868 23.1213C4.44129 23.6839 5.20435 24 6 24H18C18.7956 24 19.5587 23.6839 20.1213 23.1213C20.6839 22.5587 21 21.7956 21 21ZM14.25 4.5C14.25 5.09674 14.4871 5.66903 14.909 6.09099C15.331 6.51295 15.9033 6.75 16.5 6.75H19.5V21C19.5 21.3978 19.342 21.7794 19.0607 22.0607C18.7794 22.342 18.3978 22.5 18 22.5H6C5.60218 22.5 5.22064 22.342 4.93934 22.0607C4.65804 21.7794 4.5 21.3978 4.5 21V3C4.5 2.60218 4.65804 2.22064 4.93934 1.93934C5.22064 1.65804 5.60218 1.5 6 1.5H14.25V4.5Z" fill="#E4002B" /> </svg> </div>');
            }
            $('.coupon-area-result-list').show();
        } else
            alert("Digite um valor valido.");

    });

    $('.coupon-top-desks input[type="text"]').keypress(function (e) {
        if (e.which == 13) {
            $('.coupon-top-desks input[type="button"]').click();
        }
    });

    if ($('.coupon-top-desks input[type="text"]').val() != '')
        $('.coupon-top-desks input[type="button"]').click();

    $(document).on('input', '.coupon-top-desks input[type="text"]', function () {
        $('.coupon-top-desks input[type="button"]').removeClass('generated');
        $('.coupon-top-desks input[type="button"]').val('Gerar etiquetas');
    });

    $(document).on('click', '.orders-scroll-item', function () {

        $('.orders-qr-view').show();
        $('.orders-qr-view canvas').remove();

        var pos = $(this).attr('pos');
        var url = $(this).attr('url');

        $('.orders-qr-view p').text("MESA " + pos);

        $('.orders-qr-view div').qrcode({
            width: 224,
            height: 224,
            text: url
        });
    });

    //Botão de baixar um qr code individualmente
    $(document).on('click', '.orders-scroll-item svg', function () {
        var qrs = [];
        qrs.push($(this).parent().attr('pos'));

        generateQrs(qrs);
    });

    //Botão de baixar todos os qr codes
    $(document).on('click', '.orders-scroll-title h1:nth-child(2)', function () {
        var qrs = [];
        $(".orders-scroll-item").each(function (index) {
            qrs.push($(this).attr("pos"));
        });

        generateQrs(qrs);
    });

    function generateQrs(qrs) {
        $('#print-area *').remove();
        window.scrollTo(0, 0);

        $('.loading-registering').fadeIn(200);
        opacity(true);

        $.ajax({
            type: 'POST',
            url: 'render',
            cache: false,
            data: {
                view: 'print/a4-default-qr'
            },
            dataType: 'text'
        }).done(function (data) {
            var qtdQrs = qrs.length;
            var qtdPapers = parseInt(qtdQrs / 15);
            if (((qtdQrs / 15) - Math.floor(qtdQrs / 15)) > 0)
                qtdPapers++;

            for (var i = 1; i <= qtdPapers; i++) {
                $('#print-area').append(data);
            }

            $.each(qrs, function (key, pos) {
                $('.a4-default:eq(' + parseInt((pos - 1) / 15) + ') .a4-default-body').append("<div id='qr-print-" + pos + "' class='qr-print'><div></div><p></p></div>");
                $('#qr-print-' + pos + ' p').text("MESA " + pos);
                $('#qr-print-' + pos + ' div').qrcode({
                    width: 152,
                    height: 152,
                    text: "https://cardapio.ai/" + path + "?mesa=" + pos + "&v=" + str2num("cardapio.ai/" + path)
                });
            });

            setTimeout(function () {
                var element = document.getElementById('print-area');
                var opt = {
                    margin: 0,
                    filename: 'qrcode-mesas.pdf',
                    image: {
                        type: 'jpeg',
                        quality: 4
                    },
                    html2canvas: {
                        scale: 4,
                        scrollX: -window.scrollX,
                        scrollY: -window.scrollY,
                        windowWidth: document.documentElement.offsetWidth,
                        windowHeight: document.documentElement.offsetHeight
                    },
                    jsPDF: {
                        format: 'a4',
                        orientation: 'landscape',
                    }
                };

                html2pdf().from(element).set(opt).save().then(function () {
                    //$('.a4-default').remove();
                    $('.loading-registering').hide();
                    opacity(false);
                });
            }, 1000);
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar gerar os documento de qr codes.', 'error');
        });
    }

    /*Baixar layout do qr code*/
    $(document).on('click', '.qr-customization-button', function () {
        $('#print-area *').remove();
        window.scrollTo(0, 0);

        var element = document.getElementById('print-area');
        var opt = {
            margin: 0,
            filename: '10x15.pdf',
            image: {
                type: 'jpeg',
                quality: 4
            },
            html2canvas: {
                scale: 5,
                allowTaint: true,
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                windowWidth: document.documentElement.offsetWidth,
                windowHeight: document.documentElement.offsetHeight
            },
            jsPDF: {
                format: 'a4',
                orientation: 'landscape',
            }
        };

        $('.loading-registering').fadeIn(200);
        opacity(true);

        $.ajax({
            type: 'POST',
            url: 'render',
            cache: false,
            data: {
                view: 'print/a4-default-10x15',
                data: {
                    url: $('#img-profile').attr('src'),
                    path: path
                }
            },
            dataType: 'text'
        }).done(function (data) {
            $('#print-area').append(data);
            setTimeout(function () {
                html2pdf().from(element).set(opt).save().then(function () {
                    $('#print-area *').remove();
                    window.scrollTo(0, 0);

                    opt.filename = '20x15.pdf';
                    $.ajax({
                        type: 'POST',
                        url: 'render',
                        cache: false,
                        data: {
                            view: 'print/a4-default-20x15',
                            data: {
                                url: $('#img-profile').attr('src'),
                                path: path
                            }
                        },
                        dataType: 'text'
                    }).done(function (data) {
                        $('#print-area').append(data);
                        setTimeout(function () {
                            html2pdf().from(element).set(opt).save().then(function () {
                                $('.a4-default').remove();
                                $('.loading-registering').hide();
                                opacity(false);
                            });
                        }, 1000);
                    }).fail(function (data) {
                        if (!planList(data))
                            showMessageAlert('Tivemos um problema ao tentar gerar os templates de qr codes.', 'error');
                    });

                });
            }, 1000);
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar gerar os templates de qr codes.', 'error');
        });
    });

    /*Segunda verificação switch*/
    $(document).on('change', '.coupon-top-menu-second input[type="checkbox"]', function () {
        $.ajax({
            type: 'POST',
            url: 'tipoVerificacaoQr',
            cache: false,
            data: {
                switch: $(this).is(':checked')
            },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Tipo de verificação alterada.');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Tivemos um problema ao tentar alterar o tipo de verificação.', 'error');
        });
    });

    /*-----------------------------------------------------------*/
    /*---------------------Tabs Main Areas----------------------*/

    $(document).on('click', '.tabs-main-areas-button-frete', function () {
        if (!$(this).hasClass('clicked'))
            $(this).addClass('clicked');
    });

    $(document).on('change', '.retirada-input', function () {
        $('.retirada-input').click();
    });

    $(document).on('click', '.tabs-main-areas-form.frete-gratis-form p:nth-child(2)', function () {
        $('.tabs-main-areas-button-frete').removeClass('clicked');
    });

    $(document).on('click', '.tabs-main-areas-button', function () {
        var data = [];
        $('.tabs-main-areas input[input]:visible').each(function () {
            var index = $(this).parents('.tabs-main-areas-form').index();
            data[index] = $(this).val() ?? '';
        });
        data.push($('.tabs-main-areas .retirada-input').is(':checked'));

        $.ajax({
            type: 'POST',
            url: 'config-delivery',
            cache: false,
            data: { data: data },
            dataType: 'text'
        }).done(function (data) {
            showMessageAlert('Ajustes de entrega atualizado com sucesso!');
        }).fail(function (data) {
            if (!planList(data))
                showMessageAlert('Não conseguimos atualizar os ajustes de entrega.', 'error');
        });
    });

    /*Opacity*/
    $(document).on('click', '#opacity-body', function () {
        $('.delivery-item.creating').remove();
        $('.tabs-main-coupon-new-coupon').hide();
        $('.add-payment-type').hide();
        $('.cancel-plan-modal').hide();
        opacity(false);
    });
});