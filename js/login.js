function verifyForm() {
    var enabled = true;
    $(".form-field").each(function () {
        if ($('input', this).val() == "")
            enabled = false;
    });

    if (enabled)
        $('.form-button').removeClass('disabled');
    else
        $('.form-button').addClass('disabled');

    return enabled;
}

$(document).on('input', '.form-field input', function () {
    verifyForm();
});

$(document).on('submit', 'form', function () {
    return verifyForm();
});

var MaskFone = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000 0000' : '(00) 0000 00009';
},
    Options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(MaskFone.apply({}, arguments), options);
        }
    };
$('#fone').mask(MaskFone, Options);

function showAlertField() {
    var can = true;
    removeRequired();

    $(".form-input[required]").each(function (index) {
        if ($(this).val() == '') {
            can = false;

            $(this).addClass('alerted');

            var text = $(this).attr('message') ?? 'Campo obrigatório';

            if (text != 'none')
                $(this).after('<div class="form-error"><span>' + text + '</span> </div>');
        }
    });

    return can;
}

function removeRequired() {
    $('.form-error').remove();

    $('.form-insert-image .insert-image-alert').hide();

    $('.type-area .type-area-title').removeClass('alerted');
    $('.type-area .type-area-title').text('Categoria:');
}