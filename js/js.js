/********** CONTAINS FUNCTION ***********/

jQuery.expr[':'].contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

/********** CHANGE TITLE PAGE ***********/

var clink = window.location.href;
$('header .header-options a[href="' + clink + '"] p').css('color', 'var(--color-primary)');
var clinks = clink.split("/")[clink.split("/").length - 1]

if (clinks.includes("cadastrar") || clinks.includes("editar") || clinks.includes("duplicar")) {
    $('header .header-options a[ref="cadastrar"] p').css('color', 'var(--color-primary)');

    if (clinks.includes("readed"))
        $('.onBoardingStart').remove();
} else if (clinks.includes("vendas"))
    $('header .header-options a[ref="vendas"] p').css('color', 'var(--color-primary)');

/********** OPACITY DROPDOWNS ***********/

function opacity(i) {
    $('#opacity-body').remove();
    if (i)
        $('body').append('<div id="opacity-body"></div>');
    else
        $('#opacity-body').remove();
}
function opacityOnModal(i) {
    $('#opacity-body-modal').remove();
    if (i)
        $('body').append('<div id="opacity-body-modal"></div>');
    else
        $('#opacity-body-modal').remove();
}
function opacitySelection(i) {
    $('#opacity-selection').remove();
    if (i)
        $('body').append('<div id="opacity-selection"></div>');
    else
        $('#opacity-selection').remove();
}
function opacitySale(i) {
    $('#opacity-sale').remove();
    if (i)
        $('body').append('<div id="opacity-sale"></div>');
    else
        $('#opacity-sale').remove();
}

/********** CONVERT STRING TO NUMBER **********/

function stringNumber(value) {
    var code = 0;
    for (var i = 0; i < value.length; i++) {
        code += (value.charCodeAt(i) - 65);
    }
    return code;
}

/********** MESSAGE ALERT **********/

function showMessageAlert(message, type = 'success') {
    $('<div class="alert-center alert-' + type + ' center-fix">' +
        '<p>' + message + '</p>' +
        '<svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M9.6979 8L14 12.3021L12.3021 14L8 9.6979L3.6979 14L2 12.3021L6.3021 8L2 3.6979L3.6979 2L8 6.3021L12.3021 2L14 3.6979L9.6979 8Z" fill="#25A65B" /> </svg> </div>').hide().appendTo('body').fadeIn(100);

    setTimeout(function () { $(".alert-center").remove() }, 2000);
}

$(document).on("click", ".alert-center", function () {
    $(".alert-center").remove();
});

/********** SLIDER CATEGORY **********/

var x, y, top, left, down, ref;

$('.slider-horizontal').mousedown(function (e) {
    e.preventDefault();
    down = true;
    ref = $(this);
    x = e.pageX;
    y = e.pageY;
    top = $(this).scrollTop();
    left = $(this).scrollLeft();
});

$("body").mousemove(function (e) {
    if (down) {
        var newX = e.pageX;
        var newY = e.pageY;

        $(ref).scrollTop(top - newY + y);
        $(ref).scrollLeft(left - newX + x);
    }
});

$("body").mouseup(function (e) { down = false; });

/********** MONEY MASK **********/

$(".money-mask").maskMoney();

/********** GENERAL MASKS **********/

var CardFone = function (val) {
    return '0000 0000 0000 0000';
},
    Options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(CardFone.apply({}, arguments), options);
        }
    };
$('#c_input').mask(CardFone, Options);

var CardDate = function (val) {
    return '00/0000';
},
    Options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(CardDate.apply({}, arguments), options);
        }
    };
$('#val_input').mask(CardDate, Options);

/********** REGISTER CATEGORY **********/

function openCategory(title, option, category = "") {
    opacity(true);
    $('html').append('<div class="register-category center-fix"> <p class="register-category-title">' + title + '</p> <div class="register-category-fields"> <p class="titleNotEmpty">Título da categoria</p> <input class="fieldNotEmpty" type="text" value="' + category + '" maxlength="28"> <p class="register-category-description textDefaultNotEmpty">Após cadastrar você poderá inserir ou copiar itens na nova categoria</p><p class="register-category-description alertTextNotEmpty" style="display:none">Insira um título</p></div> <div class="register-category-footer"> <p>Cancelar</p> <p href="' + $(this).attr('cat') + '">Salvar categoria</p> </div> </div>');

    $(document).on('input', '.register-category input[type="text"]', function () {
        $('.register-category-footer p:nth-child(2)').attr('href', option + 'category=' + $(this).val());
    });
}

$(document).on('click', '.register-category-footer p:nth-child(1)', function () {
    opacity(false);
    $('.register-category').remove();
});

$(document).on('click', '.register-category-footer p:nth-child(2)', function () {
    window.location.href = $(this).attr('href');
});

$(document).on('keypress', '.register-category input[type=text]', function (e) {
    if (e.which == 13) {
        $('.register-category-footer p:nth-child(2)').click();
    }
});

/*See Products*/

//add
$(document).on('click', '.see-products .add-category', function () {
    openCategory('Nova categoria de produtos', 'cadastrar?');
});

//edit
$(document).on('click', '.see-products .more-options .dropdown-item:nth-child(1)', function () {
    openCategory('Editando', 'changeCategoryProduct?oldCategory=' + $(this).attr('ref') + '&', $(this).attr('ref'));
});

//edit adittional
$(document).on('click', '.see-additional .more-options .dropdown-item:nth-child(1)', function () {
    openCategory('Editando', 'changeCategoryAdittional?oldCategory=' + $(this).attr('ref') + '&', $(this).attr('ref'));
});

/********** DISCONT VALUE **********/

$(document).on('click', '.add-value', function () {
    var ref = $(this).parent();

    $('.value-input input', ref).addClass('disabled');
    $(this).text('Novo preço com desconto:');
    $(this).addClass('added');
    $('.value-input-added', ref).css('display', 'flex');
    $('.value-input-added input[type="text"]', ref).focus();
});

$(document).on('click', '.trash-area', function () {
    var ref = $(this).parent().parent();

    $('.value-input input', ref).removeClass('disabled');
    $('.add-value', ref).text('Adicionar desconto');
    $('.add-value', ref).removeClass('added');
    $('.value-input-added', ref).css('display', 'none');
    $('.promotion-obj', ref).val('0,00');
});

/********** SVG INFO **********/

$(document).on("click", ".svg-info", function () {
    opacityOnModal(true);

    var title = $(this).attr('svgtitle');
    var body = $(this).attr('svgbody');

    $('body').append(
        "<div class='svg-info-message center-fix'>" +
        "<div class='svg-info-message-title'>" +
        "<svg width='17' height='16' viewBox='0 0 17 16' fill='none' xmlns='http://www.w3.org/2000/svg'> <g clip-path='url(#clip0)'> <path d='M8.5 15C6.64348 15 4.86301 14.2625 3.55025 12.9497C2.2375 11.637 1.5 9.85652 1.5 8C1.5 6.14348 2.2375 4.36301 3.55025 3.05025C4.86301 1.7375 6.64348 1 8.5 1C10.3565 1 12.137 1.7375 13.4497 3.05025C14.7625 4.36301 15.5 6.14348 15.5 8C15.5 9.85652 14.7625 11.637 13.4497 12.9497C12.137 14.2625 10.3565 15 8.5 15ZM8.5 16C10.6217 16 12.6566 15.1571 14.1569 13.6569C15.6571 12.1566 16.5 10.1217 16.5 8C16.5 5.87827 15.6571 3.84344 14.1569 2.34315C12.6566 0.842855 10.6217 0 8.5 0C6.37827 0 4.34344 0.842855 2.84315 2.34315C1.34285 3.84344 0.5 5.87827 0.5 8C0.5 10.1217 1.34285 12.1566 2.84315 13.6569C4.34344 15.1571 6.37827 16 8.5 16Z' fill='#4D4D4D'/> <path d='M9.42995 6.588L7.13995 6.875L7.05795 7.255L7.50795 7.338C7.80195 7.408 7.85995 7.514 7.79595 7.807L7.05795 11.275C6.86395 12.172 7.16295 12.594 7.86595 12.594C8.41095 12.594 9.04395 12.342 9.33095 11.996L9.41895 11.58C9.21895 11.756 8.92695 11.826 8.73295 11.826C8.45795 11.826 8.35795 11.633 8.42895 11.293L9.42995 6.588ZM9.49995 4.5C9.49995 4.76522 9.3946 5.01957 9.20706 5.20711C9.01952 5.39464 8.76517 5.5 8.49995 5.5C8.23474 5.5 7.98038 5.39464 7.79285 5.20711C7.60531 5.01957 7.49995 4.76522 7.49995 4.5C7.49995 4.23478 7.60531 3.98043 7.79285 3.79289C7.98038 3.60536 8.23474 3.5 8.49995 3.5C8.76517 3.5 9.01952 3.60536 9.20706 3.79289C9.3946 3.98043 9.49995 4.23478 9.49995 4.5Z' fill='#4D4D4D'/> </g> <defs> <clipPath id='clip0'> <rect width='16' height='16' fill='white' transform='translate(0.5)'/> </clipPath> </defs> </svg>" +
        "<p>" + title + "</p>" +
        "<svg width='17' height='16' viewBox='0 0 17 16' fill='none' xmlns='http://www.w3.org/2000/svg'> <path fill-rule='evenodd' clip-rule='evenodd' d='M10.1979 8L14.5 12.3021L12.8021 14L8.5 9.6979L4.1979 14L2.5 12.3021L6.8021 8L2.5 3.6979L4.1979 2L8.5 6.3021L12.8021 2L14.5 3.6979L10.1979 8Z' fill='var(--color-primary)'/> </svg>" +
        "</div>" +
        "<p class='svg-info-message-message'>" + body + "</p>" +
        "</div>");
});

$(document).on("click", ".svg-info-message-title svg:nth-child(3)", function () {
    opacityOnModal(false);
    $(".svg-info-message").remove();
});

/********** CATEGORY DROPDOWN **********/

$(document).on('click', '.category-dropdown input[type="checkbox"]', function () {
    $('.category-dropdown input[type="checkbox"].selected').prop('checked', false);
    $('.category-dropdown input[type="checkbox"].selected').removeClass('selected');
    $(this).addClass('selected');
});

/********* VALIDATE FORM **********/

function validateForm() {
    var validate = true;
    $(".fieldNotEmpty").each(function (index) {
        if ($(this).val() === '' || $(this).val() === null) {
            $('.textDefaultNotEmpty').eq(index).hide();
            $('.alertTextNotEmpty').eq(index).show();
            $('.alertTextNotEmpty').eq(index).css('color', '#B00020');
            $(this).css('border', '1px solid #B00020');
            $('.titleNotEmpty').eq(index).css('color', '#B00020');

            validate = false;
        } else {
            $('.textDefaultNotEmpty').eq(index).show();
            $('.alertTextNotEmpty').eq(index).hide();
            $(this).css('border', '1px solid #DDDDDD');
            $('.titleNotEmpty').eq(index).css('color', '#4D4D4D');
        }
    });

    return validate;
}

/******** INPUT QUANTITY *********/

$(document).on('click', '.input-quantity .ml-quantity path:nth-child(1), .input-quantity .ml-quantity path:nth-child(2)', function () {
    var ref = $(this).parent().parent().parent();

    var value = $('.input-quantity input', ref).val() == '' || $('.input-quantity input', ref).val() == null ? 0 : parseInt($('.input-quantity input', ref).val());
    var newValue = value + 1;

    $('.input-quantity input', ref).val(newValue);
});

$(document).on('click', '.input-quantity .ml-quantity path:nth-child(3), .input-quantity .ml-quantity path:nth-child(4)', function () {
    var ref = $(this).parent().parent().parent();

    var value = $('.input-quantity input', ref).val() == '' || $('.input-quantity input', ref).val() == null ? 0 : parseInt($('.input-quantity input', ref).val());
    var newValue = $('.input-quantity input', ref).attr('min') !== undefined && value - 1 < $('.input-quantity input', ref).attr('min') ? value : value - 1 < 0 ? 0 : value - 1;

    $('.input-quantity input', ref).val(newValue);
});

/*Adittional*/

$(document).on('click', '.input-quantity[in="adittional"] .ml-quantity', function () {
    var ref = $(this).parent().parent().parent();

    $('.input-quantity', ref).removeClass('unlimited');

    var value = $('.input-quantity input', ref).val();

    if (value == 0) {
        $('.input-quantity', ref).addClass('unlimited');
        value = '';
    }

    $('.input-quantity input', ref).val(value);
});

$(document).on('click', '.input-quantity[in="adittional"] svg.text', function () {
    var ref = $(this).parent();

    $(ref).removeClass('unlimited');
    $('input[type="number"]', ref).focus();
});


$(document).on('keyup', '.input-quantity[in="adittional"] input', function () {
    var ref = $(this).parent();

    $(ref).removeClass('unlimited');

    var value = $(this).val();

    if (value > 0) {
        $(ref).removeClass('unlimited');
    } else {
        $(ref).addClass('unlimited');
        value = '';
    }

    $('.input-quantity input', ref).val(value);
});

/********* NORMALIZE MONEY **********/

function nmoney(val) {
    val = val.split('.').join("");
    val = val.replace(",", ".");

    return val;
}

function nmoneyf(val) {
    val = val.split('.').join("");
    val = val.replace(",", ".");

    return parseFloat(val);
}

/********* MONEY **********/

function money(val = "") {
    val = parseFloat(val);

    return val.toFixed(2).replace(".", ",");
}

/********* PROMOTION CALC *********/

function promotionCalc(price, promotion) {
    return ((nmoney(price) - nmoney(promotion)) / nmoney(price)) * 100;
}

/********* ON CHECKBOX CHANGE *********/

$(document).on('change', '.cl-switch input', function (event) {
    if (event.currentTarget.checked) {
        $('.title-disponibility', $(this).parent().parent()).text('Disponível');
    } else {
        $('.title-disponibility', $(this).parent().parent()).text('Indisponível');
    }
});

/********* CATEGORY ALL *********/

$(document).on('click', '.category-all', function () {
    var ref = $(this).parent();
    var root = $(this).parent().parent().parent();

    $('.category', ref).removeClass('selected');
    $(this).addClass('selected');

    $('.popup-item', root).hide();
    $('.popup-item', root).addClass('pointer');
    $('.popup-item input[type=checkbox]:checked', root).each(function () {
        var ref = $(this).parents('.popup-item');

        if ($(ref).attr('ref') != undefined) {
            $('.popup-header[ref="' + $(ref).attr('ref') + '"]', root).show();
        }

        if (!$(ref).hasClass('header-adittional')) {
            if ($(ref).hasClass('combo-products-item'))
                $(ref).show();
            else
                $(ref).css('display', 'flex');
        }
    });
});

/********** CURRENT PATH ***********/

function getCurrentPath() {
    //return window.location.href.split("www.nomercadosoft.com.br/")[1];
    return window.location.href.split("painelweb/")[1];
}

/********* FEEDBACK BUTTON *********/

$(document).on('click', '.feedback-button', function () {
    if ($('.feedback-area').is(':visible')) {
        $('.feedback-area-footer p:nth-child(1)').click();
    } else {
        opacitySelection(true);
        $('.feedback-area').show();
    }
});

$(document).on('click', '.feedback-area-header svg, .feedback-area-footer p:nth-child(1)', function () {
    opacitySelection(false);
    $('.feedback-area').hide();
    $('#feedback-selection').css('height', '0').css('width', '0');
    $('.feedback-area-body #feedback-capture').remove();
    $('.feedback-area-body textarea').val('');
});

$(document).on('click', '.feedback-area-body .capture-button', function () {
    opacitySelection(false);
    $('.feedback-area').hide();

    selectAreaFeedback();
});

var canvasCapture = null;
function selectAreaFeedback() {
    var start = {};
    var end = {};
    var isSelecting = false;

    $('html').css('cursor', 'crosshair');
    $('body').append('<div id="feedback-selection"></div>');

    $(window)
        .on('mousedown', function ($event) {
            isSelecting = true;
            start.x = $event.pageX;
            start.y = $event.pageY;

            $('#feedback-selection').css({
                left: start.x,
                top: start.y
            });
        })
        .on('mousemove', function ($event) {
            if (!isSelecting) { return; }

            end.x = $event.pageX;
            end.y = $event.pageY;

            $('#feedback-selection').css({
                left: start.x < end.x ? start.x : end.x,
                top: start.y < end.y ? start.y : end.y,
                width: Math.abs(start.x - end.x),
                height: Math.abs(start.y - end.y)
            });
        })
        .on('mouseup', function ($event) {
            $('html').css('cursor', 'auto');

            $('#feedback-capture').remove();
            $(window).off('mousedown').off('mousemove').off('mouseup');

            isSelecting = false;

            html2canvas(document.querySelector('html'), { scrollY: -window.scrollY, height: window.screen.height }).then(canvas => {
                canvasCapture = canvas.toDataURL();
                $('.feedback-area-body').append('<div id="feedback-capture" style="background-size: cover; background-position: center center; background-repeat: no-repeat; width: 664px; height: 373.50px; background-image: url(' + canvas.toDataURL() + ');"></div>');
            });

            $('#feedback-selection').remove();

            opacitySelection(true);
            $('.feedback-area').show();
        });
}

$(document).on('click', '#feedback-capture', function () {
    if (!$(this).hasClass('view')) {
        $(this).addClass('view');
        $(this).appendTo('body');
    } else {
        $(this).removeClass('view');
        $(this).appendTo('.feedback-area-body');
    }
});

$(document).on('click', '.feedback-area-footer button', function () {
    var descricao = $('.feedback-area-body textarea').val();;
    var pagina = getCurrentPath();

    $.ajax({
        type: 'POST',
        url: 'enviarfeedback',
        cache: false,
        data: {
            descricao: descricao,
            pagina: pagina,
            captura: canvasCapture
        },
        dataType: 'text'
    });

    window.location.href = getCurrentPath();
});

/********** UPLOAD IMAGEM FEEDBACK **********/

var readURLFeedback = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#feedback-capture').remove();
            canvasCapture = e.target.result;
            $('.feedback-area-body').append('<div id="feedback-capture" style="background-size: cover; background-position: center center; background-repeat: no-repeat; width: 664px; height: 373.50px; background-image: url(' + e.target.result + ');"></div>');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('change', '.feedback-area-body-buttons input[type="file"]', function () {
    readURLFeedback(this);
});

$(document).on('click', '.feedback-area-body-buttons .select-image', function () {
    $('.feedback-area-body-buttons input[type="file"]').click();
});

/********** INTERVAL **********/

function saleSound() {
    var audio = new Audio('/painelweb/audio/saleSound.ogg');
    audio.play();
}

function verifyEach() {
    setInterval(function () {
        intervalFunctions();
    }, 30000);
}

intervalFunctions();

/********** SALE MODAL **********/

function intervalFunctions() {
    $.ajax({
        type: 'POST',
        url: 'interval',
        cache: false,
        data: {},
        dataType: 'json'
    }).done(function (data) {
        var data = data.sales
        var qtd = data.length;
        if (qtd > 0) {
            if ($('.new-sales-area.closed').length < 1)
                opacitySale(true);

            $('.new-sales-area').show();
            $('.new-sales').text('');
            $('.new-sales').append(data);
            saleSound();
            calculateSales();
        } else {
            opacitySale(false);
            $('.new-sales-area').hide();
        }
    }).fail(function (data) {
        console.log(data);
    });
}

$(document).on('click', '.new-sale-footer p:nth-child(2)', function () {
    var root = $(this).parents('.new-sale');
    var cod = $(root).attr('ref');
    var phone = $(root).attr('phone');
    var type = $(root).attr('type');

    $.ajax({
        type: 'POST',
        url: 'allowOrderByKey',
        cache: false,
        data: {
            cod: cod,
            phone: phone,
            type: type
        },
        dataType: 'json'
    }).done(function (data) {
        $(root).remove();
        calculateSales();

        if (window.location.href.includes('vendas')) {
            $('.kanban-order[key="' + data.key + '"]').remove();

            $('.kanban-column:first-child .simplebar-content').prepend(data.order);
            if ($('.kanban-column:first-child .kanban-header-title .title-cont').length > 0)
                $('.kanban-column:first-child .kanban-header-title .title-cont').text($('.kanban-column:first-child .kanban-order').length);
            else
                $('.kanban-column:first-child .kanban-header-title').append('<p class="title-cont">1</p>');

            $('.ordening p:nth-child(1)').text($('.kanban-column .kanban-order').length);
        }
    }).fail(function (data) {
        location.reload();
    });
});

$(document).on('click', '.new-sale-footer p:nth-child(1)', function () {
    var root = $(this).parents('.new-sale');

    $('.new-sale-footer', root).hide();
    $('.new-sale-refuse', root).show();
});

$(document).on('click', '.new-sale-refuse-footer p:nth-child(1)', function () {
    var root = $(this).parents('.new-sale');

    $('.new-sale-refuse', root).hide();
    $('.new-sale-footer', root).show();
});

$(document).on('click', '.new-sale-refuse-footer button', function () {
    $(this).prop("disabled", true);
    var root = $(this).parents('.new-sale');
    var cod = $(root).attr('ref');
    var telefone = $(root).attr('phone').replace(/[^A-Z0-9]+/ig, "");
    var tipo = $(root).attr('type');
    var reason = $('.new-sale-refuse textarea', root).val();

    $.ajax({
        type: 'POST',
        url: 'cancelSale',
        cache: false,
        data: { key: cod, reason: reason, telefone: telefone, tipo: tipo },
        dataType: 'text'
    }).done(function (data) {
        $(this).prop("disabled", false);
        $(root).remove();
        calculateSales();
    }).fail(function (data) {
        location.reload();
    });
});

$(document).on('click', '.new-sale-footer button:nth-child(3)', function () {
    var root = $(this).parents('.new-sale');
    var cod = $(root).attr('ref');
    var tipo = $(root).attr('type');
    var telefone = $(root).attr('phone').replace(/[^A-Z0-9]+/ig, "");

    $('.new-sales').addClass('printing');
    $.ajax({
        type: 'POST',
        url: 'printSale',
        cache: false,
        data: {
            cod: cod,
            tipo: tipo,
            telefone: telefone
        },
        dataType: 'text'
    }).done(function (data) {
        $('.new-sales').append(data);
        html_pdf();
    }).fail(function (data) {
        location.reload();
    });
});

function html_pdf() {
    window.scrollTo(0, 0);
    var jsPDF = window.jspdf.jsPDF;

    html2canvas(document.getElementById("new-sale-print"), {
        scale: 5
    }).then(function (canvas) {
        $('.new-sale-print').remove();
        $('.new-sales').removeClass('printing');
        var largura = canvas.width;
        var altura = canvas.height;

        var img = canvas.toDataURL("image/jpeg");
        const doc = new jsPDF('p', 'pt', [largura, altura]);
        doc.addImage(img, 'JPEG', 0, 0, largura, altura);
        doc.autoPrint();
        window.open(doc.output('bloburl'), '_blank');
    });
}

$(document).on('click', '.new-sales-close', function () {
    if ($('.new-sales-area').hasClass('closed')) {
        $('.new-sales-area').removeClass('closed');
        opacitySale(true);
    }
    else {
        $('.new-sales-area').addClass('closed');
        opacitySale(false);
    }
});

function calculateSales() {
    var qtd = $(".new-sales .new-sale").length;

    if (qtd > 0) {
        if (qtd > 1)
            $('.new-sales-close-info-title').text(qtd + " Pedidos aguardando aprovação");
        else
            $('.new-sales-close-info-title').text(qtd + " Pedido aguardando aprovação");

    } else {
        $('.new-sales-area').hide();
        opacitySale(false);
    }
}

/*Tabs main*/

$(document).on('click', '.tabs-categories-item', function () {
    var ref = $(this).parents('.tabs-main');

    $('.tabs-categories-item', ref).removeClass('selected');
    $(this).addClass('selected');

    $('.tabs-main-area', ref).hide();
    $('.tabs-main-area.' + $(this).index(), ref).show();
});

/*Add Zero*/

function addZero(val, right = false) {
    if (parseInt(val) < 10) {
        if (right)
            return val + "0";
        else
            return "0" + val;
    } else
        return val;
}

/*Plan*/

function planList(data) {
    if (data.status != undefined && data.status == 203) {
        $.ajax({
            type: 'POST',
            url: 'render',
            cache: false,
            data: { view: 'modal/play-upgrade-modal' },
            dataType: 'text'
        }).done(function (data) {
            $('#opacity-body').click();
            opacity(true);
            $('body').append(data);
        })
    } else
        return false;
}

/*On Boarding Start*/

$(document).on('click', '.onBoardingStartButton', function () {
    if (clinks.includes("cadastrar"))
        $('.onBoardingStart').remove();
    else
        window.location.href = "cadastrar?readed";
});

/*On Boarding Help*/

$(document).on('click', '.onBoardingHelpHeader svg', function () {
    $('.onBoardingHelp').addClass('minimized');
});

$(document).on('click', '.onBoardingHelp.minimized', function () {
    $(this).removeClass('minimized');
});

/*See Session*/

// $(document).bind('keydown', function (e) {
//     if (e.keyCode == 74) {
//         $.ajax({
//             type: 'POST',
//             url: 'seeSession',
//             cache: false,
//             data: {},
//             dataType: 'json'
//         }).done(function (data) {
//             console.log(data);
//         }).fail(function (data) {
//             console.log(data);
//         });
//     }
// });