<?php

require __DIR__ . '/vendor/autoload.php';

use App\Http\Middleware\Queue as MiddlewareQueue;
use \App\Http\Router;
use App\Utils\View;

if (strpos(__DIR__, 'xampp'))
    define('URL', 'https://localhost/painelweb');
else
    define('URL', 'https://cardapio.ai/painelweb');

View::init([
    'URL' => URL,
    'version' => '1.085'
]);

MiddlewareQueue::setMap([
    'notlogged' => \App\Http\Middleware\RequireAdminLogout::class,
    'logged' => \App\Http\Middleware\RequireAdminLogin::class,
    'plan' => \App\Http\Middleware\PlanAllows::class,
]);

MiddlewareQueue::setDefault([]);

$obRouter = new Router(URL);

include __DIR__ . '/routes/pages.php';
include __DIR__ . '/routes/admin.php';
include __DIR__ . '/routes/users.php';

$obRouter->run()->sendResponse();
