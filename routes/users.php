<?php

use App\Controller\Admin\Login;
use App\Http\Response;

$obRouter->get('/whitelabel', [
    'middlewares' => [
        'notlogged'
    ],
    function ($data) {
        return new Response(200, Login::getLoginWl($data));
    }
]);

$obRouter->post('/whitelabel', [
    'middlewares' => [
        'notlogged'
    ],
    function ($request,$data) {
        return new Response(200, Login::setLogin($request, $data));
    }
]);
