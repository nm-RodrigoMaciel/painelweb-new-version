<?php

use App\Controller\Admin\Login;
use App\Http\Response;

$obRouter->get('/admin/login',[
    'middlewares' => [
        'notlogged'
    ],
    function(){
        return new Response(200,Login::getLogin());
    }
]);

$obRouter->post('/admin/login',[
    'middlewares' => [
        'notlogged'
    ],
    function($request){
        return new Response(200,Login::setLogin($request));
    }
]);

$obRouter->get('/admin/loggout',[
    'middlewares' => [
        'logged'
    ],
    function($request){
        return new Response(200,Login::setLoggout($request));
    }
]);

$obRouter->post('/admin/create',[
    'middlewares' => [
        'notlogged'
    ],
    function($request){
        return new Response(200,Login::getCreate($request));
    }
]);

$obRouter->post('/admin/account',[
    'middlewares' => [
        'notlogged'
    ],
    function($request){
        return new Response(200,Login::setCreate($request));
    }
]);
