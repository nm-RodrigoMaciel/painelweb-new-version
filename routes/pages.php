<?php

use App\Controller\Feedback;
use App\Http\Response;
use App\Controller\Foods;
use App\Controller\General;
use App\Controller\Interval;
use App\Controller\Page;

$obRouter->get('/listar', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, Foods\Product\ListProduct::getListProduct());
    }
]);

$obRouter->get('/adicionais', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, Foods\Additional\ListAdditional::getlistAdditional());
    }
]);

$obRouter->get('/cadastrar', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, Foods\Product\RegisterProduct::getRegisterProduct());
    }
]);

$obRouter->post('/registerProduct', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\RegisterProduct::setProduct());
    }
]);

$obRouter->post('/updateProduct', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::updateProduct());
    }
]);

$obRouter->get('/editar', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::getEditProduct());
    }
]);

$obRouter->get('/duplicar', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::getEditProduct());
    }
]);

$obRouter->post('/excludeProduct', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::excludeProduct());
    }
]);

$obRouter->post('/deleteCategory', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\DeleteCategory::getDeleteCategoryFunction());
    }
]);

$obRouter->post('/fastUpdate', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::fastUpdateProduct());
    }
]);

$obRouter->post('/registerAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::getRegisterAdittional());
    }
]);

$obRouter->post('/editAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::getEditAdittional());
    }
]);

$obRouter->post('/fastUpdateAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::fastUpdateAdittional());
    }
]);

$obRouter->post('/deleteAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::getDeleteCategoryFunction());
    }
]);

$obRouter->post('/updateCategories', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::updateCategoriaProdutos());
    }
]);

$obRouter->get('/changeCategoryProduct', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::changeCategoriaProduto());
    }
]);

$obRouter->get('/changeCategoryAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::changeCategoryAdittional());
    }
]);

$obRouter->post('/copyProductsCategory', [
    'middlewares' => [
        'logged',
        'plan' => 0
    ],
    function () {
        return new Response(200, Foods\Product\EditProduct::copyProductsCategory());
    }
]);

$obRouter->post('/updateCategoriesAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::updateCategoryAdittional());
    }
]);

$obRouter->post('/excludeAdittional', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::excludeAdittional());
    }
]);

$obRouter->post('/copyAdittionalCategory', [
    'middlewares' => [
        'logged',
        'plan' => 1
    ],
    function () {
        return new Response(200, Foods\Additional\EditAdittional::copyAdittionalCategory());
    }
]);

$obRouter->get('/vendas', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, Foods\Sales\Sales::getSales());
    }
]);

$obRouter->post('/getSaleByKey', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return Foods\Sales\Sales::getSaleByKey();
    }
]);

$obRouter->post('/cancelSale', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return Foods\Sales\Sales::cancelSale();
    }
]);

$obRouter->post('/changeStatus', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return Foods\Sales\Sales::changeStatus();
    }
]);

$obRouter->post('/changeTitle', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return Foods\Sales\Sales::changeTitle();
    }
]);

$obRouter->post('/restoreTitle', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return Foods\Sales\Sales::restoreTitle();
    }
]);

$obRouter->post('/enviarfeedback', [
    'middlewares' => [
        'logged'
    ],
    function ($request) {
        return Feedback::feedback($request);
    }
]);

$obRouter->post('/interval', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, Interval::interval());
    }
]);

$obRouter->get('/getSalesFirebase', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, Foods\Sales\Sales::getSalesFirebase());
    }
]);

$obRouter->post('/verifySales', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return new Response(200, Foods\Sales\Sales::verifySales());
    }
]);

$obRouter->post('/removeSale', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return new Response(200, Foods\Sales\Sales::removeSale());
    }
]);

$obRouter->get('/account', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, General\Account::getAccount());
    }
]);

$obRouter->post('/customize', [
    'middlewares' => [
        'logged'
    ],
    function ($request) {
        return new Response(200, General\Account::customizeAccount());
    }
]);

$obRouter->post('/printSale', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function ($request) {
        return new Response(200, Foods\Sales\Sales::printSale());
    }
]);

$obRouter->post('/removeCoupon', [
    'middlewares' => [
        'logged',
        'plan' => 10
    ],
    function ($request) {
        return new Response(200, General\Account::removeCoupon());
    }
]);

$obRouter->post('/createCoupon', [
    'middlewares' => [
        'logged',
        'plan' => 10
    ],
    function ($request) {
        return new Response(200, General\Account::createCoupon());
    }
]);

$obRouter->post('/statusCoupon', [
    'middlewares' => [
        'logged',
        'plan' => 10
    ],
    function ($request) {
        return new Response(200, General\Account::statusCoupon());
    }
]);

$obRouter->post('/getByKeyCoupon', [
    'middlewares' => [
        'logged',
        'plan' => 10
    ],
    function ($request) {
        return new Response(200, General\Account::getByKeyCoupon());
    }
]);

$obRouter->post('/render', [
    'middlewares' => [
        'logged'
    ],
    function ($request) {
        return new Response(200, Page::render());
    }
]);

$obRouter->post('/new-link', [
    'middlewares' => [
        'logged',
        'plan' => 11
    ],
    function ($request) {
        return new Response(200, General\Account::newLink());
    }
]);

$obRouter->post('/remove-link', [
    'middlewares' => [
        'logged',
        'plan' => 11
    ],
    function ($request) {
        return new Response(200, General\Account::removeLink());
    }
]);

$obRouter->post('/refresh-preview', [
    'middlewares' => [
        'logged',
        'plan' => 11
    ],
    function ($request) {
        return new Response(200, General\Account::refreshPreview());
    }
]);

$obRouter->post('/disable-button', [
    'middlewares' => [
        'logged',
        'plan' => 11
    ],
    function () {
        return new Response(200, General\Account::disableButton());
    }
]);

$obRouter->post('/new-hours', [
    'middlewares' => [
        'logged',
        'plan' => 7
    ],
    function ($request) {
        return new Response(200, General\Account::newHours());
    }
]);

$obRouter->post('/remove-hours', [
    'middlewares' => [
        'logged',
        'plan' => 7
    ],
    function ($request) {
        return new Response(200, General\Account::removerHorarios());
    }
]);

$obRouter->post('/new-special-hours', [
    'middlewares' => [
        'logged',
        'plan' => 7
    ],
    function ($request) {
        return new Response(200, General\Account::newSpecialHours());
    }
]);

$obRouter->post('/status-special-hour', [
    'middlewares' => [
        'logged',
        'plan' => 7
    ],
    function ($request) {
        return new Response(200, General\Account::statusSpecialHour());
    }
]);

$obRouter->post('/remove-special-hour', [
    'middlewares' => [
        'logged',
        'plan' => 7
    ],
    function ($request) {
        return new Response(200, General\Account::removeSpecialHour());
    }
]);

$obRouter->post('/enable-store', [
    'middlewares' => [
        'logged'
    ],
    function ($request) {
        return new Response(200, General\Account::enableStore());
    }
]);

$obRouter->post('/config-delivery', [
    'middlewares' => [
        'logged',
        'plan' => 8
    ],
    function ($request) {
        return new Response(200, General\Account::configDelivery());
    }
]);

$obRouter->post('/allowOrderByKey', [
    'middlewares' => [
        'logged',
        'plan' => 2
    ],
    function () {
        return new Response(200, Foods\Sales\Sales::allowOrderByKey());
    }
]);

$obRouter->post('/etiquetasQr', [
    'middlewares' => [
        'logged',
        'plan' => 5
    ],
    function () {
        return new Response(200, General\Account::etiquetasQr());
    }
]);

$obRouter->post('/tipoVerificacaoQr', [
    'middlewares' => [
        'logged',
        'plan' => 5
    ],
    function () {
        return new Response(200, General\Account::tipoVerificacaoQr());
    }
]);

$obRouter->post('/valorMinimo', [
    'middlewares' => [
        'logged',
        'plan' => 9
    ],
    function () {
        return new Response(200, General\Account::valorMinimo());
    }
]);

$obRouter->post('/habilitarDinheiro', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, General\Account::habilitarDinheiro());
    }
]);

$obRouter->post('/novosCartoes', [
    'middlewares' => [
        'logged',
        'plan' => 9
    ],
    function () {
        return new Response(200, General\Account::novosCartoes());
    }
]);

$obRouter->post('/atualizarCartoes', [
    'middlewares' => [
        'logged',
        'plan' => 9
    ],
    function () {
        return new Response(200, General\Account::atualizarCartoes());
    }
]);

$obRouter->post('/planChange', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, General\Account::planChange());
    }
]);

$obRouter->post('/planPayment', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, General\Account::planPayment());
    }
]);

$obRouter->post('/planCancel', [
    'middlewares' => [
        'logged'
    ],
    function () {
        return new Response(200, General\Account::planCancel());
    }
]);

// $obRouter->post('/seeSession',[
//     'middlewares' => [
//         'logged'
//     ],
//     function(){
//         return new Response(200,json_encode($_SESSION));
//     }
// ]);