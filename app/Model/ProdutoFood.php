<?php

namespace App\Model;

use \App\Http\Router;
use \App\Model\Estabelecimento;

class ProdutoFood
{

    public static function getProdutos()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo()
        ];

        return Router::call($jsonData, 'web/produtos/listar');
    }

    public static function getProdutosCombo($cod)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "codigoBarras" => $cod
        ];

        return Router::call($jsonData, 'web/produtos/listarCombo');
    }

    public static function getProduto($cod)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => (int)$cod
        ];

        return Router::call($jsonData, 'web/produtos/listar');
    }

    public static function setProdutos($itens, $base)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens,
            "imgBase64" => $base
        ];

        return json_encode(Router::call($jsonData, 'web/produtos/cadastrar'));
    }

    public static function atualizarProduto($itens,$base)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens,
            "imgBase64" => $base
        ];

        echo json_encode(Router::call($jsonData, 'web/produtos/alterarproduto'));
    }

    public static function getIngredientes()
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo()
        ];

        return Router::call($jsonData, 'web/listasingredientes/');
    }

    public static function getIngredientesSelected($target = '', $cod = '')
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $target,
            "codigoBarras" => $cod
        ];

        return Router::call($jsonData, 'web/listasingredientesselected');
    }

    public static function deleteCategory($category)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $category
        ];

        echo json_encode(Router::call($jsonData, 'web/produtos/deletarcategoria'));
    }

    public static function deleteProduct($cod)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "codigoBarras" => $cod
        ];

        echo json_encode(Router::call($jsonData, 'web/produtos/deletar'));
    }

    public static function fastUpdateProduct($itens)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens
        ];

        echo json_encode(Router::call($jsonData, 'web/produtos/alterarpreco'));
    }

    public static function getListaProduto($cod)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $cod
        ];

        return Router::call($jsonData, 'web/listas');
    }

    public static function updateCategoriaProdutos($cat,$disp)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $cat,
            "disponivel" => $disp
        ];

        return Router::call($jsonData, 'web/produtos/disponibilizarcategoria');
    }

    public static function changeCategoriaProduto($oldCat,$cat)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $cat,
            "oldCategoria" => $oldCat
        ];

        return Router::call($jsonData, 'web/produtos/alterarcategoria');
    }

    public static function copyProductsCategory($itens,$cat)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens,
            "key" => $cat
        ];

        return Router::call($jsonData, 'web/produtos/copiarproduto');
    }
}
