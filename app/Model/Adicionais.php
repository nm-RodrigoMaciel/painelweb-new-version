<?php

namespace App\Model;

use \App\Http\Router;
use \App\Model\Estabelecimento;

class Adicionais
{

    public static function registerAdittional($itens)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens
        ];

        echo json_encode(Router::call($jsonData, 'web/listas/cadastrar'));
    }

    public static function editAdittional($itens, $cat)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens,
            "categoriaAntiga" => $cat
        ];

        echo json_encode(Router::call($jsonData, 'web/listas/alteraradicional'));
    }

    public static function deleteCategory($category)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $category
        ];

        echo json_encode(Router::call($jsonData, 'web/listas/deletarcategoria'));
    }

    public static function deleteAdittional($key,$cod)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $key,
            "codigoBarras" => $cod
        ];

        echo json_encode(Router::call($jsonData, 'web/listas/deletar'));
    }

    public static function fastUpdateAdittional($itens)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens
        ];

        echo json_encode(Router::call($jsonData, 'web/listas/alterarpreco'));
    }

    public static function changeCategoryAdittional($oldCat,$cat)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $cat,
            "oldCategoria" => $oldCat
        ];

        return Router::call($jsonData, 'web/listas/alterarcategoria');
    }

    public static function updateCategoriaAdicionais($cat,$disp)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $cat,
            "disponivel" => $disp
        ];

        return Router::call($jsonData, 'web/listas/disponibilizarcategoria');
    }

    public static function copyAdittionalCategory($itens,$cat)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "itens" => $itens,
            "key" => $cat
        ];

        return Router::call($jsonData, 'web/listas/copiaradicional');
    }
}
