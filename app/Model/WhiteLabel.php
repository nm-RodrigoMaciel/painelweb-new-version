<?php

namespace App\Model;

use App\Http\Router;

class WhiteLabel
{
    public static function getWhiteLabel($user)
    {
        $strJsonFileContents = file_get_contents("../corporateUsers/users.json");
        $data = json_decode($strJsonFileContents, true);

        return $data[$user] ?? null;
    }

    public static function setAccount($data)
    {
        $jsonData = [
            "usuarioCorporativo" => [
                "codigoAcesso" => $data['codigoAcesso'],
                "codigoEstabelecimento" => (int)$data['codigoEstabelecimento'],
                "email" => $data['email'],
                "nome" => $data['nome'],
                "nomeEstabelecimento" => $data['nomeEstabelecimento'],
                "telefone" => $data['telefone'],
                "tipoEstabelecimento" => $data['tipoEstabelecimento'],
                "password" => $data['password']
            ],
            "corporateName" => "ADMIN",
            "token" => "55b3b80e7124348a1e5eb8050098e23c"
        ];

        return json_encode(Router::call($jsonData, 'usuarioscorporativos/cadastro'));
    }

    public static function customizeAccount($data)
    {
        $jsonData = [
            "corporateName" => "ADMIN",
            "token" => "55b3b80e7124348a1e5eb8050098e23c",
            "estabelecimento" => [
                "tipoEstabelecimento" => Estabelecimento::Tipo(),
                "nome" => Estabelecimento::Nome(),
                "codigoEstabelecimento" => Estabelecimento::Codigo(),
                "cnpj" => Estabelecimento::Cnpj(),
                "colorPrimary" => $data['colorPrimary'],
                "telefoneWhatsApp" => $data['telefoneWhatsApp'],
                "endereco" => $data['endereco'],
                "retirada" => json_decode($data['retirada'], true),
                "paletteColors" => $data['paletteColors']
            ],
            "imgBase64" => $data['image']
        ];

        return json_encode(Router::call($jsonData, 'web/estabelecimentos/alterar'));
    }

    public static function getBasicInfo()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
        ];

        return Router::call($jsonData, 'web/estabelecimentosadmin/basicinfo');
    }

    public static function getLink()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo()
        ];

        return Router::call($jsonData, 'web/estabelecimentos/listarLinkTree');
    }

    public static function newLink($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $data['key'],
            "linkTree" => [
                "title" => $data['title'],
                "link" => $data['link']
            ]
        ];

        return Router::call($jsonData, 'web/estabelecimentos/adicionarLinkTree');
    }

    public static function removeLink($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $data['key']
        ];

        return Router::call($jsonData, 'web/estabelecimentos/removerLinkTree');
    }

    public static function getHours()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo()
        ];

        return Router::call($jsonData, 'web/estabelecimentos/listarHorarios');
    }

    public static function hours($data, $array_days)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "horario" => [
                "titulo" => $data['name'],
                "dias" => $array_days,
                "abre" => $data['abre'],
                "fecha" => $data['fecha']
            ],
            "index" => $data['index'] == "" ? null : $data['index']
        ];

        return Router::call($jsonData, 'web/estabelecimentos/horarios');
    }

    public static function removeHours($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $data['key']
        ];

        return Router::call($jsonData, 'web/estabelecimentos/removerHorarios');
    }

    public static function newSpecialHours($data)
    {

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "diadasemana" => $data['semana'],
            "dia" => $data['dia'],
            "mes" => $data['mes'],
            "ano" => $data['ano'],
            "abre" => $data['abre'],
            "fecha" => $data['fecha']
        ];

        return Router::call($jsonData, 'web/estabelecimentos/adicionarDataIndisponivel');
    }

    public static function getSpecialHours()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo()
        ];

        return Router::call($jsonData, 'web/estabelecimentos/listarDataIndisponivel');
    }

    public static function disponibilidadeSpecialHours($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $data['key'],
            "disponibilidade" => json_decode($data['disponibilidade'], true)
        ];

        return Router::call($jsonData, 'web/estabelecimentos/disponibilidadeDataIndisponivel');
    }

    public static function removeSpecialHours($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $data['key']
        ];

        return Router::call($jsonData, 'web/estabelecimentos/removerDataIndisponivel');
    }

    public static function listConfigDelivery()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo()
        ];

        return Router::call($jsonData, 'web/estabelecimentos/listarAjustesEntrega');
    }

    public static function configDelivery($itens)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "itens" => $itens
        ];

        return Router::call($jsonData, 'web/estabelecimentos/atualizarAjustesEntrega');
    }

    public static function etiquetasQr($qtd)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "qtd" => $qtd
        ];

        return Router::call($jsonData, 'web/estabelecimentos/etiquetasQr');
    }

    public static function tipoVerificacaoQr($checked)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "checked" => $checked
        ];

        return Router::call($jsonData, 'web/estabelecimentos/tipoVerificacaoQr');
    }

    public static function getEstabelecimento()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo()
        ];

        return Router::call($jsonData, 'web/estabelecimentosadmin/getEstabelecimento');
    }

    public static function valorMinimo($valorMinimo)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "valorMinimo" => $valorMinimo
        ];

        return Router::call($jsonData, 'web/estabelecimentos/pedidoMinimo');
    }

    public static function habilitarDinheiro($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "status" => json_decode($data['status'], true)
        ];

        return Router::call($jsonData, 'web/estabelecimentos/pagamentoDinheiro');
    }

    public static function atualizarTipoPagamento($cartoes)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "cartoes" => $cartoes
        ];

        return Router::call($jsonData, 'web/estabelecimentos/atualizarTipoPagamento');
    }

    public static function novosCartoes($cartao)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "cartao" => $cartao
        ];

        return Router::call($jsonData, 'web/estabelecimentos/novosCartoes');
    }

    public static function desabilitarBotao($botao, $enable)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "botao" => $botao,
            "status" => $enable
        ];

        return json_encode(Router::call($jsonData, 'cardapiodigital/desabilitarBotao/'));
    }

    public static function planos()
    {
        $jsonData = [];

        return Router::call($jsonData, 'planos/listPlan/');
    }

    public static function enableStore($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "habilitar" => json_decode($data['enable'], true)
        ];

        return Router::call($jsonData, 'web/estabelecimentos/alterarDisponibilidade');
    }

    public static function createSubscription($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo() . "",
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "telefone" => Estabelecimento::Telefone(),
            "email" => Estabelecimento::Email(),
            "cpf_cnpj" => $data['card']['cpf_input'],
            "rua" => Estabelecimento::Rua(),
            "numero" => Estabelecimento::Numero(),
            "bairro" => Estabelecimento::Bairro(),
            "cidade" => Estabelecimento::Cidade(),
            "estado" => Estabelecimento::Estado(),
            "cep" => Estabelecimento::Cep(),
            "complemento" => Estabelecimento::Complemento(),
            "nomeCartao" => $data['card']['own_input'],
            "cartao" => preg_replace('/\s+/', '', $data['card']['c_input']),
            "cvv" => $data['card']['cvv_input'],
            "expiracao" => $data['card']['val_input'],
            "nomePlano" => $data['key']
        ];

        return Router::call($jsonData, 'safe2pay/createSubscription');
    }

    public static function cancelSubscription()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo() . "",
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "telefone" => Estabelecimento::Telefone()
        ];

        return Router::call($jsonData, 'safe2pay/cancelSubscription');
    }

    public static function updateToken($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo() . "",
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "nomeCartao" => $data['card']['own_input'],
            "cartao" => preg_replace('/\s+/', '', $data['card']['c_input']),
            "expiracao" => $data['card']['val_input'],
            "cvv" => $data['card']['cvv_input'],
            "cpfCartao" => $data['card']['cpf_input']
        ];

        return Router::call($jsonData, 'safe2pay/atualizarToken');
    }

    public static function statusPlanSafe2Pay()
    {
        $name = urlencode(Estabelecimento::Nome() . Estabelecimento::Codigo());
        return Router::callSafe2Pay('https://services.safe2pay.com.br/Recurrence/V1/Subscriptions?CustomerName=' . $name . '&Status=Ativa');
    }

    public static function listPlanFirebase($name)
    {
        $jsonData = [
            "nome" => $name
        ];

        return Router::call($jsonData, 'planos/listPlan');
    }

    public static function updateOnboarding($onboarding)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "onboarding" => $onboarding
        ];

        return json_encode(Router::call($jsonData, 'web/estabelecimentos/updateOnboarding/'));
    }
}
