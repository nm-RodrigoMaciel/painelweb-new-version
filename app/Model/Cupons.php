<?php

namespace App\Model;

use App\Http\Router;

class Cupons
{
    public static function getCupons()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo()
        ];

        return Router::call($jsonData, 'web/cupons/listarNovo');
    }

    public static function removerCupon($key)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $key
        ];

        return Router::call($jsonData, 'web/cupons/removerNovo');
    }

    public static function statusCupon($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $data['key'],
            "disponibilidade" => json_decode($data['disponibilidade'],true)
        ];

        return Router::call($jsonData, 'web/cupons/statusNovo');
    }

    public static function cadastrarNovo($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo()
        ];

        $jsonData = array_merge($jsonData, $data);

        return Router::call($jsonData, 'web/cupons/cadastrarNovo');
    }

    public static function getCuponsKey($key)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $key
        ];

        return Router::call($jsonData, 'web/cupons/listarNovo');
    }
}
