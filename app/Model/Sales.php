<?php

namespace App\Model;

use App\Http\Router;

class Sales
{
    public static function getSales($option)
    {
        date_default_timezone_set('America/Sao_Paulo');
        $jsonData = [
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "cnpjEstabelecimento" => Estabelecimento::Cnpj(),
            "idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
            "status" => "all",
            "date" => date('d/m/Y', $option),
            "lastIndex" => 0,
            "itemsPerPage" => 0
        ];

        return Router::call($jsonData, 'paginadas/vendas/listagemfoods');
    }

    public static function getSalesByKey($key, $telefone, $tipo)
    {
        $tipoCompra = $tipo == 'delivery' || $tipo == 'withdraw' && $telefone != null && $telefone != '' ? 2 : ($tipo == 'mesa' ? 1 : 0);

        $jsonData = [
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "cnpjEstabelecimento" => Estabelecimento::Cnpj(),
            "idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
            "status" => "all",
            "key" => $key,
            "telefone" => $telefone,
            "tipoCompra" => $tipoCompra
        ];

        return Router::call($jsonData, 'paginadas/vendas/listagemfoods');
    }

    public static function aprovarVendaFoods($key, $telefone, $tipo)
    {
        $tipoCompra = $tipo == 'delivery' || $tipo == 'withdraw' && $telefone != null && $telefone != '' ? 2 : ($tipo == 'mesa' ? 1 : 0);

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $key,
            "telefone" => $telefone,
            "tipoCompra" => $tipoCompra
        ];

        return Router::call($jsonData, 'paginadas/vendas/aprovarVendaFoods');
    }

    public static function cancelSale($data)
    {
        $tipoCompra = $data['tipo'] == 'delivery' && $data['telefone'] != null && $data['telefone'] != '' ? 2 : ($data['tipo'] == 'mesa' ? 1 : 0);

        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => (string)Estabelecimento::Codigo(),
            "codigoTransacao" => $data['key'],
            "motivoCancelamento" => $data['reason'],
            "telefone" => $data['telefone'],
            "tipoCompra" => $tipoCompra
        ];

        return Router::call($jsonData, 'sauros/vendas/cancelar');
    }

    public static function atualizarStatusFoods($data)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "key" => $data["key"],
            "telefone" => $data["telefone"],
            "tipoCompra" => $data["tipoCompra"]
        ];

        return Router::call($jsonData, 'sauros/vendas/atualizarStatusFoods');
    }

    public static function listTitle()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo()
        ];

        return Router::call($jsonData, 'status/kanban/listar');
    }

    public static function updateTitle($pos, $title)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "position" => $pos,
            "statusAlterado" => $title
        ];

        return Router::call($jsonData, 'status/kanban/update');
    }

    public static function resetTitle($pos)
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "tipoEstabelecimento" => Estabelecimento::Tipo(),
            "position" => $pos
        ];

        return Router::call($jsonData, 'status/kanban/reset');
    }

    public static function getSalesObject()
    {
        $jsonData = [
            "passCode" => "jrZup68Dg65U5CbD",
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "opened" => true
        ];

        return Router::call($jsonData, 'https://cardapio.ai/vendasController/listar.php', '');
    }

    public static function updateSalesObject($key, $status)
    {
        $jsonData = [
            "passCode" => "jrZup68Dg65U5CbD",
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo(),
            "key" => $key,
            "status" => $status
        ];

        return Router::call($jsonData, 'https://cardapio.ai/vendasController/atualizar.php', '');
    }

    public static function getSalesAprovacao()
    {
        $jsonData = [
            "nomeEstabelecimento" => Estabelecimento::Nome(),
            "codigoEstabelecimento" => Estabelecimento::Codigo()
        ];

        return Router::call($jsonData, 'paginadas/vendas/vendasFoodsAprovacao');
    }
}
