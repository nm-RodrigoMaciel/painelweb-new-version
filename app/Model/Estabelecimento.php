<?php

namespace App\Model;

use App\Http\Router;
use App\Session\Admin\Login;

class Estabelecimento
{
    public static $keyWeb = "AIzaSyAijC-9v5vJSXrNKM5BdjGPtfnv4AgJoFo";

    public static function Nome()
    {
        return Login::getData('name');
    }

    public static function NomeReduzido()
    {
        return strtolower(preg_replace("/\s+/", "-", Login::getData('name')));
    }

    public static function Codigo()
    {
        return Login::getData('code');
    }

    public static function Cnpj()
    {
        return Login::getData('cnpj');
    }

    public static function Tipo()
    {
        return Login::getData('type');
    }

    public static function Email()
    {
        return Login::getData('email');
    }
    
    public static function Rua()
    {
        return Login::getData('rua');
    }

    public static function Numero()
    {
        return Login::getData('numero');
    }

    public static function Bairro()
    {
        return Login::getData('bairro');
    }

    public static function Cidade()
    {
        return Login::getData('cidade');
    }

    public static function Estado()
    {
        return Login::getData('estado');
    }

    public static function Cep()
    {
        return Login::getData('cep');
    }

    public static function Complemento()
    {
        return Login::getData('complemento');
    }

    public static function Telefone()
    {
        return Login::getData('whatsapp');
    }

    public static function verifyLogin($form)
    {
        $jsonData = [
            "email" => $form['email'],
            "password" => $form['senha'],
            "keyWeb" => $form['keyWeb'] ?? self::$keyWeb,
            "nomeEstabelecimento" => $form['nomeEstabelecimento'] ?? null,
            "codigoEstabelecimento" => $form['codigoEstabelecimento'] ?? null,
            "tipoEstabelecimento" => $form['tipoEstabelecimento'] ?? null
        ];

        return Router::call($jsonData, 'auth/loginCardapio', $form['api'] ?? null);
    }
}
