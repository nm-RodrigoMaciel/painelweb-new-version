<?php

namespace App\Session\Admin;

use App\Model\WhiteLabel;

class Login
{

    private static function init()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function login($jsonData, $path)
    {
        self::init();

        $path = self::removerAcentos($jsonData['items'][0]['nome']);
        $path =  preg_replace('/[^A-Za-z0-9\- ]/', '', $path);
        $path = preg_replace("/\s+/", "-", $path);
        $path = strtolower($path);

        $botaoPedirNaMesa = isset($jsonData['items'][0]['botaoPedirNaMesa']) && !$jsonData['items'][0]['botaoPedirNaMesa'] ? 'false' : 'true';
        $botaoPedirOnline = isset($jsonData['items'][0]['botaoPedirOnline']) && !$jsonData['items'][0]['botaoPedirOnline'] ? 'false' : 'true';

        $_SESSION['painel_user'] = [
            'avaliacaoTotal' => $jsonData['items'][0]['avaliacaoTotal'],    // Avaliação 
            'cnpj' => $jsonData['items'][0]['cnpj'],                        // CNPJ 
            'type' => $jsonData['items'][0]['tipoEstabelecimento'],         // Tipo Estabelecimento
            'name' => $jsonData['items'][0]['nome'],                        // Nome
            'email' => $jsonData['items'][0]['email'],                      // Email
            'endereco' => $jsonData['items'][0]['endereco'],                // Endereço
            'rua' => $jsonData['items'][0]['rua'],                          // Rua
            'numero' => $jsonData['items'][0]['numero'],                    // Número
            'bairro' => $jsonData['items'][0]['bairro'],                    // Bairro
            'cidade' => $jsonData['items'][0]['cidade'],                    // Cidade
            'estado' => $jsonData['items'][0]['estado'],                    // Estado
            'cep' => $jsonData['items'][0]['cep'],                          // Cep
            'complemento' => $jsonData['items'][0]['complemento'],                  // Complemento
            'code' => $jsonData['items'][0]['codigoEstabelecimento'],       // Codigo Estabelecimento
            'url' => $jsonData['items'][0]['url'],                          // Url
            'phone' => $jsonData['items'][0]['telefone'],                   // Telefone
            'whatsapp' => $jsonData['items'][0]['telefoneWhatsApp'],        // WhatsApp
            'colorPrimary' => $jsonData['items'][0]['colorPrimary'] ?? '',  // Color Primary
            'api' => $jsonData['items'][0]['urlRoute'] ?? '',               // Url Api
            'codigoAcesso' => $jsonData['items'][0]['codigoAcesso'] ?? '',  // Codigo Acesso
            'path' => $path,                                                 // Caminho de origem (kartodromo-registro)
            'plano' => $jsonData['items'][0]['plano'],
            'onboarding' => $jsonData['items'][0]['onboarding'],
            'dataCadastro' => $jsonData['items'][0]['dataCadastro'],
            'botaoPedirNaMesa' => $botaoPedirNaMesa,
            'botaoPedirOnline' => $botaoPedirOnline
        ];
    }

    public static function isLogged()
    {
        self::init();

        return isset($_SESSION['painel_user']['cnpj']);
    }

    public static function loggout()
    {
        self::init();

        $path = $_SESSION['painel_user']['path'] ?? 'admin';
        unset($_SESSION['painel_user']);

        return $path;
    }

    public static function getData($var = null)
    {
        self::init();

        if ($var == null)
            return $_SESSION['painel_user'];
        else
            return isset($_SESSION['painel_user'][$var]) ? $_SESSION['painel_user'][$var] : '';
    }

    public static function setData($var, $value)
    {
        self::init();

        $_SESSION['painel_user'][$var] = $value;
    }

    public static function setPlan($plan)
    {
        self::init();

        $_SESSION['painel_user']['plano']['planoAtual'] = $plan;
    }

    public static function updateOnboarding($step)
    {
        self::init();

        WhiteLabel::updateOnboarding($step);
        self::setData('onboarding', $step);
    }

    public static function removerAcentos($stringExemplo)
    {
        $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ü', 'Ú', 'Ç', 'ç');

        $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'C', 'c');

        return str_replace($comAcentos, $semAcentos, $stringExemplo);
    }
}
