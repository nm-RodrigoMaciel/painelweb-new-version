<?php

namespace App\Controller\Admin;

use App\Model\Estabelecimento;
use App\Session\Admin\Login as SessionAdminLogin;
use App\Utils\View;
use App\Model\WhiteLabel;

class Login extends Page
{
    public static function getLogin($error = false)
    {
        $error = $error ? " error" : "";

        $content = View::render('admin/login', [
            'error' => $error
        ]);

        return parent::getPage($content);
    }

    public static function setLogin($request, $form = [])
    {
        $postVars = $_POST;
        $path = explode("/", $request->getUri())[2];

        if ((isset($postVars['email'])) && (isset($postVars['senha']))) {

            $form['email'] = $postVars['email'];
            $form['senha'] = $postVars['senha'];

            $jsonData = Estabelecimento::verifyLogin($form);

            if (isset($jsonData['statusCode']) && $jsonData['statusCode'] == 200) {

                SessionAdminLogin::login($jsonData, $path);

                if (isset($jsonData['items'][0]['onboarding'])) {

                    if ($jsonData['items'][0]['onboarding'] == 1)
                        $request->getRouter()->redirect('/cadastrar');
                    else if ($jsonData['items'][0]['onboarding'] == 2)
                        $request->getRouter()->redirect('/account?personalization');
                    else if ($jsonData['items'][0]['onboarding'] == 3)
                        $request->getRouter()->redirect('/account?mylinks');
                    else if ($jsonData['items'][0]['onboarding'] == 4)
                        $request->getRouter()->redirect('/account?hours');
                    else
                        $request->getRouter()->redirect('/listar');
                        
                } else
                    $request->getRouter()->redirect('/listar');
            } else {
                if ($path == 'admin')
                    return self::getLogin(true);
                else
                    $request->getRouter()->redirect('/' . $path . '?error');
            }
        } else {
            if ($path == 'admin')
                return self::getLogin(true);
            else
                $request->getRouter()->redirect('/' . $path . '?error');
        }
    }

    public static function setLoggout($request)
    {
        $path = SessionAdminLogin::loggout();

        if ($path == 'admin')
            $request->getRouter()->redirect('/admin/login');
        else
            $request->getRouter()->redirect('/' . $path);
    }

    public static function getLoginWl($data = [])
    {
        $error = isset($_GET['error']) ? " error" : "";

        $data['error'] = $error;

        $content = View::render('admin/loginWl', $data);

        return parent::getPage($content);
    }

    public static function getCreate($request)
    {
        $cod_corp = '';
        $title = '';
        $new = '';
        $url = '/painelweb/img/logo-horizontal.svg';
        $urlRoute = '';
        $colorPrimary = '';
        if (isset($_POST['cod_corp']) && $_POST['cod_corp'] != '') {
            $cod_corp = $_POST['cod_corp'];
            $url = $_POST['url'];
            $urlRoute = $_POST['urlRoute'];
            $colorPrimary = $_POST['colorPrimary'];
            $title = '<img class="create-title center-abs-hor" src="/painelweb/img/create-title.svg">';
            $new = ' new';
        } else {
            $request->getRouter()->redirect('/admin/login');
        }

        $content = View::render('admin/create', [
            'cod_corp' => $cod_corp,
            'title' => $title,
            'new' => $new,
            'url' => $url,
            'urlRoute' => $urlRoute,
            'colorPrimary' => $colorPrimary,
            'responsavel' => $_POST['responsavel'] ?? '',
            'email' => $_POST['email'] ?? '',
            'codigoEstabelecimento' => $_POST['codigoEstabelecimento'] ?? '',
            'nome' => $_POST['nomeEstabelecimento'] ?? '',
            'tipoEstabelecimento' => $_POST['tipoEstabelecimento'] ?? ''
        ]);

        return parent::getPage($content);
    }

    public static function setCreate($request)
    {
        $response = json_decode(WhiteLabel::setAccount($_POST), true);

        if (isset($response['statusCode']) && $response['statusCode'] == 400)
            echo 'Tivemos problemas ao tentar cadastrar essa nova conta';
        else if (isset($response['item']))
            $request->getRouter()->redirect('/' . strtolower(preg_replace("/\s+/", "-", $response['item']['nomeEstabelecimento'])));
        else
            echo 'Tivemos problemas ao tentar cadastrar essa nova conta';
    }
}
