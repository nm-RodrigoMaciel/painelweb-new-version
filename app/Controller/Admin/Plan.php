<?php

namespace App\Controller\Admin;

use App\Session\Admin\Login;

class Plan
{
    public static function verify($var)
    {
        $plano = Login::getData('plano');
        if ($plano != '') {
            $funcionalidades = $plano['planoAtual']['idFuncionalidades'];

            if (!in_array((string)$var, $funcionalidades)) {
                header('HTTP/1.1 203 /account?plan');
                die('/account?plan');
            }
        } else {
            header('HTTP/1.1 203 /account?plan');
            die('/account?plan');
        }
    }
}
