<?php

namespace App\Controller;

use App\Controller\Page;
use PHPMailer\PHPMailer\PHPMailer;

class Feedback extends Page
{
    public static function feedback($request)
    {
        $enviarPara = 'lucas@nomercadosoft.com.br';
        $copiaPara = 'diretoria@nomercadosoft.com.br';
        $outraCopiaPara = 'rodrigorochamaciel@gmail.com';
        $postDescricao = trim(htmlspecialchars($_POST['descricao'], ENT_QUOTES));
        $postPagina = $_POST['pagina'];
        $postCaptura = $_POST['captura'];
        $postAssunto = 'Bug na pagina painelweb/' . $postPagina;

        $htmlSite = "		
            <table width='500' style='border:1px solid #666666;' cellspacing='0' cellpadding='0'>
                <tr style='font-weight:bold; font-family: Tahoma, Verdana, Arial, sans-serif; font-size:14px; color:#FFF; background-color:#fc2043; text-align:center'>
                    <td style='padding:5px;'>" . $postAssunto . "</td>
                </tr>
                <tr>
                    <td align='center' style='padding:10px;'>
                        <table width='480' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td bgcolor='#EEE' style='padding:8px'><span style='font-size: 14px; font-family:Tahoma, Verdana, Arial, sans-serif; color:#000;'>" . $postDescricao . "</span></td>
                            </tr>
                            <br/>
                        </table>
                    </td>
                </tr>					
            </table>";

        require 'vendor/autoload.php';
        $mail = new PHPMailer(true);
        $mail->SMTPDebug = false;
        $mail->isSMTP();
        $mail->Host       = 'smtp.gmail.com';
        $mail->SMTPAuth   = true;
        $mail->Username   = 'nomercado.noreply@gmail.com';
        $mail->Password   = 'mqcaxmbqbdblmeqd';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port       = 465;
        $mail->setFrom($enviarPara, "No Mercado");
        $mail->addAddress($enviarPara);
        $mail->addAddress($copiaPara);
        $mail->addCC($outraCopiaPara);
        $mail->isHTML(true);
        $mail->Subject = $postAssunto;
        $mail->Body    = $htmlSite;

        if (isset($postCaptura) && $postCaptura != null) {
            $resource = base64_decode(str_replace(" ", "+", substr($postCaptura, strpos($postCaptura, ","))));
            $mail->addStringAttachment($resource, "captura.png");
        }

        $mail->send();


        $request->getRouter()->redirect('/' . $postPagina);
    }
}
