<?php

namespace App\Controller;

use App\Controller\Foods\Sales\Sales;
use App\Controller\General\Account;

class Interval extends Page
{
    public static function interval()
    {
        Account::verifyPlan();
        $response['sales'] = Sales::getSalesFirebase();

        return json_encode($response);
    }
}
