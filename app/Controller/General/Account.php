<?php

namespace App\Controller\General;

use App\Controller\Page;
use App\Model\Cupons;
use App\Model\WhiteLabel;
use App\Session\Admin\Login;
use App\Utils\View;

class Account extends Page
{

    public static function getAccount()
    {
        error_reporting(0);
        $view = 'general/account';

        $cupons = self::getCoupons();
        $estabelecimento = WhiteLabel::getEstabelecimento()['item'];
        $segundaVerificacaoQr = $estabelecimento['segundaVerificacaoQr'] ?? false;
        $planosCardapio = WhiteLabel::planos();
        $plano = $estabelecimento['plano'];
        $planoAtual = Login::getData('plano')['planoAtual'];

        //Plano Selecionado
        $planSelected = View::render('utils/account/plan-basic');
        $dataRenovacao = '';
        $bandeiraCartao = '';
        $meioPagamento = '';
        $hasPayment = '';
        $planCancel = '';
        if ($planoAtual != '') {

            $planBenefits = '';
            foreach ($planoAtual['beneficios'] as $key => $value) {
                $planBenefits .= View::render('utils/account/plan-benefits', ['text' => $value]);
            }

            $valor = 'GRÁTIS';
            if ($planoAtual['valor'] > 0) {
                $valor = parent::price($planoAtual['valor']);
                $valor = "R$ " . explode(',', $valor)[0] . "<span>," . explode(',', $valor)[1] . "/mês";
            }

            $planSelected = View::render('utils/account/plan', [
                'selected' => ' selected',
                'key' =>  $planoAtual['planoKey'],
                'nome' => $planoAtual['nome'],
                'valor' => $valor,
                'descricao' => $planoAtual['descricao'],
                'valorAntigo' => isset($planoAtual['valorAntigo']) ? '<h5>R$ ' . parent::price($planoAtual['valorAntigo']) . '/mês</h5>' : '',
                'plan-benefits' => $planBenefits
            ]);

            //Verificar dias restantes
            $diasRestantes = "";
            if ($planoAtual['nome'] == "Plano Teste") {

                $currentTime = strtotime(date("m") . '/' . date("d") . '/' . date("Y"));
                $periodoGratis = Login::getData('periodoGratis');
                $periodoGratis = $periodoGratis == '' ? 30 : $periodoGratis;
                $data = parent::fixDate(Login::getData('dataCadastro'));
                $timeData = strtotime($data) + ($periodoGratis * 86400);

                if ($currentTime <= $timeData) {

                    $dias = (int)(($timeData - $currentTime) / 86400);
                    if ($dias <= 15) {
                        $diasRestantes = View::render('utils/account/dias-restantes', [
                            'dias' => $dias,
                            's' =>  $dias > 1 ? 's' : ''
                        ]);
                    }
                }
            } else if (isset($plano['dataAssinatura'])) {
                //Botão de cancelar plano
                $planCancel = '<button class="plan-cancel">Cancelar assinatura</button>';

                //Data Renovação info
                $dataRenovacao = 'Próxima mensalidade: ' . $estabelecimento['plano']['dataRenovacao'];
            }
        }

        if (isset($plano['digitosCartao'])) {
            $hasPayment = ' hasPayment';
            $meioPagamento = 'Crédito •••• ' . substr($plano['digitosCartao'], -4);
        }

        $bandeiraCartao = strtolower($plano['bandeiraCartao']);
        $planPaymentInfo = View::render('utils/account/plan-payment-info', [
            'bandeiraCartao' => $bandeiraCartao,
            'meioPagamento' => $meioPagamento
        ]);

        //Lista de planos
        $plans = '';
        if (sizeof($planosCardapio) > 0) {
            foreach ($planosCardapio as $key => $plano) {

                if (
                    $plano['planoKey'] == $planoAtual['planoKey']
                )
                    continue;

                $planBenefits = '';
                foreach ($plano['beneficios'] as $key => $value) {
                    $planBenefits .= View::render('utils/account/plan-benefits', ['text' => $value]);
                }

                $valor = 'GRÁTIS';
                if ($plano['valor'] > 0) {
                    $valor = parent::price($plano['valor']);
                    $valor = "R$ " . explode(',', $valor)[0] . "<span>," . explode(',', $valor)[1] . "/mês";
                }
                $plans .= View::render('utils/account/plan', [
                    'selected' => '',
                    'key' => $plano['planoKey'],
                    'nome' => $plano['nome'],
                    'valor' => $valor,
                    'descricao' => $plano['descricao'],
                    'valorAntigo' => isset($plano['valorAntigo']) ? '<h5>R$ ' . parent::price($plano['valorAntigo']) . '/mês</h5>' : '',
                    'plan-benefits' => $planBenefits
                ]);
            }
        }

        //Caso tenha o parametro plan, redirecionar para a listagem de planos
        $redirect_plan = '';
        if (isset($_GET['plan'])) {
            $type = $_GET['plan'];

            if ($type != "") {
                $redirect_plan = '<script>$( document ).ready(function() { $(".side-menu-item:nth-child(3)").click(); $(".next-bill-action button").click(); $(".plan[key=' . $type . '] .plan-button").click();});</script>';
            } else
                $redirect_plan = '<script>$( document ).ready(function() { $(".side-menu-item:nth-child(3)").click(); $(".next-bill-action button").click(); });</script>';
        } else if (isset($_GET['personalization']))
            $redirect_plan = '<script>$( document ).ready(function() { $(".tabs-main-information .tabs-categories-item:nth-child(2)").click(); });</script>';
        else if (isset($_GET['mylinks']))
            $redirect_plan = '<script>$( document ).ready(function() { $(".side-menu-item:nth-child(4)").click(); });</script>';
        else if (isset($_GET['hours']))
            $redirect_plan = '<script>$( document ).ready(function() { $(".side-menu-item:nth-child(5)").click(); });</script>';

        //Verificando se o estabelecimento está disponivel
        $disponivel = $estabelecimento['disponivel'] == "sim" ? 'checked' : '';

        //Pagamento dinheiro
        $pagamentoDinheiro = isset($estabelecimento['tiposPagamentos'][4]) ? 'checked' : '';

        //Paleta de cores
        $paletteColors = '';
        $colors = $estabelecimento['paletteColors'] ?? '#000000;#000000;#000000;#000000;#000000;#000000;#000000;#000000;#000000;#000000;';
        foreach (explode(";", $colors) as $value) {
            if ($value != null && $value != '')
                $paletteColors .= '<div class="color-area"><div class="color-circle" style="background-color:' . $value . '"></div></div>';
        }

        $data = [
            'disponivel' => $disponivel,
            'image' => Login::getData('url') == '' ? '' : Login::getData('url') . '?t=' . time(),
            'color' => Login::getData('colorPrimary') == '' ? '#F16B24' : Login::getData('colorPrimary'),
            'avaliacaoTotal' => number_format(Login::getData('avaliacaoTotal'), 1, ',', '.'),
            'phone' => Login::getData('whatsapp'),
            'address' => $estabelecimento['endereco'],
            'retirada' => ($estabelecimento['retirada'] ?? true) ? 'checked' : '',
            'name' => Login::getData('name'),
            'endereco' => Login::getData('endereco'),
            'codigoAcesso' => Login::getData('codigoAcesso'),
            'path' => Login::getData('path'),
            'coupons' => $cupons,
            'empty' => $cupons == '' ? 'empty' : '',
            'links' => self::getLinks()['links'],
            'buttons_preview' => self::getLinks()['preview'],
            'horarios' => self::getHorarios(),
            'horariosEspeciais' => self::getHorariosEspeciais(),
            'image' => Login::getData('url') == '' ? '/painelweb/img/logo-example.svg' : Login::getData('url') . '?t=' . time(),
            'secondverification' => $segundaVerificacaoQr ? 'checked' : '',
            'valorMinimo' => parent::price(($estabelecimento['valorMinimo'] ?? 0)),
            'botaoPedirOnline' => ($estabelecimento['botaoPedirOnline'] ?? true) ? 'checked' : '',
            'botaoPedirNaMesa' => ($estabelecimento['botaoPedirNaMesa'] ?? true) ? 'checked' : '',
            'planSelected' => $planSelected,
            'plans' => $plans,
            'dataRenovacao' => $dataRenovacao,
            'planPaymentInfo' => $planPaymentInfo,
            'pagamentoDinheiro' => $pagamentoDinheiro,
            'hasPayment' => $hasPayment,
            'paletteColors' => $paletteColors,
            'etiquetasQr' => $estabelecimento['etiquetasQr'] ?? '',
            'diasRestantes' => $diasRestantes,
            'planCancel' => $planCancel
        ];

        $data = array_merge(self::listConfigDelivery(), $data);
        $data = array_merge(self::getCartoes($estabelecimento), $data);
        $content = View::render($view, $data);
        return parent::getPage($view, $content, [
            'title' => 'Meu Perfil',
            'extraFooter'  => '<script src="js/color-thief.umd.min.js"></script>
                               <script src="js/jquery.mask.min.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/jquery.qrcode.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/qrcode.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/es6-promise.auto.min.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/jspdf.min.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/html2canvas.min.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/html2pdf.bundle.min.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/html2pdf.min.js"></script>
                               <script type="text/javascript" src="/painelweb/js/qrcode/jsmd5.min.js"></script>',
            'after' => $redirect_plan
        ]);
    }

    public static function customizeAccount()
    {
        $response = json_decode(WhiteLabel::customizeAccount($_POST), true);

        if (isset($response["statusCode"]) && $response["statusCode"] == 200) {

            Login::setData('colorPrimary', $_POST['colorPrimary']);
            Login::setData('whatsapp', $_POST['telefoneWhatsApp']);

            if ($_POST['image'] != null)
                return 201;
            else
                return 200;
        }
        return 400;
    }

    public static function getCoupons()
    {
        $Cupons = Cupons::getCupons()['items'];

        $ListaCupons = '';
        if ($Cupons != null &&  sizeof($Cupons)) {
            foreach ($Cupons as $key => $value) {
                $benefits = $value['valorReal'] > 0 ? '-R$ ' . parent::price($value['valorReal']) : '';
                $benefits .= $value['valorPorcentagem'] > 0 ? '-' . $value['valorPorcentagem'] . '%' : '';
                $benefits .= $value['freteGratis'] ? ($benefits != '' ? ' |' : '') . ' Frete grátis' : '';

                $ListaCupons .= View::render('utils/coupon/main', [
                    'code' => $value['nomeCupom'],
                    'benefits' => $benefits,
                    'checked' => $value['disponibilidade'] ? 'checked' : ''
                ]);
            }
        }

        return $ListaCupons;
    }

    public static function removeCoupon()
    {
        $response = Cupons::removerCupon($_POST['key']);

        parent::status($response);
    }

    public static function createCoupon()
    {
        $data = json_decode($_POST['data'], true);
        $response = Cupons::cadastrarNovo($data);

        $return['key'] = '';
        $return['view'] = '';
        if ($response["statusCode"] == 200) {
            $return['key'] = $response['item']['nomeCupom'];

            $benefits = $data['valorReal'] > 0 ? '-R$ ' . parent::price($data['valorReal']) : '';
            $benefits .= $data['valorPorcentagem'] > 0 ? '-' . $data['valorPorcentagem'] . '%' : '';
            $benefits .= $data['freteGratis'] ? ($benefits != '' ? ' |' : '') . ' Frete grátis' : '';

            $return['view'] = View::render('utils/coupon/main', [
                'code' => $response['item']['nomeCupom'],
                'benefits' => $benefits,
                'checked' => $response['item']['disponibilidade'] ? 'checked' : ''
            ]);

            return json_encode($return);
        } else
            parent::status($response);
    }

    public static function statusCoupon()
    {
        $response = Cupons::statusCupon($_POST);

        parent::status($response);
    }

    public static function getByKeyCoupon()
    {
        $response = Cupons::getCuponsKey($_POST['key']);

        if ($response["statusCode"] == 200) {
            $item = $response['item'];

            $coupon_list = '';
            $usado = 0;
            $movimentado = 0;
            if (isset($item['uso'])) {
                foreach ($item['uso'] as $tel => $valueS) {
                    foreach ($valueS as $key => $value) {
                        $usado++;

                        $valorCompra = $value['valorCompra'] ?? $value['pagamento']['valorCompra'];
                        $valorFrete = $value['valorFrete'] ?? $value['pagamento']['valorFrete'];

                        $movimentado += ($valorCompra + $valorFrete);

                        $hora = $value['hora'] ?? $value['pedido']['hora'];
                        $data = $value['data'] ?? $value['pedido']['data'];
                        $nomeUsuario = $value['data'] ?? $value['cliente']['nomeUsuario'];

                        $coupon_list .= View::render('utils/coupon/stats-iteration', [
                            'key' => $key,
                            'nome' => $nomeUsuario,
                            'value' => parent::price($valorCompra + $valorFrete),
                            'data' => $hora . ' - ' . $data
                        ]);
                    }
                }
            }

            $item['uso'] = $coupon_list;
            $item['usado'] = $usado;
            $item['movimentado'] = parent::price($movimentado);

            return json_encode($item);
        } else
            parent::status($response);
    }

    public static function getLinks()
    {
        $linkTree = WhiteLabel::getLink()['items'];

        $links['links'] = '';
        $links['preview'] = '';
        if ($linkTree != null && sizeof($linkTree) > 0) {
            foreach ($linkTree as $key => $value) {
                $links['links'] .= View::render('utils/account/link', [
                    'key' => $key,
                    'title' => $value['title'],
                    'link' => $value['link']
                ]);

                $links['preview'] .= View::render('utils/account/button-preview', [
                    'title' => $value['title'],
                    'link' => $value['link']
                ]);
            }
        }

        $links['links'] .= View::render('utils/account/new-link', []);

        return $links;
    }

    public static function newLink()
    {
        $response = WhiteLabel::newLink($_POST);

        if (Login::getData('onboarding') == 3) {
            Login::updateOnboarding(4);
            return 201;
        }

        parent::status($response);
    }

    public static function removeLink()
    {
        $response = WhiteLabel::removeLink($_POST);

        parent::status($response);
    }

    public static function refreshPreview()
    {
        $baseInfo = WhiteLabel::getBasicInfo();
        $linkTree = $baseInfo['item']['linkTree'] ?? [];

        $preview = [];
        $preview['links'] = '';
        $preview['preview'] = '';
        if ($linkTree != null && sizeof($linkTree) > 0) {
            foreach ($linkTree as $key => $value) {
                $preview['links'] .= View::render('utils/account/link', [
                    'key' => $key,
                    'title' => $value['title'],
                    'link' => $value['link']
                ]);

                $preview['preview'] .= View::render('utils/account/button-preview', [
                    'title' => $value['title'],
                    'link' => $value['link']
                ]);
            }
        }

        return json_encode($preview);
    }

    public static function getHorarios()
    {
        $horarios = WhiteLabel::getHours()['items'];

        $horariosView = '';
        if ($horarios != null && sizeof($horarios) > 0) {
            foreach ($horarios as $key => $value) {
                $data = [
                    'key' => $key,
                    'titulo' => $value['titulo'],
                    'abre' => $value['abre'],
                    'fecha' => $value['fecha']
                ];

                $dias = explode(';', $value['dias']);
                $dias = array_fill_keys($dias, 'selected');

                $data = array_merge($data, $dias);

                $horariosView .= View::render('utils/delivery/delivery-item', $data);
            }
        }

        return $horariosView;
    }

    public static function newHours()
    {
        $data = $_POST;

        if ($data['name'] == '') {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Insira o título do período.']);
            return;
        }
        if (!isset($data['dias'])) {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Selecione pelomenos um dia.']);
            return;
        }
        if ($data['abre'] == '' || $data['fecha'] == '') {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Digite um período válido.']);
            return;
        }

        $days = [
            "0" => "segunda",
            "1" => "terça",
            "2" => "quarta",
            "3" => "quinta",
            "4" => "sexta",
            "5" => "sabado",
            "6" => "domingo"
        ];

        $array_days = '';
        foreach ($data['dias'] as $value) {
            if (array_key_exists($value, $days))
                $array_days .= $days[$value] . ';';
        }

        if ($array_days != '')
            $array_days = substr($array_days, 0, -1);

        $response = WhiteLabel::hours($data, $array_days);

        if (Login::getData('onboarding') == 4) {
            Login::updateOnboarding(null);
            return 201;
        }

        parent::status($response);
    }

    public static function removerHorarios()
    {
        $response = WhiteLabel::removeHours($_POST);

        parent::status($response);
    }

    public static function newSpecialHours()
    {
        $data = $_POST;

        if ($data['abre'] == '' && $data['fecha'] != '' || $data['fecha'] == '' && $data['abre'] != '') {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Digite um período válido.']);
            return;
        }

        $response = WhiteLabel::newSpecialHours($data);

        if ($response["statusCode"] == 200) {

            $days = [
                "1" => "Seg",
                "2" => "Ter",
                "3" => "Qua",
                "4" => "Qui",
                "5" => "Sex",
                "6" => "Sab",
                "0" => "Dom"
            ];

            $months = [
                "01" => "Janeiro",
                "02" => "Fevereiro",
                "03" => "Março",
                "04" => "Abril",
                "05" => "Maio",
                "06" => "Junho",
                "07" => "Julho",
                "08" => "Agosto",
                "09" => "Setembro",
                "10" => "Outubro",
                "11" => "Novembro",
                "12" => "Dezembro"
            ];

            $periodo = '';
            if ($data['abre'] == '' && $data['fecha'] == '') {
                $periodo = ' - Fechado';
            } else {
                $periodo = ' - ' . $data['abre'] . ' até ' . $data['fecha'];
            }

            return View::render(
                'utils/delivery/delivery-special-item',
                [
                    'key' => $response["item"],
                    'semana' => $days[$data['semana']],
                    'dia' => $data['dia'],
                    'mes' => $months[$data['mes']],
                    'ano' => $data['ano'],
                    'abreFecha' => $periodo
                ]
            );
        } else {
            parent::status($response);
        }
    }

    public static function getHorariosEspeciais()
    {
        $horarios = WhiteLabel::getSpecialHours()['items'];

        $days = [
            "1" => "Seg",
            "2" => "Ter",
            "3" => "Qua",
            "4" => "Qui",
            "5" => "Sex",
            "6" => "Sab",
            "0" => "Dom"
        ];

        $months = [
            "01" => "Janeiro",
            "02" => "Fevereiro",
            "03" => "Março",
            "04" => "Abril",
            "05" => "Maio",
            "06" => "Junho",
            "07" => "Julho",
            "08" => "Agosto",
            "09" => "Setembro",
            "10" => "Outubro",
            "11" => "Novembro",
            "12" => "Dezembro"
        ];

        $horariosView = '';
        if ($horarios != null && sizeof($horarios) > 0) {
            foreach ($horarios as $key => $value) {

                $periodo = '';
                if ($value['abre'] == '' && $value['fecha'] == '') {
                    $periodo = ' - Fechado';
                } else {
                    $periodo = ' - ' . $value['abre'] . ' até ' . $value['fecha'];
                }

                $horariosView .= View::render(
                    'utils/delivery/delivery-special-item',
                    [
                        'key' => $key,
                        'semana' => $days[$value['diadasemana']],
                        'dia' => $value['dia'],
                        'mes' => $months[$value['mes']],
                        'ano' => $value['ano'],
                        'abreFecha' => $periodo
                    ]
                );
            }
        }

        return $horariosView;
    }

    public static function statusSpecialHour()
    {
        $response = WhiteLabel::disponibilidadeSpecialHours($_POST);

        parent::status($response);
    }

    public static function removeSpecialHour()
    {
        $response = WhiteLabel::removeSpecialHours($_POST);

        parent::status($response);
    }

    public static function enableStore()
    {
        $response = WhiteLabel::enableStore($_POST);

        parent::status($response);
    }

    public static function configDelivery()
    {
        $postvars = $_POST['data'];
        $itens = [];

        $itens["entrega"] = $postvars[2];
        $itens["freteGAP"] = $postvars[5] !== null ? parent::priceFix($postvars[5]) : 0;
        $itens["freteGratisKm"] = parent::priceFix($postvars[6]);
        $itens["freteKm"] = parent::priceFix($postvars[7]);
        $itens["freteInicial"] = parent::priceFix($postvars[8]);
        $itens["rangeDistancia"] = parent::priceFix($postvars[9]);
        $itens["freteExcedente"] = parent::priceFix($postvars[10]);
        $itens["retirada"] = json_decode($postvars[10], true);

        $response = WhiteLabel::configDelivery($itens);

        parent::status($response);
    }

    public static function listConfigDelivery()
    {
        $item = WhiteLabel::listConfigDelivery()['item'];

        $item['entrega'] = $item['entrega'] ?? '0';
        $item['freteGAP'] = parent::price($item['freteGAP'] ?? '0');
        $item['freteGratisKm'] = $item['freteGratisKm'] ?? '0';
        $item['freteKm'] = $item['freteKm'] ?? '0';
        $item['freteInicial'] = parent::price($item['freteInicial']  ?? '0');
        $item['rangeDistancia'] = $item['rangeDistancia'] ?? '0';
        $item['freteExcedente'] = parent::price($item['freteExcedente']  ?? '0');
        $item['freteGAPVisibility'] = $item['freteGAP'] !== null && $item['freteGAP'] !== '' && $item['freteGAP'] > 0
            ? ' clicked'
            : '';

        return $item;
    }

    public static function etiquetasQr()
    {
        $response = WhiteLabel::etiquetasQr(json_decode($_POST['qtd'], true));

        parent::status($response);
    }

    public static function tipoVerificacaoQr()
    {
        $response = WhiteLabel::tipoVerificacaoQr(json_decode($_POST['switch']));

        parent::status($response);
    }

    public static function valorMinimo()
    {
        $response = WhiteLabel::valorMinimo(parent::priceFix($_POST['valorMinimo']));

        parent::status($response);
    }

    public static function habilitarDinheiro()
    {
        $response = WhiteLabel::habilitarDinheiro($_POST);

        parent::status($response);
    }

    public static function novosCartoes()
    {
        $data = $_POST;

        if ($data['nome'] == '') {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Insira o nome do novo cartão.']);
            return;
        }

        if (!isset($data['tipo'])) {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Selecione pelomenos um tipo para o cartão.']);
            return;
        }

        $tipoString = [
            "0" => "Crédito",
            "1" => "Débito",
            "2" => "VR/VA"
        ];

        $cartao = [];
        foreach ($data['tipo'] as $value) {
            $cartao[$value][] = $data['nome'];
        }

        $response = WhiteLabel::novosCartoes($cartao);

        if ($response["statusCode"] == 200) {
            $newCards = '';
            foreach ($data['tipo'] as $value) {
                $newCards .= View::render('utils/account/payment-card-area-item', [
                    "nome" => $data['nome'],
                    "tipo" => $tipoString[$value],
                    "tipoId" => $value
                ]);
            }

            return $newCards;
        } else {
            parent::status(['statusCode' => '400', 'statusMessage' => 'Infelizmente não foi possível cadastrar os novos cartões.']);
            return;
        }
    }

    public static function atualizarCartoes()
    {
        error_reporting(0);

        /*
            "codigoPagamento": 0
            "tipoPagamento": "Cartão de crédito"
        
            "codigoPagamento": 1
            "tipoPagamento": "Cartão de débito"

            "codigoPagamento": 2
            "tipoPagamento": "Vale Alimentação"
        
            "codigoPagamento": 3
            "tipoPagamento": "Vale Refeição"
        */

        $cards = $_POST['cards'];

        //Id's das cartoes
        $flags = [
            "Mastercard",
            "Visa",
            "Elo",
            "Alelo",
            "Sodexo",
            "American Express",
            "Hiper",
            "Hipercard",
            "Maestro",
            "Cirrus",
            "Diners Club",
            "Banescard",
            "Cabal"
        ];

        $cartoes[0] = [];
        $cartoes[1] = [];
        $cartoes[2] = [];
        $cartoes[3] = [];
        foreach ($cards as $idCard => $typeCard) {
            $flag = $flags[$idCard] ?? $idCard;
            foreach ($typeCard as $type) {

                if ($type == 2) {
                    $cartoes[2][] = $flag;
                    $cartoes[3][] = $flag;
                } else if ($type <= 1 && $type >= 0) {
                    $cartoes[$type][] = $flag;
                }
            }
        }

        $response = WhiteLabel::atualizarTipoPagamento($cartoes);

        parent::status($response);
    }

    public static function getCartoes($estabelecimento)
    {
        $tipoString = [
            "0" => "Crédito",
            "1" => "Débito",
            "2" => "VR/VA"
        ];

        $cartoes['cartoesNovos'] = '';
        if (isset($estabelecimento['tiposPagamentos'])) {
            foreach ($estabelecimento['tiposPagamentos'] as $key => $value) {
                if ($key < 3) {
                    $paymentType = $key;
                    if (isset($value['cartoes'])) {
                        foreach ($value['cartoes'] as $key => $value) {
                            $cartoes[$paymentType . '_' . $value] = 'checked';
                        }
                    }
                }
            }
        }

        if (isset($estabelecimento['cartoesAdicionados'])) {
            foreach ($estabelecimento['cartoesAdicionados'] as $key => $value) {
                $type = $key;
                foreach ($value as $key => $value) {
                    $cartoes['cartoesNovos'] .= View::render('utils/account/payment-card-area-item', [
                        "nome" => $value,
                        "tipo" => $tipoString[$type],
                        "tipoId" => $type,
                        "checked" => $cartoes[$type . "_" . $value] ?? ''
                    ]);
                }
            }
        }

        return $cartoes;
    }

    public static function setRetirada()
    {
        $button = $_POST['button'];
        $enable = json_decode($_POST['enable'], true);

        WhiteLabel::desabilitarBotao($button, $enable);
    }

    public static function disableButton()
    {
        $button = $_POST['button'];
        $enable = json_decode($_POST['enable'], true);

        WhiteLabel::desabilitarBotao($button, $enable);

        if (Login::getData('onboarding') == 3) {
            Login::updateOnboarding(4);
            return 201;
        }
    }

    public static function planChange()
    {
        $data = $_POST;
        $error = '';

        if (strlen($data['card']['val_input']) > 0 && strlen($data['card']['val_input']) < 7)
            $error = 'Preencha a Validade corretamente (00/0000)';

        if ($error != '')
            parent::status([
                "statusCode" => 400,
                "statusMessage" => $error
            ]);

        $response = WhiteLabel::createSubscription($data);

        if ($response["statusCode"] == 200 && $response["statusMessage"] == "O plano foi assinado com sucesso!") {
            $plano = WhiteLabel::listPlanFirebase($data['key']);
            Login::setPlan($plano);

            return json_encode([
                'append' => View::render('utils/account/payment-success', [
                    'value' => 'R$ ' . parent::price($response["item"]["valorPlanoAtual"]),
                    'card-flag' => strtolower($response["item"]["bandeiraCartao"]),
                    'card-info' => 'Crédito •••• ' . $response["item"]["digitosCartao"],
                    'next-payment' => $response["item"]["dataRenovacao"]
                ]),
                'days' => 31,
                'button' => '<button class="plan-cancel">Cancelar assinatura</button>'
            ]);
        } else {
            parent::status([
                "statusCode" => 400,
                "statusMessage" => $response["statusMessage"]
            ]);
        }
    }

    public static function planPayment()
    {
        $data = $_POST;
        $error = '';

        if (strlen($data['card']['c_input']) < 9)
            $error = 'Preencha o Número do cartão corretamente';
        else if (strlen($data['card']['own_input']) == 0)
            $error = 'Preencha um Titular do cartão válido';
        else if (strlen($data['card']['val_input']) < 7)
            $error = 'Preencha a Validade corretamente (00/0000)';
        else if (strlen($data['card']['cvv_input']) == 0)
            $error = 'Preencha o Cod. de Segurança';
        else if (strlen($data['card']['cpf_input']) < 11)
            $error = 'Preencha um CPF/CNPJ válido';

        if ($error != '')
            parent::status([
                "statusCode" => 400,
                "statusMessage" => $error
            ]);

        $response = WhiteLabel::updateToken($data);

        if ($response["statusCode"] == 200 && $response["statusMessage"] == "Token atualizado com sucesso!")
            return View::render('utils/account/plan-payment-info', [
                'bandeiraCartao' => $data['img'],
                'meioPagamento' => 'Crédito •••• ' . substr($data['card']['c_input'], -4)
            ]);
        else {
            parent::status([
                "statusCode" => 400,
                "statusMessage" => $response["statusMessage"]
            ]);
        }
    }

    public static function planCancel()
    {
        $response = WhiteLabel::cancelSubscription();

        if ($response["statusCode"] == 200)
            self::verifyPlan();

        parent::status([
            "statusCode" => $response["statusCode"],
            "statusMessage" => $response["statusMessage"]
        ]);
    }

    public static function verifyPlan()
    {
        $plano = Login::getData('plano');
        $plano = $plano['planoAtual'];
        $safe2pay = WhiteLabel::statusPlanSafe2Pay();

        //Caso tenha um plano pago
        if (isset($safe2pay['data']['objects'][0])) {
            if ($plano['planoKey'] != $safe2pay['data']['objects'][0]['planName'])
                $plano = WhiteLabel::listPlanFirebase($safe2pay['data']['objects'][0]['planName']);
        } else {
            //Caso não tenha plano pago, verificar se ainda está nos 30 dias de teste
            if (Login::getData('dataCadastro') != '') {

                $periodoGratis = Login::getData('periodoGratis');
                $periodoGratis = $periodoGratis == '' ? 30 : $periodoGratis;
                $data = parent::fixDate(Login::getData('dataCadastro'));
                $timeData = strtotime($data) + ($periodoGratis * 86400);
                $currentTime = strtotime(date("m") . '/' . date("d") . '/' . date("Y"));

                if ($currentTime <= $timeData)
                    $plano = WhiteLabel::listPlanFirebase("testeKey");
                else
                    $plano = WhiteLabel::listPlanFirebase("vitrineKey");
            } else if ($plano['planoKey'] != "vitrineKey")
                $plano = WhiteLabel::listPlanFirebase("vitrineKey");
        }

        Login::setPlan($plano);
    }
}
