<?php

namespace App\Controller\Foods\Product;

use App\Model\ProdutoFood;

class DeleteCategory
{
    public static function getDeleteCategoryFunction()
    {
        ProdutoFood::deleteCategory($_POST['category']);
    }
}
