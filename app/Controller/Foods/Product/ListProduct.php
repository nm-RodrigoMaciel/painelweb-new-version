<?php

namespace App\Controller\Foods\Product;

use App\Controller\Page;
use App\Model\ProdutoFood;
use \App\Utils\View;

class ListProduct extends Page
{
    public static function getListProduct()
    {
        $view = 'foods/listProduct';

        $category = '';
        $product = '';
        $productCopy = '';
        $qtdProducts = 0;
        $selected = 'selected';

        $productList = ProdutoFood::getProdutos();

        if (sizeof($productList) > 0) {
            foreach ($productList as $key => $values) {

                $keyFix = parent::keyFix($key);

                $categoryAvailability = '';
                foreach ($values as $a => $b) {
                    if ($b['disponivel']) {
                        $categoryAvailability = 'checked';
                    }
                }

                $category .=
                    "<div class='category $selected' ref='$keyFix'>
                        <span>$key</span>
                    </div>";
                $selected = '';

                //Cabeçalho de cada categoria dos produtos
                $product .=
                    '<div class="category-section-title" ref="' . $keyFix . '" cat="' . $key . '">
                    <div class="title" id="' . $keyFix . '">
                        <p class="title-text">' . $key . '</p>
                        <p class="title-disponibility">Disponível</p>
                        <label class="cl-switch cl-switch-green">
                            <input class="category-switch" type="checkbox" ' . $categoryAvailability . ' cat="' . $key . '" ref="' . $keyFix . '">
                            <span class="switcher"></span>
                        </label>
                    </div>
                    <div class="category-section-title-options d-flex">
                        <a href="cadastrar?category=' . $key . '">
                            <div class="new-product">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5303 5.46967C12.3897 5.32902 12.1989 5.25 12 5.25C11.8011 5.25 11.6103 5.32902 11.4697 5.46967C11.329 5.61032 11.25 5.80109 11.25 6V11.25H6C5.80109 11.25 5.61032 11.329 5.46967 11.4697C5.32902 11.6103 5.25 11.8011 5.25 12C5.25 12.1989 5.32902 12.3897 5.46967 12.5303C5.61032 12.671 5.80109 12.75 6 12.75H11.25V18C11.25 18.1989 11.329 18.3897 11.4697 18.5303C11.6103 18.671 11.8011 18.75 12 18.75C12.1989 18.75 12.3897 18.671 12.5303 18.5303C12.671 18.3897 12.75 18.1989 12.75 18V12.75H18C18.1989 12.75 18.3897 12.671 18.5303 12.5303C18.671 12.3897 18.75 12.1989 18.75 12C18.75 11.8011 18.671 11.6103 18.5303 11.4697C18.3897 11.329 18.1989 11.25 18 11.25H12.75V6C12.75 5.80109 12.671 5.61032 12.5303 5.46967Z" fill="var(--color-primary)" /> </svg>
                                <p>Novo produto</p>
                            </div>
                        </a>
                        <div class="duplicate-product" ref="' . $key . '">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M14.1661 0H5.35016C4.34069 0 3.5163 0.824395 3.5163 1.83386V3.5163H1.83386C0.824395 3.5163 0 4.34069 0 5.35016V14.1661C0 15.1756 0.824395 16 1.83386 16H10.6498C11.6593 16 12.4837 15.1756 12.4837 14.1661V12.4837H14.1661C15.1756 12.4837 16 11.6593 16 10.6498V1.83386C16 0.824395 15.1756 0 14.1661 0ZM11.4406 14.1493C11.4406 14.5868 11.0873 14.9401 10.6498 14.9401H1.85068C1.41325 14.9401 1.05994 14.5868 1.05994 14.1493V5.33333C1.05994 4.8959 1.41325 4.54259 1.85068 4.54259H10.6667C11.1041 4.54259 11.4574 4.8959 11.4574 5.33333V14.1493H11.4406ZM14.9569 10.6498C14.9569 11.0873 14.6036 11.4406 14.1661 11.4406H12.4837V5.33333C12.4837 4.32387 11.6593 3.49947 10.6498 3.49947H4.55941V1.81703C4.55941 1.3796 4.91272 1.02629 5.35016 1.02629H14.1661C14.6036 1.02629 14.9569 1.3796 14.9569 1.81703V10.6498Z" fill="var(--color-primary)" /> <path d="M9.01784 9.21965H6.7802V6.98201C6.7802 6.69599 6.54466 6.46045 6.25864 6.46045C5.97263 6.46045 5.73708 6.69599 5.73708 6.98201V9.21965H3.48262C3.1966 9.21965 2.96106 9.45519 2.96106 9.74121C2.96106 10.0272 3.1966 10.2628 3.48262 10.2628H5.72026V12.5004C5.72026 12.7864 5.9558 13.022 6.24182 13.022C6.52783 13.022 6.76337 12.7864 6.76337 12.5004V10.2628H9.01784C9.30386 10.2628 9.5394 10.0272 9.5394 9.74121C9.5394 9.45519 9.30386 9.21965 9.01784 9.21965Z" fill="var(--color-primary)" /> </svg>
                            <p>Copiar produto cadastrado</p>
                        </div>
                        <div class="input-group-append more-options">
                            <button data-toggle="dropdown">
                                <svg width="4" height="17" viewBox="0 0 4 17" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M2 4.5C3.1 4.5 4 3.6 4 2.5C4 1.4 3.1 0.5 2 0.5C0.9 0.5 0 1.4 0 2.5C0 3.6 0.9 4.5 2 4.5ZM2 6.5C0.9 6.5 0 7.4 0 8.5C0 9.6 0.9 10.5 2 10.5C3.1 10.5 4 9.6 4 8.5C4 7.4 3.1 6.5 2 6.5ZM2 12.5C0.9 12.5 0 13.4 0 14.5C0 15.6 0.9 16.5 2 16.5C3.1 16.5 4 15.6 4 14.5C4 13.4 3.1 12.5 2 12.5Z" fill="var(--color-primary)" /> </svg>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" ref="' . $key . '">Editar</a>
                                <a class="dropdown-item" ref="' . $key . '">Excluir</a>
                            </div>
                        </div>
                    </div>
                </div>';

                //Cabeçalho dos produtos combo.
                $productCopy .=
                    '<div id="' . $keyFix . '" class="product-header popup-item">
                        <p>' . $key . '</p>
                    </div>';

                foreach ($values as $value) {
                    $nameFix = parent::keyFix($value['nomeProduto']);

                    $productValue = $value['valorInicial'];
                    $productPromoValue = "0.00";

                    $productPromo = isset($value['promocao']) && $value['promocao'] != "0";
                    if ($productPromo) {
                        $productPromoValue = $productValue;
                        $productValue = ($productValue / ($value['promocao'] / 100));
                    }

                    //Adicionando os produtos
                    $product .=
                        "<div class='see-products-product' search='" . $nameFix . "' cod='" . $value['codigoBarras'] . "' cat='" . $key . "' ref='" . $keyFix . "'>
                            <a class='image-area' href='editar?cod=" . $value['codigoBarras'] . "'>
                                <img class='image' src='" . parent::https($value['url']) . "?t=" . time() . "' onerror='this.style.opacity=0'>
                                <svg class='not-wl image-empty' width='128' height='128' viewBox='0 0 128 128' fill='none' xmlns='http://www.w3.org/2000/svg'> <rect width='128' height='128' fill='#F5F5F5'></rect> <path opacity='0.5' d='M57.7617 56.1875C57.7617 57.4307 57.2679 58.623 56.3888 59.5021C55.5097 60.3811 54.3174 60.875 53.0742 60.875C51.831 60.875 50.6387 60.3811 49.7597 59.5021C48.8806 58.623 48.3867 57.4307 48.3867 56.1875C48.3867 54.9443 48.8806 53.752 49.7597 52.8729C50.6387 51.9939 51.831 51.5 53.0742 51.5C54.3174 51.5 55.5097 51.9939 56.3888 52.8729C57.2679 53.752 57.7617 54.9443 57.7617 56.1875V56.1875Z' fill='#BFBFBF'></path> <path opacity='0.5' fill-rule='evenodd' clip-rule='evenodd' d='M40.3729 46.6229C41.252 45.7439 42.4443 45.25 43.6875 45.25H84.3125C85.5557 45.25 86.748 45.7439 87.6271 46.6229C88.5061 47.502 89 48.6943 89 49.9375V67.0655C88.4681 66.9889 87.9242 66.9492 87.371 66.9492C86.8638 66.9492 86.3645 66.9826 85.875 67.0472V49.9375C85.875 49.5231 85.7104 49.1257 85.4174 48.8326C85.1243 48.5396 84.7269 48.375 84.3125 48.375H43.6875C43.2731 48.375 42.8757 48.5396 42.5826 48.8326C42.2896 49.1257 42.125 49.5231 42.125 49.9375V78.0625C42.1254 78.1042 42.1275 78.1459 42.1312 78.1875V76.5L50.4 69.1437C50.6554 68.8893 50.991 68.7309 51.3498 68.6956C51.7086 68.6603 52.0686 68.7502 52.3687 68.95L60.6813 74.4875L72.275 62.8937C72.5071 62.6623 72.8061 62.5097 73.1297 62.4576C73.4533 62.4054 73.7851 62.4563 74.0781 62.6031L83.6904 67.5608C79.2431 69.0884 76.0469 73.3076 76.0469 78.2733C76.0469 79.8634 76.3746 81.3769 76.9662 82.75H43.6875C42.4443 82.75 41.252 82.2561 40.3729 81.3771C39.4939 80.498 39 79.3057 39 78.0625V49.9375C39 48.6943 39.4939 47.502 40.3729 46.6229Z' fill='#BFBFBF'></path> <path opacity='0.5' d='M81.0977 74.2234L83.3172 72.0039L87.3699 76.0567L91.4196 72.0039L93.6391 74.2234L89.5894 78.2762L93.6391 82.3259L91.4196 84.5454L87.3699 80.4957L83.3172 84.5454L81.0977 82.3259L85.1504 78.2762L81.0977 74.2234Z' fill='#BFBFBF'></path></svg>
                            </a>
                            <div class='product-details'>
                                <div class='product-info'>
                                    <a href='editar?cod=" . $value['codigoBarras'] . "'><p class='title'>" . $value['nomeProduto'] . "</p></a>
                                    <p class='sub-title'>" . $value['descricao'] . "</p>
                                    <div class='measure-area'>" .
                        (isset($value['medida']) ? "<p class='measure'>" . $value['medida'] . "</p>" : '') .
                        (isset($value['servePessoas']) && $value['servePessoas'] > 0 ?
                            "<p class='serves'>
                                            <svg class='not-wl' width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'> <g opacity='0.8' clip-path='url(#clip0)'> <path d='M11.3584 10.8346C10.8688 10.1909 10.2656 9.27275 9.30092 9.27275C8.33503 9.27275 6.77618 9.27275 6.77618 9.27275C6.36577 9.27275 6.03393 9.60459 6.03393 10.0156C6.03393 10.4254 6.36577 10.7579 6.77618 10.7579H8.07512C8.23892 10.7579 8.3719 10.8908 8.3719 11.0546V11.1042C8.3719 11.268 8.23892 11.401 8.07512 11.401H5.8931L3.2106 9.02554C2.96519 8.80854 2.58984 8.83151 2.37224 9.07692C2.15464 9.32232 2.17761 9.69767 2.42301 9.91527C2.42301 9.91527 5.11821 13.2191 5.47724 13.2191L8.99145 13.2185L10.5382 14.0001L12.6066 12.4853C12.606 12.4853 12.4192 12.2297 11.3584 10.8346Z' fill='#4D4D4D'/> <path d='M12.4596 7.34888H1.3905V8.16064H12.4596V7.34888Z' fill='#4D4D4D'/> <path d='M11.8219 6.79027C11.837 6.62767 11.8467 6.46387 11.8467 6.29765C11.8467 3.92765 10.1718 1.95052 7.94141 1.48148C7.98795 1.36059 8.01636 1.23003 8.01636 1.09222C8.01636 0.4902 7.52616 0 6.92414 0C6.32212 0 5.83192 0.4902 5.83192 1.09222C5.83192 1.23003 5.86032 1.35999 5.90687 1.48148C3.67709 1.94992 2.00159 3.92765 2.00159 6.29765C2.00159 6.46387 2.01005 6.62828 2.02637 6.79027H11.8219ZM6.55241 1.09283C6.55241 0.887317 6.71923 0.720491 6.92474 0.720491C7.13025 0.720491 7.29708 0.887317 7.29708 1.09283C7.29708 1.21492 7.23421 1.31828 7.14234 1.38658C7.06981 1.38296 6.99848 1.3751 6.92474 1.3751C6.851 1.3751 6.77907 1.38296 6.70654 1.38658C6.61466 1.31828 6.55241 1.21492 6.55241 1.09283Z' fill='#4D4D4D'/> </g> <defs> <clipPath id='clip0'> <rect width='14' height='14' fill='white'/> </clipPath> </defs> </svg>
                                            Serve " . $value['servePessoas'] . " pessoa" . ($value['servePessoas'] > 1 ? "s" : "")
                            . "</p>" : "") .
                        "</div>
                                </div>
                                <div class='product-value'>
                                    <div class='value-input'>
                                        <span>R$</span>
                                        <input type='text' class='money-mask value-obj " . ($productPromo ? "disabled" : "") . " ' data-thousands='.' data-decimal=','  value='" . self::price($productValue) . "'/>
                                    </div>" .
                        ($productPromo ? "<p class='add-value added'>Novo preço com desconto:</p>" : "<p class='add-value'>Adicionar desconto</p>")
                        . "<div class='value-input-added' " . ($productPromo ? "style='display: flex;'" : "style='display: none;'") . ">
                                        <div class='value-input-promotion'><span>R$</span><input type='text' class='money-mask promotion-obj' data-thousands='.' data-decimal=','  value='" . self::price($productPromoValue) . "'/></div>
                                        <div class='trash-area'>
                                            <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'> <path d='M16.4094 9.22333V19.5956H8.11167V9.22333H16.4094ZM14.8536 3H9.6675L8.63028 4.03722H5V6.11167H19.5211V4.03722H15.8908L14.8536 3ZM18.4839 7.14889H6.03722V19.5956C6.03722 20.7365 6.97072 21.67 8.11167 21.67H16.4094C17.5504 21.67 18.4839 20.7365 18.4839 19.5956V7.14889Z' fill='var(--color-primary)' /> </svg>
                                        </div>
                                    </div>
                                    <p class='promotion-error'>Insira um valor válido.</p>
                                </div>
                            </div>
                            <div class='product-options'>
                                <div class='available-area'>
                                    <p class='title-disponibility'>" . ($value['disponivel'] ? 'Disponível' : 'Indisponível') . "</p>
                                    <label class='cl-switch cl-switch-green'>
                                        <input class='enabled-obj' type='checkbox' " . ($value['disponivel'] ? 'checked' : '') . " cat='" . $key . "'>
                                        <span class='switcher'></span>
                                    </label>
                                </div>
                                <a href='editar?cod=" . $value['codigoBarras'] . "'><p class='see-complete-item'>Editar</p></a>
                                <a href='duplicar?dup=" . $value['codigoBarras'] . "'><p class='duplicate-item'>Duplicar</p></a>
                            </div>
                        </div>";

                    $keyFix = parent::keyFix($value['categoria']);

                    //Lista de produtos a serem copiados
                    $productCopy .=
                        '<div class="copy-products-item product-' . $keyFix . ' popup-item" search="' . $nameFix . '">
                        <div class="info">
                            <img class="image" src="' . parent::https($value["url"]) . '?t=' . time() . '" onerror="this.style.opacity=0">
                            <svg class="not-wl image-placeholder" width="112" height="112" viewBox="0 0 112 112" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M112 0H0V112H112V0Z" fill="#F5F5F5"/>
                                <path opacity="0.5" d="M48.0603 46.0568C48.0603 47.6391 47.4319 49.1565 46.313 50.2754C45.1942 51.3941 43.6767 52.0227 42.0944 52.0227C40.5122 52.0227 38.9947 51.3941 37.876 50.2754C36.7571 49.1565 36.1285 47.6391 36.1285 46.0568C36.1285 44.4746 36.7571 42.9571 37.876 41.8382C38.9947 40.7195 40.5122 40.0909 42.0944 40.0909C43.6767 40.0909 45.1942 40.7195 46.313 41.8382C47.4319 42.9571 48.0603 44.4746 48.0603 46.0568Z" fill="#BFBFBF"/>
                                <path opacity="0.5" fill-rule="evenodd" clip-rule="evenodd" d="M25.9292 33.8837C27.048 32.765 28.5655 32.1364 30.1477 32.1364H81.8523C83.4345 32.1364 84.952 32.765 86.0709 33.8837C87.1896 35.0025 87.8182 36.52 87.8182 38.1023V59.9015C87.1412 59.8041 86.449 59.7535 85.7449 59.7535C85.0994 59.7535 84.4639 59.796 83.8409 59.8783V38.1023C83.8409 37.5749 83.6314 37.0691 83.2585 36.696C82.8855 36.3231 82.3797 36.1136 81.8523 36.1136H30.1477C29.6203 36.1136 29.1145 36.3231 28.7415 36.696C28.3686 37.0691 28.1591 37.5749 28.1591 38.1023V73.8977C28.1596 73.9508 28.1623 74.0039 28.167 74.0568V71.9091L38.6909 62.5465C39.016 62.2227 39.4431 62.0211 39.8998 61.9762C40.3564 61.9313 40.8146 62.0457 41.1965 62.3L51.7762 69.3477L66.5318 54.592C66.8272 54.2975 67.2078 54.1033 67.6196 54.0369C68.0315 53.9705 68.4538 54.0353 68.8267 54.2221L81.0605 60.5319C75.4003 62.4761 71.3324 67.846 71.3324 74.166C71.3324 76.1898 71.7495 78.1161 72.5025 79.8636H30.1477C28.5655 79.8636 27.048 79.235 25.9292 78.1163C24.8104 76.9975 24.1818 75.48 24.1818 73.8977V38.1023C24.1818 36.52 24.8104 35.0025 25.9292 33.8837Z" fill="#BFBFBF"/>
                                <path opacity="0.5" d="M77.7607 69.0116L80.5855 66.1868L85.7435 71.3449L90.8977 66.1868L93.7225 69.0116L88.5683 74.1697L93.7225 79.3239L90.8977 82.1487L85.7435 76.9945L80.5855 82.1487L77.7607 79.3239L82.9187 74.1697L77.7607 69.0116Z" fill="#BFBFBF"/>
                            </svg>
                            <div>
                                <p class="title">' . $value['nomeProduto'] . '</p>
                                <p class="description">' . $value['descricao'] . '</p>
                                <div class="d-flex value">' .
                        (isset($value['medida']) ? '<p class="measure">' . $value['medida'] . '</p>' : '') .
                        (isset($value['servePessoas']) && $value['servePessoas'] > 0 ?
                            '<p class="serves">
                                        <svg class="not-wl" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.8" clip-path="url(#clip0)"> <path d="M11.3584 10.8346C10.8688 10.1908 10.2656 9.27269 9.30092 9.27269C8.33503 9.27269 6.77618 9.27269 6.77618 9.27269C6.36577 9.27269 6.03393 9.60453 6.03393 10.0155C6.03393 10.4254 6.36577 10.7578 6.77618 10.7578H8.07512C8.23892 10.7578 8.3719 10.8908 8.3719 11.0546V11.1041C8.3719 11.2679 8.23892 11.4009 8.07512 11.4009H5.8931L3.2106 9.02548C2.96519 8.80848 2.58984 8.83145 2.37224 9.07685C2.15464 9.32226 2.17761 9.69761 2.42301 9.91521C2.42301 9.91521 5.11821 13.2191 5.47724 13.2191L8.99145 13.2185L10.5382 14L12.6066 12.4853C12.606 12.4853 12.4192 12.2296 11.3584 10.8346Z" fill="#4D4D4D" /> <path d="M12.4597 7.34875H1.39056V8.16052H12.4597V7.34875Z" fill="#4D4D4D" /> <path d="M11.822 6.79027C11.8371 6.62767 11.8468 6.46387 11.8468 6.29765C11.8468 3.92765 10.1719 1.95052 7.94153 1.48148C7.98807 1.36059 8.01648 1.23003 8.01648 1.09222C8.01648 0.4902 7.52628 0 6.92426 0C6.32224 0 5.83204 0.4902 5.83204 1.09222C5.83204 1.23003 5.86045 1.35999 5.90699 1.48148C3.67721 1.94992 2.00171 3.92765 2.00171 6.29765C2.00171 6.46387 2.01017 6.62828 2.02649 6.79027H11.822ZM6.55253 1.09283C6.55253 0.887317 6.71935 0.720491 6.92486 0.720491C7.13037 0.720491 7.2972 0.887317 7.2972 1.09283C7.2972 1.21492 7.23434 1.31828 7.14246 1.38658C7.06993 1.38296 6.99861 1.3751 6.92486 1.3751C6.85112 1.3751 6.77919 1.38296 6.70666 1.38658C6.61479 1.31828 6.55253 1.21492 6.55253 1.09283Z" fill="#4D4D4D" /> </g> <defs> <clipPath id="clip0"> <rect width="14" height="14" fill="white" /> </clipPath> </defs> </svg>
                                        Serve ' . $value['servePessoas'] . ' pessoa' . ($value['servePessoas'] > 1 ? "s" : "")
                            . '</p>' : '') .
                        '<p class="value">R$ ' . parent::price($value['valorInicial']) . '</p>
                                </div>
                            </div>
                        </div>
                        <label class="custom-checkbox">
                            <input type="checkbox" cat="' . $value['categoria'] . '" cod="' . $value['codigoBarras'] . '">
                            <span class="checkmark"></span>
                        </label>
                    </div>';

                    $qtdProducts++;
                }
            }
        }

        $content = View::render($view, [
            'products' => $product,
            'category' => $category,
            'productsCopy' => $productCopy,
            'qtdProducts' => $qtdProducts != 1 ? $qtdProducts . " Itens cadastrados" : $qtdProducts . " Item cadastrado",
            'empty' => $qtdProducts > 0
                ? ''
                : 'empty'
        ]);

        return parent::getPage($view, $content, ['title' => 'Produtos']);
    }
}
