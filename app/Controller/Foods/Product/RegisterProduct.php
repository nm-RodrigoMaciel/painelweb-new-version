<?php

namespace App\Controller\Foods\Product;

use App\Controller\Page;
use App\Model\ProdutoFood;
use App\Session\Admin\Login;
use \App\Utils\View;

class RegisterProduct extends Page
{

    public static function getRegisterProduct()
    {
        $view = 'foods/registerProduct';

        $categorySelected = '';
        if (isset($_GET['category']))
            $categorySelected = $_GET['category'];

        //Opção de adicionar nova categoria na listagem.
        $categoryItens =
            '<div class="category-dropdown-new-item">
                <div class="new-item-default">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5303 5.46967C12.3897 5.32902 12.1989 5.25 12 5.25C11.8011 5.25 11.6103 5.32902 11.4697 5.46967C11.329 5.61032 11.25 5.80109 11.25 6V11.25H6C5.80109 11.25 5.61032 11.329 5.46967 11.4697C5.32902 11.6103 5.25 11.8011 5.25 12C5.25 12.1989 5.32902 12.3897 5.46967 12.5303C5.61032 12.671 5.80109 12.75 6 12.75H11.25V18C11.25 18.1989 11.329 18.3897 11.4697 18.5303C11.6103 18.671 11.8011 18.75 12 18.75C12.1989 18.75 12.3897 18.671 12.5303 18.5303C12.671 18.3897 12.75 18.1989 12.75 18V12.75H18C18.1989 12.75 18.3897 12.671 18.5303 12.5303C18.671 12.3897 18.75 12.1989 18.75 12C18.75 11.8011 18.671 11.6103 18.5303 11.4697C18.3897 11.329 18.1989 11.25 18 11.25H12.75V6C12.75 5.80109 12.671 5.61032 12.5303 5.46967Z" fill="#F16B24"/>
                    </svg>                         
                    <p>Criar nova categoria</p>
                </div>
                <div class="new-item-insert">
                    <input type="text" placeholder="Insira o nome da nova categoria…">
                    <p class="new-item-insert-save">Salvar</p>
                </div>
            </div>';

        $productList = '';
        $productCategory = '';
        $selected = 'selected';
        foreach (ProdutoFood::getProdutos() as $key => $values) {

            $keyFix = parent::keyFix($key);

            //Itens da lista categorias do produto.
            $categoryItens .=
                '<div class="category-dropdown-item" search="' . $keyFix . '">
                <p>' . $key . '</p>
                <div class="area-custom-radio">
                    <input id="' . $keyFix . '" type="radio" class="custom-radio" name="category" ref="' . $key . '" ' . (parent::keyFix($categorySelected) == $keyFix ? 'checked' : '') . '>
                    <label for="' . $keyFix . '"></label>
                </div>
            </div>';

            //Lista horizontal de categorias na listagem de produtos combo.
            $productCategory .=
                "<div class='category $selected' ref='$keyFix'>
                    <span>$key</span>
                </div>";
            $selected = '';

            //Cabeçalho dos produtos combo.
            $productList .=
                '<div id="' . $keyFix . '" class="product-header popup-item">
                    <p>' . $key . '</p>
                </div>';

            foreach ($values as $key => $value) {

                $nameFix = parent::keyFix($value['nomeProduto']);

                //Item na lista de produtos combo.
                $productList .=
                    '<div class="combo-products-item popup-item product-' . $keyFix . '" search="' . $nameFix . '" cod="' . $value['codigoBarras'] . '">
                        <div class="d-flex align-items-center">
                        <div class="info">
                            <img class="image" src="' . parent::https($value["url"]) . '?t=' . time() . '" onerror="this.style.opacity=0">
                            <svg class="not-wl image-placeholder" width="112" height="112" viewBox="0 0 112 112" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M112 0H0V112H112V0Z" fill="#F5F5F5"/>
                                <path opacity="0.5" d="M48.0603 46.0568C48.0603 47.6391 47.4319 49.1565 46.313 50.2754C45.1942 51.3941 43.6767 52.0227 42.0944 52.0227C40.5122 52.0227 38.9947 51.3941 37.876 50.2754C36.7571 49.1565 36.1285 47.6391 36.1285 46.0568C36.1285 44.4746 36.7571 42.9571 37.876 41.8382C38.9947 40.7195 40.5122 40.0909 42.0944 40.0909C43.6767 40.0909 45.1942 40.7195 46.313 41.8382C47.4319 42.9571 48.0603 44.4746 48.0603 46.0568Z" fill="#BFBFBF"/>
                                <path opacity="0.5" fill-rule="evenodd" clip-rule="evenodd" d="M25.9292 33.8837C27.048 32.765 28.5655 32.1364 30.1477 32.1364H81.8523C83.4345 32.1364 84.952 32.765 86.0709 33.8837C87.1896 35.0025 87.8182 36.52 87.8182 38.1023V59.9015C87.1412 59.8041 86.449 59.7535 85.7449 59.7535C85.0994 59.7535 84.4639 59.796 83.8409 59.8783V38.1023C83.8409 37.5749 83.6314 37.0691 83.2585 36.696C82.8855 36.3231 82.3797 36.1136 81.8523 36.1136H30.1477C29.6203 36.1136 29.1145 36.3231 28.7415 36.696C28.3686 37.0691 28.1591 37.5749 28.1591 38.1023V73.8977C28.1596 73.9508 28.1623 74.0039 28.167 74.0568V71.9091L38.6909 62.5465C39.016 62.2227 39.4431 62.0211 39.8998 61.9762C40.3564 61.9313 40.8146 62.0457 41.1965 62.3L51.7762 69.3477L66.5318 54.592C66.8272 54.2975 67.2078 54.1033 67.6196 54.0369C68.0315 53.9705 68.4538 54.0353 68.8267 54.2221L81.0605 60.5319C75.4003 62.4761 71.3324 67.846 71.3324 74.166C71.3324 76.1898 71.7495 78.1161 72.5025 79.8636H30.1477C28.5655 79.8636 27.048 79.235 25.9292 78.1163C24.8104 76.9975 24.1818 75.48 24.1818 73.8977V38.1023C24.1818 36.52 24.8104 35.0025 25.9292 33.8837Z" fill="#BFBFBF"/>
                                <path opacity="0.5" d="M77.7607 69.0116L80.5855 66.1868L85.7435 71.3449L90.8977 66.1868L93.7225 69.0116L88.5683 74.1697L93.7225 79.3239L90.8977 82.1487L85.7435 76.9945L80.5855 82.1487L77.7607 79.3239L82.9187 74.1697L77.7607 69.0116Z" fill="#BFBFBF"/>
                            </svg>
                            <div>
                                <p class="title">' . $value['nomeProduto'] . '</p>
                                <p class="description">' . $value['descricao'] . '</p>
                                <div class="d-flex value">' .
                    (isset($value['medida']) ? '<p class="measure">' . $value['medida'] . '</p>' : '') .
                    (isset($value['servePessoas']) && $value['servePessoas'] != 0 ?
                        '<p class="serves"><svg class="not-wl" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.8" clip-path="url(#clip0)"> <path d="M11.3584 10.8346C10.8688 10.1908 10.2656 9.27269 9.30092 9.27269C8.33503 9.27269 6.77618 9.27269 6.77618 9.27269C6.36577 9.27269 6.03393 9.60453 6.03393 10.0155C6.03393 10.4254 6.36577 10.7578 6.77618 10.7578H8.07512C8.23892 10.7578 8.3719 10.8908 8.3719 11.0546V11.1041C8.3719 11.2679 8.23892 11.4009 8.07512 11.4009H5.8931L3.2106 9.02548C2.96519 8.80848 2.58984 8.83145 2.37224 9.07685C2.15464 9.32226 2.17761 9.69761 2.42301 9.91521C2.42301 9.91521 5.11821 13.2191 5.47724 13.2191L8.99145 13.2185L10.5382 14L12.6066 12.4853C12.606 12.4853 12.4192 12.2296 11.3584 10.8346Z" fill="#4D4D4D" /> <path d="M12.4597 7.34875H1.39056V8.16052H12.4597V7.34875Z" fill="#4D4D4D" /> <path d="M11.822 6.79027C11.8371 6.62767 11.8468 6.46387 11.8468 6.29765C11.8468 3.92765 10.1719 1.95052 7.94153 1.48148C7.98807 1.36059 8.01648 1.23003 8.01648 1.09222C8.01648 0.4902 7.52628 0 6.92426 0C6.32224 0 5.83204 0.4902 5.83204 1.09222C5.83204 1.23003 5.86045 1.35999 5.90699 1.48148C3.67721 1.94992 2.00171 3.92765 2.00171 6.29765C2.00171 6.46387 2.01017 6.62828 2.02649 6.79027H11.822ZM6.55253 1.09283C6.55253 0.887317 6.71935 0.720491 6.92486 0.720491C7.13037 0.720491 7.2972 0.887317 7.2972 1.09283C7.2972 1.21492 7.23434 1.31828 7.14246 1.38658C7.06993 1.38296 6.99861 1.3751 6.92486 1.3751C6.85112 1.3751 6.77919 1.38296 6.70666 1.38658C6.61479 1.31828 6.55253 1.21492 6.55253 1.09283Z" fill="#4D4D4D" /> </g> <defs> <clipPath id="clip0"> <rect width="14" height="14" fill="white" /> </clipPath> </defs> </svg>
                                                Serve ' . $value['servePessoas'] . ' pessoas</p>' : '') .
                    '<p class="price">R$ ' . str_replace('.', ',', $value['valorInicial']) . '</p>
                                        </div>
                                    </div>
                        </div>
                        <div class="input-quantity">
                            <input class="combo-products-item-quantity" type="number" value="1" min="1">
                            <svg class="ml-quantity" width="25" height="34" viewBox="0 0 25 34" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0 0H18C21.866 0 25 3.13401 25 7V17H0V0Z" fill="#FFFFFF"/> <path d="M5.76929 15.1111H15.3847L10.577 10.3889L5.76929 15.1111Z" fill="#F16B24"/> <path d="M0 17H25V27C25 30.866 21.866 34 18 34H0V17Z" fill="#FFFFFF"/> <path d="M5.76929 18.8889H15.3847L10.577 23.6111L5.76929 18.8889Z" fill="#F16B24"/> </svg>
                        </div>
                        <label class="custom-checkbox">
                            <input type="checkbox" cod="' . $value['codigoBarras'] . '" val="' . $value['nomeProduto'] . '">
                            <span class="checkmark"></span>
                        </label>
                        </div>
                        <div class="value-remove">
                            <svg class="svg-info" svgtitle="Desconto unitário para remoção do produto" svgbody="Caso o cliente remova uma ou mais unidades deste produto do combo, o valor inserido neste campo será aplicado como desconto para cada unidade removida.<br><br>Se não desejar dar desconto, deixe o campo vazio." width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"> <path d="M8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15ZM8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16Z" fill="#F16B24"/> <path d="M8.92995 6.588L6.63995 6.875L6.55795 7.255L7.00795 7.338C7.30195 7.408 7.35995 7.514 7.29595 7.807L6.55795 11.275C6.36395 12.172 6.66295 12.594 7.36595 12.594C7.91095 12.594 8.54395 12.342 8.83095 11.996L8.91895 11.58C8.71895 11.756 8.42695 11.826 8.23295 11.826C7.95795 11.826 7.85795 11.633 7.92895 11.293L8.92995 6.588ZM8.99995 4.5C8.99995 4.76522 8.8946 5.01957 8.70706 5.20711C8.51952 5.39464 8.26517 5.5 7.99995 5.5C7.73474 5.5 7.48038 5.39464 7.29285 5.20711C7.10531 5.01957 6.99995 4.76522 6.99995 4.5C6.99995 4.23478 7.10531 3.98043 7.29285 3.79289C7.48038 3.60536 7.73474 3.5 7.99995 3.5C8.26517 3.5 8.51952 3.60536 8.70706 3.79289C8.8946 3.98043 8.99995 4.23478 8.99995 4.5Z" fill="#F16B24"/> </g> <defs> <clipPath id="clip0"> <rect width="16" height="16" fill="white"/> </clipPath> </defs> </svg>
                            <p>Desconto unitário para remoção do produto:</p>
                            <div>
                                <span>R$</span>
                                <input class="money-mask" type="text" data-thousands="." data-decimal=","  />
                            </div>
                        </div>
                    </div>';
            }
        }

        $ingredient = '';
        $category = '';
        $adittional = '';
        $selected = 'selected';
        foreach (ProdutoFood::getIngredientes() as $key => $values) {
            $keyFix = parent::keyFix($key);

            //Lista horizontal de categorias dos ingredientes e adicionais.
            $category .=
                "<div class='category $selected' ref='$keyFix'>
                    <span>$key</span>
                </div>";
            $selected = '';

            //Cabeçalho dos ingredientes na listagem.
            $ingredient .=
                '<div id="' . $keyFix . '" class="ingredient-header popup-item popup-header" ref="' . $key . '">
                    <p>' . $key . '</p>
                </div>';

            //Cabeçalho dos adicionais na listagem de adicionais do produto.
            $adittional .=
                '<div id="' . $keyFix . '" class="header-adittional adittional-header-' . $keyFix . ' ' . (isset($values['obrigatorio']) ? 'selected' : '') . ' popup-item popup-header" ref="' . $key . '">
                    <div class="center-rel">
                        <div class="header-adittional-title">
                            <p>' . $key . '</p>
                            <div class="header-adittional-button" ref="' . $key . '">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M8.39589 2.19298C8.2724 2.06942 8.10473 2 7.93002 2C7.75532 2 7.58765 2.06942 7.46416 2.19298C7.34055 2.31655 7.27113 2.48414 7.27113 2.65889V7.27118H2.65889C2.48414 7.27118 2.31655 7.34056 2.19298 7.46416C2.06942 7.58765 2 7.75532 2 7.93002C2 8.10473 2.06942 8.2724 2.19298 8.39589C2.31655 8.51949 2.48414 8.58887 2.65889 8.58887H7.27113V13.2012C7.27113 13.3759 7.34055 13.5435 7.46416 13.667C7.58765 13.7906 7.75532 13.86 7.93002 13.86C8.10473 13.86 8.2724 13.7906 8.39589 13.667C8.51949 13.5435 8.58887 13.3759 8.58887 13.2012V8.58887H13.2012C13.3759 8.58887 13.5435 8.51949 13.667 8.39589C13.7906 8.2724 13.86 8.10473 13.86 7.93002C13.86 7.75532 13.7906 7.58765 13.667 7.46416C13.5435 7.34056 13.3759 7.27118 13.2012 7.27118H8.58887V2.65889C8.58887 2.48414 8.51949 2.31655 8.39589 2.19298Z" fill="var(--color-primary)"/> </svg>
                                <p>Selecionar categoria</p>
                                <p>Categoria selecionada</p>
                            </div>
                        </div>
                        <div class="header-adittional-options">
                            <div class="d-flex align-items-center choose-o">
                                <svg class="svg-info" svgtitle="Escolha obrigatória de adicionais" svgbody="Caso a escolha de um grupo de adicionais seja definida como obrigatória, o grupo será exibido para o cliente escolher apenas uma opção, e não haverá cobrança." width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"> <path d="M8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15ZM8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16Z" fill="var(--color-primary)"/> <path d="M8.92995 6.588L6.63995 6.875L6.55795 7.255L7.00795 7.338C7.30195 7.408 7.35995 7.514 7.29595 7.807L6.55795 11.275C6.36395 12.172 6.66295 12.594 7.36595 12.594C7.91095 12.594 8.54395 12.342 8.83095 11.996L8.91895 11.58C8.71895 11.756 8.42695 11.826 8.23295 11.826C7.95795 11.826 7.85795 11.633 7.92895 11.293L8.92995 6.588ZM8.99995 4.5C8.99995 4.76522 8.8946 5.01957 8.70706 5.20711C8.51952 5.39464 8.26517 5.5 7.99995 5.5C7.73474 5.5 7.48038 5.39464 7.29285 5.20711C7.10531 5.01957 6.99995 4.76522 6.99995 4.5C6.99995 4.23478 7.10531 3.98043 7.29285 3.79289C7.48038 3.60536 7.73474 3.5 7.99995 3.5C8.26517 3.5 8.51952 3.60536 8.70706 3.79289C8.8946 3.98043 8.99995 4.23478 8.99995 4.5Z" fill="var(--color-primary)"/> </g> <defs> <clipPath id="clip0"> <rect width="16" height="16" fill="white"/> </clipPath> </defs> </svg>
                                <p>Escolha obrigatória</p>
                                <label class="custom-checkbox">
                                    <input type="checkbox" ref="' . $key . '" ' . (isset($values['obrigatorio']) && $values['obrigatorio'] ? 'checked' : '') . '>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="header-adittional-right">
                                <p class="choice-limit">Limite de escolha</p>
                                <div class="input-quantity unlimited" in="adittional">
                                    <svg class="not-wl text" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.958807 10H6.23011V8.86648H2.27557V1.27273H0.958807V10ZM7.62997 10H8.90412V3.45455H7.62997V10ZM8.27344 2.4446C8.71236 2.4446 9.07884 2.10369 9.07884 1.68608C9.07884 1.26847 8.71236 0.923295 8.27344 0.923295C7.83026 0.923295 7.46804 1.26847 7.46804 1.68608C7.46804 2.10369 7.83026 2.4446 8.27344 2.4446ZM10.6183 10H11.8924V5.96449C11.8924 5.08239 12.5103 4.46875 13.2347 4.46875C13.9421 4.46875 14.4322 4.9375 14.4322 5.64915V10H15.7021V5.82812C15.7021 5.03977 16.1836 4.46875 17.0146 4.46875C17.6879 4.46875 18.2418 4.84375 18.2418 5.73011V10H19.516V5.6108C19.516 4.11506 18.6808 3.36932 17.4961 3.36932C16.5543 3.36932 15.8469 3.82102 15.5316 4.51989H15.4634C15.1779 3.80398 14.5771 3.36932 13.7035 3.36932C12.8384 3.36932 12.195 3.79972 11.9222 4.51989H11.8413V3.45455H10.6183V10ZM21.2237 10H22.4979V3.45455H21.2237V10ZM21.8672 2.4446C22.3061 2.4446 22.6726 2.10369 22.6726 1.68608C22.6726 1.26847 22.3061 0.923295 21.8672 0.923295C21.424 0.923295 21.0618 1.26847 21.0618 1.68608C21.0618 2.10369 21.424 2.4446 21.8672 2.4446ZM27.2674 3.45455H25.9251V1.88636H24.6509V3.45455H23.6921V4.47727H24.6509V8.34233C24.6467 9.53125 25.5543 10.1065 26.56 10.0852C26.9648 10.081 27.2376 10.0043 27.3867 9.94886L27.1566 8.89631C27.0714 8.91335 26.9137 8.9517 26.7092 8.9517C26.2958 8.9517 25.9251 8.81534 25.9251 8.07812V4.47727H27.2674V3.45455ZM31.4276 10.1321C32.8551 10.1321 33.8651 9.42898 34.1548 8.36364L32.9489 8.14631C32.7188 8.7642 32.1648 9.07955 31.4403 9.07955C30.3494 9.07955 29.6165 8.37216 29.5824 7.1108H34.2358V6.65909C34.2358 4.29403 32.821 3.36932 31.3381 3.36932C29.5142 3.36932 28.3125 4.75852 28.3125 6.76989C28.3125 8.80256 29.4972 10.1321 31.4276 10.1321ZM29.5866 6.15625C29.6378 5.22727 30.3111 4.42188 31.3466 4.42188C32.3352 4.42188 32.983 5.15483 32.9872 6.15625H29.5866ZM36.5018 5.31676C36.9663 5.31676 37.354 4.9375 37.354 4.46449C37.354 4 36.9663 3.61648 36.5018 3.61648C36.033 3.61648 35.6495 4 35.6495 4.46449C35.6495 4.9375 36.033 5.31676 36.5018 5.31676ZM36.5018 10.081C36.9663 10.081 37.354 9.7017 37.354 9.22869C37.354 8.7642 36.9663 8.38068 36.5018 8.38068C36.033 8.38068 35.6495 8.7642 35.6495 9.22869C35.6495 9.7017 36.033 10.081 36.5018 10.081Z" fill="#999999"/>
                                    </svg> 
                                    <input class="choice-limit" type="number">
                                    <svg class="ml-quantity" width="25" height="34" viewBox="0 0 25 34" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0 0H18C21.866 0 25 3.13401 25 7V17H0V0Z" fill="#FFFFFF"/> <path d="M5.76929 15.1111H15.3847L10.577 10.3889L5.76929 15.1111Z" fill="#F16B24"/> <path d="M0 17H25V27C25 30.866 21.866 34 18 34H0V17Z" fill="#FFFFFF"/> <path d="M5.76929 18.8889H15.3847L10.577 23.6111L5.76929 18.8889Z" fill="#F16B24"/> </svg>
                                </div>  
                            </div>  
                        </div>
                    </div>
                </div>';

            foreach ((isset($values['adicionais']) ? $values['adicionais'] : $values) as $value) {

                $nameFix = parent::keyFix($value['nomeProduto']);

                //Itens de ingredientes na listagem de ingredientes.
                $ingredient .=
                    '<div class="product-dropdown-item ingredient-' . $keyFix . ' popup-item" search="' . $nameFix . '" cod="' . $value['codigoBarras'] . '" ref="' . $key . '">
                    <p class="product-dropdown-item-name">' . $value['nomeProduto'] . '</p>
                    <div class="product-dropdown-item-more">
                        <div class="input-quantity">
                            <input class="product-dropdown-item-quantity" type="number" value="1" ref="' . $value['nomeProduto'] . '" min="1"> 
                            <svg class="ml-quantity" width="25" height="34" viewBox="0 0 25 34" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0 0H18C21.866 0 25 3.13401 25 7V17H0V0Z" fill="#FFFFFF"/> <path d="M5.76929 15.1111H15.3847L10.577 10.3889L5.76929 15.1111Z" fill="#F16B24"/> <path d="M0 17H25V27C25 30.866 21.866 34 18 34H0V17Z" fill="#FFFFFF"/> <path d="M5.76929 18.8889H15.3847L10.577 23.6111L5.76929 18.8889Z" fill="#F16B24"/> </svg>
                        </div>
                        <label class="custom-checkbox">
                            <input class="product-dropdown-item-check" type="checkbox" val="' . $value['nomeProduto'] . '" ref="' . $key . '" cod="' . $value['codigoBarras'] . '">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>';

                //Itens de adicionais na listagem de adicionais.
                $adittional .=
                    '<div class="product-dropdown-item adittional-' . $keyFix . ' popup-item" search="' . $nameFix . '" cod="' . $value['keyPush'] . '" ref="' . $key . '">
                    <p class="adittional-title">' . $value['nomeProduto'] . '</p>
                    <div class="product-dropdown-item-more">
                        <div class="input-quantity unlimited" in="adittional">
                            <svg class="not-wl text" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.958807 10H6.23011V8.86648H2.27557V1.27273H0.958807V10ZM7.62997 10H8.90412V3.45455H7.62997V10ZM8.27344 2.4446C8.71236 2.4446 9.07884 2.10369 9.07884 1.68608C9.07884 1.26847 8.71236 0.923295 8.27344 0.923295C7.83026 0.923295 7.46804 1.26847 7.46804 1.68608C7.46804 2.10369 7.83026 2.4446 8.27344 2.4446ZM10.6183 10H11.8924V5.96449C11.8924 5.08239 12.5103 4.46875 13.2347 4.46875C13.9421 4.46875 14.4322 4.9375 14.4322 5.64915V10H15.7021V5.82812C15.7021 5.03977 16.1836 4.46875 17.0146 4.46875C17.6879 4.46875 18.2418 4.84375 18.2418 5.73011V10H19.516V5.6108C19.516 4.11506 18.6808 3.36932 17.4961 3.36932C16.5543 3.36932 15.8469 3.82102 15.5316 4.51989H15.4634C15.1779 3.80398 14.5771 3.36932 13.7035 3.36932C12.8384 3.36932 12.195 3.79972 11.9222 4.51989H11.8413V3.45455H10.6183V10ZM21.2237 10H22.4979V3.45455H21.2237V10ZM21.8672 2.4446C22.3061 2.4446 22.6726 2.10369 22.6726 1.68608C22.6726 1.26847 22.3061 0.923295 21.8672 0.923295C21.424 0.923295 21.0618 1.26847 21.0618 1.68608C21.0618 2.10369 21.424 2.4446 21.8672 2.4446ZM27.2674 3.45455H25.9251V1.88636H24.6509V3.45455H23.6921V4.47727H24.6509V8.34233C24.6467 9.53125 25.5543 10.1065 26.56 10.0852C26.9648 10.081 27.2376 10.0043 27.3867 9.94886L27.1566 8.89631C27.0714 8.91335 26.9137 8.9517 26.7092 8.9517C26.2958 8.9517 25.9251 8.81534 25.9251 8.07812V4.47727H27.2674V3.45455ZM31.4276 10.1321C32.8551 10.1321 33.8651 9.42898 34.1548 8.36364L32.9489 8.14631C32.7188 8.7642 32.1648 9.07955 31.4403 9.07955C30.3494 9.07955 29.6165 8.37216 29.5824 7.1108H34.2358V6.65909C34.2358 4.29403 32.821 3.36932 31.3381 3.36932C29.5142 3.36932 28.3125 4.75852 28.3125 6.76989C28.3125 8.80256 29.4972 10.1321 31.4276 10.1321ZM29.5866 6.15625C29.6378 5.22727 30.3111 4.42188 31.3466 4.42188C32.3352 4.42188 32.983 5.15483 32.9872 6.15625H29.5866ZM36.5018 5.31676C36.9663 5.31676 37.354 4.9375 37.354 4.46449C37.354 4 36.9663 3.61648 36.5018 3.61648C36.033 3.61648 35.6495 4 35.6495 4.46449C35.6495 4.9375 36.033 5.31676 36.5018 5.31676ZM36.5018 10.081C36.9663 10.081 37.354 9.7017 37.354 9.22869C37.354 8.7642 36.9663 8.38068 36.5018 8.38068C36.033 8.38068 35.6495 8.7642 35.6495 9.22869C35.6495 9.7017 36.033 10.081 36.5018 10.081Z" fill="#999999"/>
                            </svg> 
                            <input class="product-dropdown-item-quantity" type="number" value="" ref="' . $value['nomeProduto'] . '">
                            <svg class="ml-quantity" width="25" height="34" viewBox="0 0 25 34" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0 0H18C21.866 0 25 3.13401 25 7V17H0V0Z" fill="#FFFFFF"/> <path d="M5.76929 15.1111H15.3847L10.577 10.3889L5.76929 15.1111Z" fill="#F16B24"/> <path d="M0 17H25V27C25 30.866 21.866 34 18 34H0V17Z" fill="#FFFFFF"/> <path d="M5.76929 18.8889H15.3847L10.577 23.6111L5.76929 18.8889Z" fill="#F16B24"/> </svg>
                        </div>
                        <p ref="' . $key . '">R$ ' . str_replace('.', ',', $value['valorInicial']) . '</p>
                        <label class="custom-checkbox">
                        <input class="product-dropdown-item-check" type="checkbox" val="' . $value['nomeProduto'] . '" ref="' . $key . '" price="' . str_replace('.', ',', $value['valorInicial']) . '" cod="' . $value['codigoBarras'] . '">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>';
            }
        }

        $data = [
            'categoryDropdown' => $categoryItens,
            'ingredientList' => $ingredient,
            'categoryIngredient' => $category,
            'adittional' => $adittional,
            'productCategory' => $productCategory,
            'productList' => $productList,
            'categorySelected' => $categorySelected,
            'logo' => Login::getData('url')
        ];

        $content = View::render($view, $data);

        return parent::getPage($view, $content, [
            'title' => 'Cadastrar Produto'
        ]);
    }

    public static function setProduct()
    {
        ProdutoFood::setProdutos(json_decode($_POST['product'], true), $_POST['base']);

        if (Login::getData('onboarding') == 1) {
            Login::updateOnboarding(3);
            return 201;
        } else
            return 200;
    }
}
