<?php

namespace App\Controller\Foods\Sales;

use App\Controller\Admin\Plan;
use App\Controller\Page;
use App\Model\Sales as ModelSales;
use App\Session\Admin\Login;
use App\Utils\View;

class Sales extends Page
{

    public static function getSales()
    {
        $view = 'foods/sales';

        $chosed = 'Hoje';

        //Filtro de dias
        $option = strtotime('today');
        if (isset($_GET['o'])) {
            switch ($_GET['o']) {
                case 0:
                    $option = strtotime('today');
                    $chosed = 'Hoje';
                    break;
                case 1:
                    $option = strtotime('-' . date('w') . ' days');
                    $chosed = 'Nesta semana';
                    break;
                case 2:
                    $option = strtotime('-' . (date("d") - 1) . ' days');
                    $chosed = 'Neste mês';
                    break;
                case 3:
                    $option = strtotime('');
                    $chosed = 'Todos';
                    break;
            }
        }

        $listSales = ModelSales::getSales($option);

        $next[1] = "Em preparo";
        $next[2] = "Saiu pra entrega";
        $next[3] = "Concluir pedido";

        //Adicionando vazio a todas as colunas do kanban
        $columns = [];
        for ($i = 0; $i <= 4; $i++)
            $columns[$i] = '';

        //Vendas do kanban
        $cont = 0;
        $qtdOrders = 0;
        foreach ($listSales as $key => $values) {
            foreach ($values as $key => $values) {

                $statusQuant = sizeof($values['pedido']['status']);
                $retirada = $values['pedido']['retirada'] ?? false;

                if ($statusQuant > 1) {

                    //Contando as novas compras
                    if ($statusQuant == 2 && !isset($values['status']) || $statusQuant == 2 && $values['status'] != "Cancelada")
                        $qtdOrders++;

                    $dataAndHour = $values['pedido']['hora'] . ' - ' . substr($values['pedido']['data'], 0, -5);

                    //Reduzi uma quantidade pois o primeiro status é fora do kamban
                    $column = $statusQuant - 1;

                    //Caso seja pedido na mesa, pular o status saiu pra entrega
                    if ($values['tipoPedido'] == "mesa") {
                        $next[2] = "Concluir pedido";
                        if ($statusQuant == 4)
                            $column = 4;
                    } else if ($retirada) {
                        $next[2] = "Pronto p/ retirada";
                    } else
                        $next[2] = "Saiu pra entrega";

                    //Verificando se o status da compra é cancelada
                    $statusEnd = "Entregue";
                    $cancelled = "";
                    if (isset($values['status']) && $values['status'] == "Cancelada") {
                        $column = 4;
                        $cancelled = "cancelled";
                        $statusEnd = "Cancelada";
                    }

                    //Order footer info
                    $svgProgramada = 'none';
                    if (isset($values['pedido']['mesa'])) {
                        $order_footer_info = 'Mesa ' . $values['pedido']['mesa'];
                    } else if (isset($values['pedido']['dataProgramada']) && $values['pedido']['dataProgramada'] !== '') {
                        $order_footer_info = $values['pedido']['dataPrevistaEntrega'] . ' - ' . $values['pedido']['horaPrevistaEntrega'];
                        $svgProgramada = 'block';
                    } else {
                        $order_footer_info = 'Entregar até ' . $values['pedido']['horaPrevistaEntrega'];
                    }

                    //Order footer
                    if ($column == 4) {
                        $last_status = end($values['pedido']['status']);
                        $hour = explode('-', $last_status['dataHoraInsert'])[1];
                        $data = explode('-', $last_status['dataHoraInsert'])[0];

                        $order_footer = View::render('utils/sales/kanban-order-footer-end', [
                            "statusEnd" => $statusEnd,
                            "dataAndHour" => substr($hour, 0, -3) . " - " . $data
                        ]);
                    } else {
                        $order_footer = View::render('utils/sales/kanban-order-footer', [
                            "order-footer-info" => $order_footer_info,
                            "nextColumn" => $next[$column] ?? '',
                            "svgProgramada" => $svgProgramada
                        ]);
                    }

                    $columns[$column] .= View::render('utils/sales/kanban-order', [
                        "tipoPedido" => $values['tipoPedido'] ?? 'delivery',
                        "codigoTransacao" => $values['codigoTransacao'],
                        "codigoTransacaoMin" => explode("CPID", explode("-", $values['codigoTransacao'])[1])[1],
                        "telefone" => isset($values['telefoneCompra']) ? 'telefone="' . $values['telefoneCompra'] . '"' : '',
                        "nomeUsuario" => $values['cliente']['nomeUsuario'],
                        "cancelled" => $cancelled,
                        "dataHora" => $dataAndHour,
                        "valorPedido" => parent::price($values['pagamento']['valorTotal']),
                        "endereco" => isset($values['cliente']['rua']) && isset($values['cliente']['numero']) ? '<div class="order-address"> <p>' . $values['cliente']['rua'] . ', ' . $values['cliente']['numero'] . ', ' . $values['cliente']['bairro'] . ', ' . $values['cliente']['cidade'] . ', ' . $values['cliente']['estado'] . ', CEP ' . $values['cliente']['cep'] . '</p> </div>' : '',
                        "order-footer" => $order_footer
                    ]);

                    $cont++;
                }
            }
        }

        //Titulos do kanban
        $titles = [];
        $listTitle = ModelSales::listTitle();
        if ($listTitle['items'] != null) {
            foreach ($listTitle['items'] as $key => $value) {
                if (isset($value['statusAlterado']) && $value['statusAlterado'] != '') {
                    $titles['t' . $key] = $value['statusAlterado'];
                    $titles['ct' . $key] = 'block';
                    $titles['st' . $key] = '(' . $value['statusInicial'] . ')';
                } else {
                    $titles['t' . $key] = $value['statusInicial'];
                    $titles['ct' . $key] = 'none';
                    $titles['st' . $key] = '';
                }
            }
        }

        //Abrindo uma compra especifica via POST do id da compra.
        $sale_key = '';
        if (isset($_GET['key']))
            $sale_key = '<script>$(\'.kanban-order[key="' . $_GET['key'] . '"]\').click();</script>';

        //Variaveis do html
        $data = [
            'cont' => $cont,
            'qtdOrders' => $qtdOrders > 0 ? '<p class="title-cont">' . $qtdOrders . '</p>' : '',
            'chosed' => $chosed
        ];
        $data = array_merge($columns, $data);
        $data = array_merge($titles, $data);

        $content = View::render($view, $data);
        return parent::getPage($view, $content, [
            'title' => 'Vendas',
            'after' => $sale_key
        ]);
    }

    public static function getSaleByKey()
    {
        error_reporting(0);

        $view = 'utils/sales/purchase-data';

        $code = $_POST['key'];
        $type = $_POST['type'];
        $telefone = $_POST['telefone'];

        $sale = ModelSales::getSalesByKey($code, $telefone, $type);
        $retirada = $sale['pedido']['retirada'] ?? false;
        $finalizada = end($sale['pedido']['status'])['descricao'] == "Finalizado";
        $cancelada = end($sale['pedido']['status'])['descricao'] == "Cancelada";

        $next[1] = "Avançar ‣ EM PREPARO";
        $next[2] = "Avançar ‣ SAIU PRA ENTREGA";
        $next[3] = "Avançar ‣ CONCLUÍDO";

        //Cabeçalho
        $purchase_data_header_info = "";
        $message_canceled = "";
        if ($sale['status'] == "Cancelada") {
            $purchase_data_header_info =
                '<div class="purchase-data-header-info canceled"> 
                    <svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"><path d="M8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15ZM8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16Z" fill="#B00020"/> <path d="M8.92995 6.588L6.63995 6.875L6.55795 7.255L7.00795 7.338C7.30195 7.408 7.35995 7.514 7.29595 7.807L6.55795 11.275C6.36395 12.172 6.66295 12.594 7.36595 12.594C7.91095 12.594 8.54395 12.342 8.83095 11.996L8.91895 11.58C8.71895 11.756 8.42695 11.826 8.23295 11.826C7.95795 11.826 7.85795 11.633 7.92895 11.293L8.92995 6.588ZM8.99995 4.5C8.99995 4.76522 8.8946 5.01957 8.70706 5.20711C8.51952 5.39464 8.26517 5.5 7.99995 5.5C7.73474 5.5 7.48038 5.39464 7.29285 5.20711C7.10531 5.01957 6.99995 4.76522 6.99995 4.5C6.99995 4.23478 7.10531 3.98043 7.29285 3.79289C7.48038 3.60536 7.73474 3.5 7.99995 3.5C8.26517 3.5 8.51952 3.60536 8.70706 3.79289C8.8946 3.98043 8.99995 4.23478 8.99995 4.5Z" fill="#B00020"/></g> <defs> <clipPath id="clip0"> <rect width="16" height="16" fill="white"/> </clipPath> </defs> </svg> 
                    <p>Pedido Cancelado</p> 
                </div>';

            $message_canceled =
                '<div class="order-cancelled center-fix">
                    <div class="order-cancelled-title">
                        <svg class="not-wl" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"> <path d="M8 15C6.14348 15 4.36301 14.2625 3.05025 12.9497C1.7375 11.637 1 9.85652 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8C15 9.85652 14.2625 11.637 12.9497 12.9497C11.637 14.2625 9.85652 15 8 15ZM8 16C10.1217 16 12.1566 15.1571 13.6569 13.6569C15.1571 12.1566 16 10.1217 16 8C16 5.87827 15.1571 3.84344 13.6569 2.34315C12.1566 0.842855 10.1217 0 8 0C5.87827 0 3.84344 0.842855 2.34315 2.34315C0.842855 3.84344 0 5.87827 0 8C0 10.1217 0.842855 12.1566 2.34315 13.6569C3.84344 15.1571 5.87827 16 8 16Z" fill="#B00020"/> <path d="M8.92995 6.588L6.63995 6.875L6.55795 7.255L7.00795 7.338C7.30195 7.408 7.35995 7.514 7.29595 7.807L6.55795 11.275C6.36395 12.172 6.66295 12.594 7.36595 12.594C7.91095 12.594 8.54395 12.342 8.83095 11.996L8.91895 11.58C8.71895 11.756 8.42695 11.826 8.23295 11.826C7.95795 11.826 7.85795 11.633 7.92895 11.293L8.92995 6.588ZM8.99995 4.5C8.99995 4.76522 8.8946 5.01957 8.70706 5.20711C8.51952 5.39464 8.26517 5.5 7.99995 5.5C7.73474 5.5 7.48038 5.39464 7.29285 5.20711C7.10531 5.01957 6.99995 4.76522 6.99995 4.5C6.99995 4.23478 7.10531 3.98043 7.29285 3.79289C7.48038 3.60536 7.73474 3.5 7.99995 3.5C8.26517 3.5 8.51952 3.60536 8.70706 3.79289C8.8946 3.98043 8.99995 4.23478 8.99995 4.5Z" fill="#B00020"/> </g> <defs> <clipPath id="clip0"> <rect width="16" height="16" fill="white"/> </clipPath> </defs> </svg><p>Pedido cancelado</p><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M10.1979 8L14.5 12.3021L12.8021 14L8.5 9.6979L4.1979 14L2.5 12.3021L6.8021 8L2.5 3.6979L4.1979 2L8.5 6.3021L12.8021 2L14.5 3.6979L10.1979 8Z" fill="#B00020"></path> </svg>
                    </div>
                    <p class="order-cancelled-motivation">Motivação</p>
                    <p class="order-cancelled-message">' . $sale['motivoCancelamento'] . '</p>
                </div>';
        } else {
            $svgProgramada = 'none';
            if ($sale['tipoPedido'] == "mesa")
                $info = 'Mesa ' . $sale['pedido']['mesa'];
            else if ($retirada)
                $info = 'Retirada até ' . $sale['pedido']['horaPrevistaEntrega'];
            else if (isset($sale['pedido']['dataProgramada']) && $sale['pedido']['dataProgramada'] !== '') {
                $info = $sale['pedido']['dataPrevistaEntrega'] . ' - ' . $sale['pedido']['horaPrevistaEntrega'];
                $svgProgramada = 'block';
            } else
                $info = 'Entregar até ' . $sale['pedido']['horaPrevistaEntrega'];

            $purchase_data_header_info = View::render('utils/sales/purchase-data-header-info', [
                "info" => $info,
                "svgProgramada" => $svgProgramada
            ]);
        }

        //Mostrar kebab com opção de cancelar apenas se a compra ainda estiver em andamento
        $more_options = "";
        if (!$finalizada && !$cancelada)
            $more_options = '<button data-toggle="dropdown"> <svg width="24" height="40" viewBox="0 0 24 40" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M22 16C23.1 16 24 15.1 24 14C24 12.9 23.1 12 22 12C20.9 12 20 12.9 20 14C20 15.1 20.9 16 22 16ZM22 18C20.9 18 20 18.9 20 20C20 21.1 20.9 22 22 22C23.1 22 24 21.1 24 20C24 18.9 23.1 18 22 18ZM22 24C20.9 24 20 24.9 20 26C20 27.1 20.9 28 22 28C23.1 28 24 27.1 24 26C24 24.9 23.1 24 22 24Z" fill="var(--color-primary)" /> </svg> </button> <div class="dropdown-menu"> <a id="order-cancel" class="dropdown-item">Cancelar pedido</a> </div>';

        //Montando svg de progresso do pedido
        $column = 0;
        $status_hours = [];
        $status_svg = $sale['pedido']['status'];
        array_shift($status_svg);
        for ($i = 0; $i <= 3; $i++) {
            if (isset($status_svg[$i])) {

                $dataHoraInsert = explode('-', $status_svg[$i]['dataHoraInsert']);
                $status_hours['status_hours_' . $i] .= '<p>' . $dataHoraInsert[1] . '</p> <p>' . $dataHoraInsert[0] . '</p>';

                $column++;
            } else {
                $status_hours['status_hours_' . $i] = '';
            }
        }

        //Renderizando svg de progresso do pedido
        $svg_status = View::render('utils/sales/svg-status-' . $type, $status_hours);

        //Caso seja pedido na mesa, pular o status saiu pra entrega
        if ($sale['tipoPedido'] == "mesa")
            $next[2] = "Avançar ‣ CONCLUÍDO";

        //Caso seja pedido na mesa, pular o status saiu pra entrega
        if ($retirada)
            $next[2] = "Avançar ‣ PRONTO P/ RETIRADA";

        //Botão para alterar o progresso do pedido
        $buttonProgress = "";
        if (!$finalizada && !$cancelada) {
            $buttonProgress = View::render('utils/sales/sales-progress-button', [
                "code" => $code,
                "telefone" => isset($telefone) ? 'telefone="' . $telefone . '"' : '',
                "type" => $type,
                "index" => sizeof($sale['pedido']['status']) - 1,
                "next_count" => $next[$column]
            ]);
        }

        //Lista com todos os produtos do pedido
        $carrinhoList = "";
        foreach ($sale['carrinhoList'] as $key => $value) {

            $salesitem = "";
            if (isset($value['ingredientesEscolhidos'])) {
                //Title
                $salesitem .= '<div class="sales-item-each-extra"><h5>Ingredientes removidos</h5>';
                //Itens
                foreach ($value['ingredientesEscolhidos'] as $keyRem => $valueRem) {
                    if ($valueRem != null)
                        $salesitem .= '<p class="item-removed">- ' . $valueRem['quantidade'] . ' ' . $valueRem['nomeProduto'] . '</p>';
                }
                //Finish
                $salesitem .= '</div>';
            }

            $secao_escolha_unica = '';
            $count_adicionais = 0;
            if (isset($value['adicionais'])) {
                //Itens
                foreach ($value['adicionais'] as $key => $values) {
                    if ($values['quantidade'] != "0") {
                        //Title
                        if ($count_adicionais == 0) {
                            $salesitem .= '<div class="sales-item-each-extra"><h5>Adicionais</h5>';
                            $count_adicionais++;
                        }
                        $salesitem .= '<p>+ ' . $values['quantidade'] . ' ' . $values['nomeProduto'] . '</p>';
                    } else if ($values['quantidade'] == "0")
                        $secao_escolha_unica .=
                            '<p>
                                <svg class="not-wl" width="5" height="5" viewBox="0 0 5 5" fill="none" xmlns="http://www.w3.org/2000/svg"> <circle cx="2.5" cy="2.5" r="2.5" fill="#585858"/></svg> 
                                ' . $values['nomeProduto'] . '
                            </p>';
                }
                //Finish
                if ($count_adicionais > 0)
                    $salesitem .= '</div>';
            }

            if ($secao_escolha_unica != '') {
                $salesitem .=
                    '<div class="sales-item-each-extra">
                        <h5>Seção de escolha única</h5>
                        ' . $secao_escolha_unica . '
                    </div>';
            }

            if (isset($value['notasAdicionais']))
                $salesitem .=
                    '<div class="sales-item-each-extra">
                        <h5>Observação</h5>
                        <p>' . $value['notasAdicionais'] . '</p>
                    </div>';

            $carrinhoList .= View::render('utils/sales/sales-item-each', [
                "url" => parent::https($value['produto'][0]['url']),
                "nomeProduto" => $value['produto'][0]['nomeProduto'],
                "medida" => isset($value['produto'][0]['medida']) ? $value['produto'][0]['medida'] : '',
                "quantidade" => $value['quantidade'] < 10 ? '0' . $value['quantidade'] : $value['quantidade'],
                "valorInicial" => parent::price($value['produto'][0]['valorInicial']),
                "valorFinal" => parent::price($value['valorFinal']),
                "sales-item-each-extra" => $salesitem
            ]);
        }

        //Sales Payment
        if ($sale['tipoPedido'] == 'mesa') {
            $sales_payment = View::render('utils/sales/sales-payment-mesa', [
                "total" => 'R$ ' . parent::price($sale['pagamento']['valorTotal'])
            ]);
        } else if ($retirada) {
            $sales_payment = View::render('utils/sales/sales-payment-withdraw', [
                "total" => 'R$ ' . parent::price($sale['pagamento']['valorTotal'])
            ]);
        } else {
            //Se o pagamento for no dinheiro
            if ($sale['pagamento']['metodoPagamento'] == "Dinheiro") {
                if ($sale['pagamento']['valorPagoDinheiro'] > 0) {
                    $sales_method = View::render('utils/sales/sales-payment-delivery-money', [
                        "valorPagoDinheiro" => 'R$ ' . parent::price($sale['pagamento']['valorPagoDinheiro']),
                        "vTroco" => 'R$ ' . parent::price($sale['pagamento']['troco'])
                    ]);
                } else {
                    $sales_method = View::render('utils/sales/sales-payment-delivery-money', [
                        "valorPagoDinheiro" => 'Não definido',
                        "vTroco" => 'R$ ' . parent::price($sale['pagamento']['troco'])
                    ]);
                }
            } else {
                $sales_method = View::render('utils/sales/sales-payment-delivery-card', [
                    "tipo" => $sale['pagamento']['metodoPagamento']
                ]);
            }

            $sales_payment = View::render('utils/sales/sales-payment-delivery', [
                "sales-method" => $sales_method,
                "vProds" => 'R$ ' . parent::price($sale['pagamento']['valorCompra']),
                "valorFrete" => 'R$ ' . parent::price($sale['pagamento']['valorFrete']),
                "cupom" => isset($sale['pagamento']['cupomCompra']) && $sale['pagamento']['cupomCompra'] != '' ? '<div class="d-flex"> <p>Cupom:</p> <p>' . $sale['pagamento']['cupomCompra'] . '</p> </div>' : '',
                "desconto" => isset($sale['pagamento']['cupomDesconto']) && $sale['pagamento']['cupomDesconto'] != '' ? '<div class="d-flex"> <p>Desconto:</p> <p>' . $sale['pagamento']['cupomDesconto'] . '</p> </div>' : '',
                "total" => 'R$ ' . parent::price($sale['pagamento']['valorTotal'])
            ]);
        }

        echo View::render($view, [
            "tipoPedido" => $retirada ? 'withdraw' : $sale['tipoPedido'],
            "message_canceled" => $message_canceled,
            "more_options" => $more_options,
            "buttonProgress" => $buttonProgress,
            "status_hours" => $status_hours,
            "purchase_data_header_info" => $purchase_data_header_info,
            "carrinhoList" => $carrinhoList,
            "codigoPedido" => explode("CPID", explode("-", $sale['codigoTransacao'])[1])[1],
            "codigoPedidoCompleto" => $code,
            "nomeUsuario" => $sale['cliente']['nomeUsuario'],
            "telefone" => $sale['cliente']['telefone'],
            "telefoneFix" => ($telefone != null ? 'phone="' . $telefone . '"' : ''),
            "status" => $cancelada ? 'cancel' : $column,
            "status_cancelado" => "",
            "endereco" => isset($sale['cliente']['rua']) && isset($sale['cliente']['numero']) ? $sale['cliente']['rua'] . ', ' . $sale['cliente']['numero'] . ', ' . $sale['cliente']['bairro'] . ', ' . $sale['cliente']['cidade'] . ', ' . $sale['cliente']['estado'] . ', CEP ' . $sale['cliente']['cep'] : '',
            "complemento" => isset($sale['cliente']['complemento']) && $sale['cliente']['complemento'] != '' ? '<div class="line-breaker" style="margin-top:8px"></div><p class="sales-address-note">Observação:</p><p class="sales-address-note-text">' . $sale['cliente']['complemento'] . '</p>' : '',
            "sales-payment" => $sales_payment,
            "svg_status" => $svg_status
        ]);
    }

    public static function allowOrderByKey()
    {
        $response = [];
        $code = $_POST['cod'];
        $telefone = $_POST['phone'];
        $type = $_POST['type'];

        $sale = ModelSales::atualizarStatusFoods([
            "key" => $code,
            "telefone" => $telefone,
            "tipoCompra" => $type == 'mesa' ? 1 : 2
        ])['item'];

        if (!isset($sale['statusCode']) || $sale['statusCode'] !== 400) {

            $dataAndHour = $sale['pedido']['hora'] . ' - ' . substr($sale['pedido']['data'], 0, -5);

            //Order footer info
            $svgProgramada = 'none';
            if (isset($sale['pedido']['mesa'])) {
                $order_footer_info = 'Mesa ' . $sale['pedido']['mesa'];
            } else if (isset($sale['pedido']['dataProgramada']) && $sale['pedido']['dataProgramada'] !== '') {
                $order_footer_info = $sale['pedido']['dataPrevistaEntrega'] . ' - ' . $sale['pedido']['horaPrevistaEntrega'];
                $svgProgramada = 'block';
            } else {
                $order_footer_info = 'Entregar até ' . $sale['pedido']['horaPrevistaEntrega'];
            }

            //Order footer
            $order_footer = View::render('utils/sales/kanban-order-footer', [
                "order-footer-info" => $order_footer_info,
                "nextColumn" => "Em preparo",
                "svgProgramada" => $svgProgramada
            ]);

            $response['order'] = View::render('utils/sales/kanban-order', [
                "tipoPedido" => $sale['tipoPedido'] ?? 'delivery',
                "codigoTransacao" => $sale['codigoTransacao'],
                "codigoTransacaoMin" => explode("CPID", explode("-", $sale['codigoTransacao'])[1])[1],
                "telefone" => isset($sale['telefoneCompra']) ? 'telefone="' . $sale['telefoneCompra'] . '"' : '',
                "nomeUsuario" => $sale['cliente']['nomeUsuario'],
                "cancelled" => "",
                "dataHora" => $dataAndHour,
                "valorPedido" => parent::price($sale['pagamento']['valorTotal']),
                "endereco" => isset($sale['cliente']['rua']) && isset($sale['cliente']['numero']) ? '<div class="order-address"> <p>' . $sale['cliente']['rua'] . ', ' . $sale['cliente']['numero'] . ', ' . $sale['cliente']['bairro'] . ', ' . $sale['cliente']['cidade'] . ', ' . $sale['cliente']['estado'] . ', CEP ' . $sale['cliente']['cep'] . '</p> </div>' : '',
                "order-footer" => $order_footer
            ]);

            $response['key'] = $sale['codigoTransacao'];

            return json_encode($response);
        }
    }

    public static function cancelSale()
    {
        $response = ModelSales::cancelSale($_POST);
        die(json_encode($response));
    }

    public static function changeStatus()
    {
        $id = $_POST['id'];
        $type = $_POST['type'];

        if (isset($_POST['telefone']) && $_POST['telefone'] != '') {
            $telefone = $_POST['telefone'];

            $response = ModelSales::atualizarStatusFoods([
                "key" => $id,
                "telefone" => $telefone,
                "tipoCompra" => $type == 'mesa' ? 1 : 2
            ]);

            parent::status($response);
        }
    }

    public static function changeTitle()
    {
        $response = ModelSales::updateTitle($_POST['pos'], $_POST['title']);
        die(json_encode($response));
    }

    public static function restoreTitle()
    {
        $response = ModelSales::resetTitle($_POST['pos']);
        die(json_encode($response));
    }

    public static function getSalesFirebase()
    {
        Plan::verify(2);

        $sales = ModelSales::getSalesAprovacao()['item'];
        $codes = [];

        $salesList = [];
        if ($sales != null) {

            foreach ($sales as $index => $value) {
                $retirada = $value['pedido']['retirada'] ?? false;

                $itensPedido = '';
                foreach ($value['carrinhoList'] as $indexCl => $valueCl) {
                    $itensPedido .=
                        '<p class="order-item">' . $valueCl['quantidade'] . 'x ' . $valueCl['produto'][0]['nomeProduto'] . '</p>';
                }

                $salesList[] .= View::render('utils/salePrint/sale', [
                    "telefone" => $value['cliente']['telefone'],
                    "type" => $value['tipoPedido'] ?? '',
                    "subType" => $retirada ? 'withdraw' : '',
                    "mesa" => isset($value['pedido']['mesa']) ? 'Mesa ' . $value['pedido']['mesa'] : '',
                    "nome" => $value['cliente']['nomeUsuario'],
                    "codigoTransacaoFull" => $value['codigoTransacao'],
                    "codigoTransacao" => explode('-', $value['codigoTransacao'])[1],
                    "endereco" => isset($value['cliente']['rua']) && isset($value['cliente']['numero']) ? $value['cliente']['rua'] . ', ' . $value['cliente']['numero'] . ' - ' . $value['cliente']['bairro'] . ', ' . $value['cliente']['cidade'] . ' - ' . $value['cliente']['estado'] . ', ' . $value['cliente']['cep'] : '',
                    "total" => parent::price($value['pagamento']['valorTotal']),
                    "itensPedido" => $itensPedido
                ]);

                $codes[] = $value['codigoTransacao'];
            }
        }

        return $salesList;
    }

    public static function verifySales()
    {
        $sales = ModelSales::getSalesObject();

        if (isset($sales["itens"])) {
            return json_encode($sales["itens"]);
        }
    }

    public static function removeSale()
    {
        return ModelSales::updateSalesObject($_POST['cod'], "Finalizado");
    }

    public static function printSale()
    {
        $view = 'utils/salePrint/main';

        $code = $_POST['cod'];
        $telefone = $_POST['telefone'];
        $tipo  = $_POST['tipo'];

        $sale = ModelSales::getSalesByKey($code, $telefone, $tipo);
        $retirada = $sale['pedido']['retirada'] ?? false;

        $itens = '';
        foreach ($sale['carrinhoList'] as $cod => $values) {

            //Separando adicionais normais de obrigatorios
            $itemAdicionais = '';
            $escolhaUnica = [];
            if (isset($values['adicionais'])) {
                foreach ($values['adicionais'] as $key => $value) {
                    if ($value['quantidade'] < 1)
                        $escolhaUnica[$value['key']][] =
                            '<p>' . $value['nomeProduto'] . '</p>';
                    else
                        $itemAdicionais .=
                            '<p>' . $value['quantidade']  . 'x ' . $value['nomeProduto'] . '</p>';
                }
            }

            //Montando adicionais
            $adicionais = '';
            if ($itemAdicionais != '')
                $adicionais .= '<div>' . $itemAdicionais . '</div>';

            //Montando Ingredientes removidos
            $ingredientesRemovidos = '';
            if (isset($values['ingredientesEscolhidos'])) {
                $ingredientesRemovidos .= '<div><h5>Ingredientes removidos:</h5>';

                foreach ($values['ingredientesEscolhidos'] as $key => $value) {
                    if ($value != null)
                        $ingredientesRemovidos .=
                            '<p>-' . $value['quantidadeAlterada']  . ' ' . $value['nomeProduto'] . '</p>';
                }

                $ingredientesRemovidos .= '</div>';
            }

            //Montando adicionais obrigatorios
            $adicionaisObrigatorios = '';
            if ($escolhaUnica != null) {
                foreach ($escolhaUnica as $key => $value) {
                    $adicionaisObrigatorios = '<div><h5>' . $key . ':</h5>';
                    foreach ($value as $value) {
                        $adicionaisObrigatorios .= '<div>' . $value . '</div>';
                    }
                    $adicionaisObrigatorios .= '</div>';
                }
            }

            //Montando observação
            $observacao = '';
            if (isset($values['notasAdicionais']))
                $observacao .= '<div><h5>Observação:</h5><h5>' . $values['notasAdicionais'] . '</h5></div>';

            $itens .=
                '<div class="new-sale-print-orderitems-item">
                    <h5 class="item-name">' . $values['quantidade'] . 'x ' . $values['produto'][0]['nomeProduto'] . '</h5>
                    <h5 class="item-price">R$ ' . parent::price($values['valorFinal']) . '</h5>
                    <h5 class="item-price-unity">(UNIT: ' . parent::price($values['valorFinal'] / $values['quantidade']) . ')</h5>
                    ' . $adicionais . '
                    ' . $ingredientesRemovidos . '
                    ' . $adicionaisObrigatorios . '
                    ' . $observacao . '
                </div>';
        }


        $cupom = '';
        if (isset($sale['pagamento']['cupomCompra']) && $sale['pagamento']['cupomCompra'] != null) {
            $cupom =
                '<div>
                    <p>Cupom:</p>
                    <p>' . $sale['pagamento']['cupomCompra'] . '</p>
                </div>
                <div>
                    <p>Desconto:</p>
                    <p style="position: unset;">' . $sale['pagamento']['cupomDesconto'] . '</p>
                </div>';
        }

        $orderTag = '';
        if ($sale['tipoPedido'] == 'delivery') {
            if ($retirada) {
                $orderTag = View::render('utils/salePrint/orderTagDeliveryWithdraw', []);
            } else {
                $orderTag = View::render('utils/salePrint/orderTagDelivery', []);
            }
        } else if ($sale['tipoPedido'] == 'mesa') {
            $orderTag = View::render('utils/salePrint/orderTagMesa', ['mesa' => $sale['pedido']['mesa']]);
        }

        $paymentMode = '';
        if (isset($sale['pagamento']['tipoPagamento'])) {
            if ($sale['pagamento']['tipoPagamento'] == "online") {
                if ($sale['tipoPedido'] == "mesa") {
                    $paymentMode = View::render('utils/salePrint/paymentModeMesa', []);
                } else {
                    $paymentMode = View::render('utils/salePrint/paymentModeOnline', []);
                }
            } else if ($sale['pagamento']['tipoPagamento'] == "delivery") {
                if (isset($sale['pagamento']['valorPagoDinheiro']) && isset($sale['troco'])) {
                    $paymentMode = View::render('utils/salePrint/paymentModeDinheiroTroco', ['valor' => parent::price($sale['pagamento']['valorPagoDinheiro']), 'troco' => parent::price($sale['pagamento']['troco'])]);
                } else if (isset($sale['pagamento']['valorPagoDinheiro']) && $sale['pagamento']['valorPagoDinheiro'] != null) {
                    $paymentMode = View::render('utils/salePrint/paymentModeDinheiro', ['valor' => parent::price($sale['pagamento']['valorPagoDinheiro'])]);
                } else {
                    $paymentMode = View::render('utils/salePrint/paymentModeCartao', []);
                }
            }
        }

        return View::render($view, [
            'nomeEstabelecimento' => Login::getData('name'),
            'cnpjEstabelecimento' => parent::mask(Login::getData('cnpj'), '##.###.###/####-##'),
            'enderecoEstabelecimento' => Login::getData('endereco'),
            'codigoTransacao' => explode('-', $code)[1],
            'data' => $sale['pedido']['data'] ?? '',
            'hora' => $sale['pedido']['hora'] ?? '',
            'previsao' => isset($sale['pedido']['horaPrevistaEntrega']) ? '<p>Previsão de entrega: ' . $sale['pedido']['horaPrevistaEntrega'] . ' - ' . $sale['pedido']['dataPrevistaEntrega'] . '</p>' : '',
            'nome' => $sale['cliente']['nomeUsuario'],
            'telefone' => $sale['cliente']['telefone'],
            'endereco' => isset($sale['cliente']['rua']) && isset($sale['cliente']['numero']) ? '<div class="order-address"> <p>' . $sale['cliente']['rua'] . ', ' . $sale['cliente']['numero'] . ', ' . $sale['cliente']['bairro'] . ', ' . $sale['cliente']['cidade'] . ', ' . $sale['cliente']['estado'] . ', CEP ' . $sale['cliente']['cep'] . '</p> </div>' : '',
            'complemento' => isset($sale['cliente']['complemento']) && $sale['cliente']['complemento'] != "" ? '<p>' . $sale['cliente']['complemento'] . '</p>' : '',
            'itens' => $itens,
            'valorPedido' => isset($sale['pagamento']['valorCompra']) ? parent::price($sale['pagamento']['valorCompra']) : '',
            'frete' => isset($sale['pagamento']['valorFrete']) && !$retirada ? '<div><p>Frete:</p><p>R$ ' . parent::price($sale['pagamento']['valorFrete']) . '</p></div>' : '',
            'cupom' => $cupom,
            'total' => parent::price($sale['pagamento']['valorTotal']),
            'orderTag' => $orderTag,
            'paymentMode' => $paymentMode
        ]);
    }
}
