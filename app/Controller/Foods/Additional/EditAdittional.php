<?php

namespace App\Controller\Foods\Additional;

use App\Http\Request;
use App\Http\Router;
use App\Model\Adicionais;

class EditAdittional
{
    public static function getRegisterAdittional()
    {
        Adicionais::registerAdittional(json_decode($_POST['itens'], true));
    }

    public static function getEditAdittional()
    {
        Adicionais::editAdittional(json_decode($_POST['itens'], true), $_POST['cat']);
    }

    public static function getDeleteCategoryFunction()
    {
        Adicionais::deleteCategory($_POST['category']);
    }

    public static function fastUpdateAdittional()
    {
        Adicionais::fastUpdateAdittional(json_decode($_POST['fastUpdate'], true));
    }

    public static function changeCategoryAdittional()
    {
        Adicionais::changeCategoryAdittional($_GET['oldCategory'], $_GET['category']);

        $request = new Request(new Router(URL));
        $request->getRouter()->redirect('/adicionais');
    }

    public static function updateCategoryAdittional()
    {
        Adicionais::updateCategoriaAdicionais($_POST['category'], json_decode($_POST['availability']));
    }

    public static function excludeAdittional()
    {
        Adicionais::deleteAdittional($_POST['key'],$_POST['cod']);
    }

    public static function copyAdittionalCategory()
    {
        Adicionais::copyAdittionalCategory(json_decode($_POST['itens'], true),$_POST['category']);
    }
}
