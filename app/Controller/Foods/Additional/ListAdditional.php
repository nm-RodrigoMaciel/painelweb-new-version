<?php

namespace App\Controller\Foods\Additional;

use App\Controller\Page;
use App\Model\ProdutoFood;
use \App\Utils\View;

class ListAdditional extends Page
{
    public static function getlistAdditional()
    {
        $view = 'foods/listAdditional';

        $qtdAdittional = 0;
        $adittional = '';
        $category = '';
        $selected = 'selected';
        $item = '';
        $first = 'first-header';
        $copyAdittional = '';

        $ingredientList = ProdutoFood::getIngredientes();

        if (sizeof($ingredientList) > 0) {
            foreach ($ingredientList as $key => $values) {
                $keyFix = parent::keyFix($key);

                $categoryAvailability = '';
                foreach ($values as $a => $b) {
                    if ($b['disponivel']) {
                        $categoryAvailability = 'checked';
                    }
                }

                //Scroll Horizontal de categorias no topo do site
                $category .=
                    "<div class='category $selected' ref='$keyFix'>
                    <span>$key</span>
                </div>";
                $selected = '';

                //Lista de categorias na criação de um adicional
                $item .=
                    '<div class="category-dropdown-item" search="' . $keyFix . '">
                    <p>' . $key . '</p>
                    <label class="custom-checkbox">
                        <input type="checkbox" ref="' . $key . '">
                        <span class="checkmark"></span>
                    </label>
                </div>';

                //Cabeçalho de cada categoria de adicionais
                $adittional .=
                    '<div class="category-section-title ' . $first . '" ref="' . $keyFix . '" cat="' . $key . '">
                    <div class="title" id="' . $keyFix . '">
                        <p class="title-text">' . $key . '</p>
                        <p class="title-disponibility">Disponível</p>
                        <label class="cl-switch cl-switch-green">
                            <input class="category-switch" type="checkbox" cat="' . $key . '" ' . $categoryAvailability . '>
                            <span class="switcher"></span>
                        </label>
                    </div>
                    <div class="buttons-title">
                        <div class="new-adittional" cat="' . $key . '">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5303 5.46967C12.3897 5.32902 12.1989 5.25 12 5.25C11.8011 5.25 11.6103 5.32902 11.4697 5.46967C11.329 5.61032 11.25 5.80109 11.25 6V11.25H6C5.80109 11.25 5.61032 11.329 5.46967 11.4697C5.32902 11.6103 5.25 11.8011 5.25 12C5.25 12.1989 5.32902 12.3897 5.46967 12.5303C5.61032 12.671 5.80109 12.75 6 12.75H11.25V18C11.25 18.1989 11.329 18.3897 11.4697 18.5303C11.6103 18.671 11.8011 18.75 12 18.75C12.1989 18.75 12.3897 18.671 12.5303 18.5303C12.671 18.3897 12.75 18.1989 12.75 18V12.75H18C18.1989 12.75 18.3897 12.671 18.5303 12.5303C18.671 12.3897 18.75 12.1989 18.75 12C18.75 11.8011 18.671 11.6103 18.5303 11.4697C18.3897 11.329 18.1989 11.25 18 11.25H12.75V6C12.75 5.80109 12.671 5.61032 12.5303 5.46967Z" fill="var(--color-primary)" /> </svg>
                            <p>Novo item</p>
                        </div>
                        <div class="duplicate-adittional" ref="' . $key . '">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M14.1661 0H5.35016C4.34069 0 3.5163 0.824395 3.5163 1.83386V3.5163H1.83386C0.824395 3.5163 0 4.34069 0 5.35016V14.1661C0 15.1756 0.824395 16 1.83386 16H10.6498C11.6593 16 12.4837 15.1756 12.4837 14.1661V12.4837H14.1661C15.1756 12.4837 16 11.6593 16 10.6498V1.83386C16 0.824395 15.1756 0 14.1661 0ZM11.4406 14.1493C11.4406 14.5868 11.0873 14.9401 10.6498 14.9401H1.85068C1.41325 14.9401 1.05994 14.5868 1.05994 14.1493V5.33333C1.05994 4.8959 1.41325 4.54259 1.85068 4.54259H10.6667C11.1041 4.54259 11.4574 4.8959 11.4574 5.33333V14.1493H11.4406ZM14.9569 10.6498C14.9569 11.0873 14.6036 11.4406 14.1661 11.4406H12.4837V5.33333C12.4837 4.32387 11.6593 3.49947 10.6498 3.49947H4.55941V1.81703C4.55941 1.3796 4.91272 1.02629 5.35016 1.02629H14.1661C14.6036 1.02629 14.9569 1.3796 14.9569 1.81703V10.6498Z" fill="var(--color-primary)" /> <path d="M9.01784 9.21965H6.7802V6.98201C6.7802 6.69599 6.54466 6.46045 6.25864 6.46045C5.97263 6.46045 5.73708 6.69599 5.73708 6.98201V9.21965H3.48262C3.1966 9.21965 2.96106 9.45519 2.96106 9.74121C2.96106 10.0272 3.1966 10.2628 3.48262 10.2628H5.72026V12.5004C5.72026 12.7864 5.9558 13.022 6.24182 13.022C6.52783 13.022 6.76337 12.7864 6.76337 12.5004V10.2628H9.01784C9.30386 10.2628 9.5394 10.0272 9.5394 9.74121C9.5394 9.45519 9.30386 9.21965 9.01784 9.21965Z" fill="var(--color-primary)" /> </svg>
                            <p>Copiar item cadastrado</p>
                        </div>
                        <div class="input-group-append more-options">
                            <button data-toggle="dropdown">
                                <svg width="4" height="17" viewBox="0 0 4 17" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M2 4.5C3.1 4.5 4 3.6 4 2.5C4 1.4 3.1 0.5 2 0.5C0.9 0.5 0 1.4 0 2.5C0 3.6 0.9 4.5 2 4.5ZM2 6.5C0.9 6.5 0 7.4 0 8.5C0 9.6 0.9 10.5 2 10.5C3.1 10.5 4 9.6 4 8.5C4 7.4 3.1 6.5 2 6.5ZM2 12.5C0.9 12.5 0 13.4 0 14.5C0 15.6 0.9 16.5 2 16.5C3.1 16.5 4 15.6 4 14.5C4 13.4 3.1 12.5 2 12.5Z" fill="var(--color-primary)" /> </svg>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" ref="' . $key . '">Editar</a>
                                <a class="dropdown-item" ref="' . $key . '">Excluir</a>
                            </div>
                        </div>
                    </div>
                </div>';
                $first = '';

                //Cabeçalho dos ingredientes na listagem.
                $copyAdittional .=
                    '<div id="' . $keyFix . '" class="adittional-header popup-item">
                        <p>' . $key . '</p>
                    </div>';

                foreach ($values as $value) {

                    $nameFix = parent::keyFix($value['nomeProduto']);

                    //Item adicional na tela principal
                    $adittional .=
                        "<div class='see-itens-adittional' search='" . $nameFix . "' cat='" . $key . "' key='" . $value['keyPush'] . "' cod='" . $value['codigoBarras'] . "' ref='" . $keyFix . "'>
                        <div class='adittional-details'>
                            <div class='adittional-info'>
                                <p class='title'>" . $value['nomeProduto'] . "</p>
                            </div>
                            <div class='adittional-value'>
                                <div class='value-input'>
                                    <span>R$</span>
                                    <input type='text' id='demo4' class='value-obj money-mask' data-thousands='.' data-decimal=','  value='" . str_replace('.', ',', $value['valorInicial']) . "'/>
                                </div>
                            </div>
                        </div>
                        <div class='adittional-options'>
                            <div class='available-area'>
                                <p class='title-disponibility'>" . ($value['disponivel'] ? 'Disponível' : 'Indisponível') . "</p>
                                <label class='cl-switch cl-switch-green'>
                                    <input class='enabled-obj' type='checkbox' cat='" . $key . "' " . ($value['disponivel'] ? 'checked' : '') . ">
                                    <span class='switcher'></span>
                                </label>
                            </div>
                            <p class='edit-adittional'>Editar</p>
                        </div>
                    </div>";

                    //Itens adicionais para copiar.
                    $copyAdittional .=
                        '<div class="product-dropdown-item ingredient-' . $keyFix . ' popup-item" search="' . $nameFix . '" cod="' . $value['codigoBarras'] . '">
                            <p>' . $value['nomeProduto'] . '</p>
                            <label class="custom-checkbox">
                                <input class="product-dropdown-item-check" type="checkbox" val="' . $value['nomeProduto'] . '" ref="' . $key . '" cod="' . $value['codigoBarras'] . '">
                                <span class="checkmark"></span>
                            </label>
                        </div>';

                    $qtdAdittional++;
                }
            }
        }

        $content = View::render($view, [
            'category' => $category,
            'qtdAdittional' => $qtdAdittional != 1
                ? $qtdAdittional . " Itens cadastrados"
                : $qtdAdittional . " Item cadastrado",
            'adittional' => $adittional,
            'categoryDropdown' => $item,
            'adittionalList' => $copyAdittional,
            'empty' => $qtdAdittional > 0
                ? ''
                : 'empty'
        ]);

        return parent::getPage($view, $content, ['title' => 'Adicionais']);
    }
}
