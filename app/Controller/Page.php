<?php

namespace App\Controller;

use App\Model\WhiteLabel;
use App\Session\Admin\Login;
use \App\Utils\View;

class Page
{
    private function getHeader($view, $extra)
    {
        return View::render('header', [
            'title' => $extra['title'] ?? 'Página',
            'css' => $view,
            'extraHeader' => $extra['extraHeader'] ?? '',
            'image' => Login::getData('url') == '' ? '/painelweb/img/logo-example.svg' : Login::getData('url') . '?t=' . time(),
            'favicon' => Login::getData('url') == '' ? 'img/favicon.ico' : Login::getData('url') . '?t=' . time(),
            'color' => Login::getData('colorPrimary') == '' ? '#F16B24' : Login::getData('colorPrimary')
        ]);
    }

    private function getFooter($view, $extra)
    {
        return View::render('footer', [
            'js' => $view,
            'extraFooter' => $extra['extraFooter'] ?? ''
        ]);
    }

    public static function getPage($view, $content, $extra = [])
    {
        //Header
        $header = self::getHeader($view, $extra);

        //Footer
        $footer = self::verifySteps();
        $footer .= self::getFooter($view, $extra);
        $footer .= self::recommendedPlan();
        $footer .= $extra['after'] ?? '';

        return View::render('page', [
            'header' => $header,
            'content' => $content,
            'footer' => $footer
        ]);
    }

    public static function price($value = "")
    {
        if ($value == "")
            $value = 0;

        return number_format($value, 2, ',', '.');
    }

    public static function priceFix($value = "")
    {
        if ($value == "")
            $value = 0;

        if (is_string($value)) {
            $value = str_replace(',', '.', $value);
        }

        return (float)$value;
    }

    public static function keyFix($key)
    {
        return preg_replace("/[^a-zA-Z0-9]+/", "", preg_replace('/\s+/', '', strtolower($key)));
    }

    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i])) $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function render()
    {
        $view = $_POST['view'];
        $data  = $_POST['data'] ?? [];

        return View::render('utils/' . $view, $data);
    }

    public static function status($response)
    {
        header('HTTP/1.1 ' . $response["statusCode"] . ' ' . $response["statusMessage"]);
        header('Content-Type: application/json; charset=UTF-8');
        die($response["statusMessage"]);
    }

    public static function verifySteps()
    {
        $content = '';
        $onboarding = Login::getData('onboarding');
        if ($onboarding != '' && $onboarding > 0 && $onboarding < 5) {

            if ($onboarding == 1)
                $content .= View::render('utils/onBoarding/onBoardingStart', []);

            $content .= View::render('utils/onBoarding/onBoardingHelp', [
                'step' => $onboarding
            ]);
        }

        return $content;
    }

    public static function https($url)
    {
        if (strpos($url, "http://") !== false)
            $url = str_replace("http://", "https://", $url);

        return $url;
    }

    public static function fixDate($data)
    {
        $dia = explode("/", $data)[0];
        $mes = explode("/", $data)[1];
        $ano = explode("/", $data)[2];

        return $mes . "/" . $dia . "/" . $ano;
    }

    public static function recommendedPlan()
    {
        $planoAtual = Login::getData('plano')['planoAtual'];

        if ($planoAtual['nome'] == "Plano Teste" || $planoAtual['nome'] == "Plano Vitrine") {

            $modal_recommended = $_COOKIE["modal_recommended"] ?? "";

            if ($modal_recommended != date("d/m/Y")) {

                setcookie("modal_recommended", "", time() - 3600);
                setcookie('modal_recommended', date("d/m/Y"), "/");

                $currentTime = strtotime(date("m") . '/' . date("d") . '/' . date("Y"));
                $periodoGratis = Login::getData('periodoGratis');
                $periodoGratis = $periodoGratis == '' ? 30 : $periodoGratis;
                $data = self::fixDate(Login::getData('dataCadastro'));
                $timeData = strtotime($data) + ($periodoGratis * 86400);

                $key = "";
                $tipo = "";
                if (Login::getData('botaoPedirNaMesa') == 'false') {
                    $key = "deliveryKey";
                    $tipo = "Plano Delivery";
                } else if (Login::getData('botaoPedirOnline')  == 'false') {
                    $key = "mesaKey";
                    $tipo = "Na mesa";
                } else {
                    $key = "completoKey";
                    $tipo = "Completo";
                }

                $plano = WhiteLabel::listPlanFirebase($key);
                $planBenefits = '';
                foreach ($plano['beneficios'] as $id => $value) {
                    $planBenefits .= View::render('utils/account/plan-benefits', ['text' => $value]);
                }

                $valor = 'GRÁTIS';
                if ($plano['valor'] > 0) {
                    $valor = self::price($plano['valor']);
                    $valor = "R$ " . explode(',', $valor)[0] . "<span>," . explode(',', $valor)[1] . "/mês";
                }
                $plan = View::render('utils/account/plan', [
                    'selected' => '',
                    'key' => $plano['planoKey'],
                    'nome' => $plano['nome'],
                    'valor' => $valor,
                    'descricao' => $plano['descricao'],
                    'valorAntigo' => isset($plano['valorAntigo']) ? '<h5>R$ ' . self::price($plano['valorAntigo']) . '/mês</h5>' : '',
                    'plan-benefits' => $planBenefits
                ]);

                $alert = "";
                $dias = (int)(($timeData - $currentTime) / 86400) + 1;

                if ($dias <= 7) {
                    if ($dias <= 0)
                        $alert = "O período de teste grátis chegou ao fim";
                    else
                        $alert = "Restam " . $dias . " dia" . ($dias > 1 ? 's' : '') . " para o fim do seu teste grátis";

                    return View::render('utils/modal/alert-upgrade-modal', [
                        "alert" => $alert,
                        "key" => $key,
                        "plan" => $plan,
                        "tipo" => $tipo
                    ]);
                }
            }
        }
    }
}
