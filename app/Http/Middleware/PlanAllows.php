<?php

namespace App\Http\Middleware;

use App\Controller\Admin\Plan;
class PlanAllows
{
    public function handle($request, $next, $var)
    {
        Plan::verify($var);

        return $next($request);
    }
}
